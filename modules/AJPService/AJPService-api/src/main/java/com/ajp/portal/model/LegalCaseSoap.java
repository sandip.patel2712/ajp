/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LegalCaseSoap implements Serializable {

	public static LegalCaseSoap toSoapModel(LegalCase model) {
		LegalCaseSoap soapModel = new LegalCaseSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setLegalCaseId(model.getLegalCaseId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setStatus(model.getStatus());
		soapModel.setStatusByUserId(model.getStatusByUserId());
		soapModel.setStatusByUserName(model.getStatusByUserName());
		soapModel.setStatusDate(model.getStatusDate());
		soapModel.setTitle(model.getTitle());
		soapModel.setDescription(model.getDescription());
		soapModel.setDecisionDate(model.getDecisionDate());
		soapModel.setCountryCategoryId(model.getCountryCategoryId());
		soapModel.setSubareaCategoryId(model.getSubareaCategoryId());
		soapModel.setSummaryFileEntryId(model.getSummaryFileEntryId());
		soapModel.setJudgementFileEntryId(model.getJudgementFileEntryId());
		soapModel.setUrl(model.getUrl());

		return soapModel;
	}

	public static LegalCaseSoap[] toSoapModels(LegalCase[] models) {
		LegalCaseSoap[] soapModels = new LegalCaseSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LegalCaseSoap[][] toSoapModels(LegalCase[][] models) {
		LegalCaseSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LegalCaseSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LegalCaseSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LegalCaseSoap[] toSoapModels(List<LegalCase> models) {
		List<LegalCaseSoap> soapModels = new ArrayList<LegalCaseSoap>(
			models.size());

		for (LegalCase model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LegalCaseSoap[soapModels.size()]);
	}

	public LegalCaseSoap() {
	}

	public long getPrimaryKey() {
		return _legalCaseId;
	}

	public void setPrimaryKey(long pk) {
		setLegalCaseId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getLegalCaseId() {
		return _legalCaseId;
	}

	public void setLegalCaseId(long legalCaseId) {
		_legalCaseId = legalCaseId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public long getStatusByUserId() {
		return _statusByUserId;
	}

	public void setStatusByUserId(long statusByUserId) {
		_statusByUserId = statusByUserId;
	}

	public String getStatusByUserName() {
		return _statusByUserName;
	}

	public void setStatusByUserName(String statusByUserName) {
		_statusByUserName = statusByUserName;
	}

	public Date getStatusDate() {
		return _statusDate;
	}

	public void setStatusDate(Date statusDate) {
		_statusDate = statusDate;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public Date getDecisionDate() {
		return _decisionDate;
	}

	public void setDecisionDate(Date decisionDate) {
		_decisionDate = decisionDate;
	}

	public long getCountryCategoryId() {
		return _countryCategoryId;
	}

	public void setCountryCategoryId(long countryCategoryId) {
		_countryCategoryId = countryCategoryId;
	}

	public long getSubareaCategoryId() {
		return _subareaCategoryId;
	}

	public void setSubareaCategoryId(long subareaCategoryId) {
		_subareaCategoryId = subareaCategoryId;
	}

	public long getSummaryFileEntryId() {
		return _summaryFileEntryId;
	}

	public void setSummaryFileEntryId(long summaryFileEntryId) {
		_summaryFileEntryId = summaryFileEntryId;
	}

	public long getJudgementFileEntryId() {
		return _judgementFileEntryId;
	}

	public void setJudgementFileEntryId(long judgementFileEntryId) {
		_judgementFileEntryId = judgementFileEntryId;
	}

	public String getUrl() {
		return _url;
	}

	public void setUrl(String url) {
		_url = url;
	}

	private String _uuid;
	private long _legalCaseId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private int _status;
	private long _statusByUserId;
	private String _statusByUserName;
	private Date _statusDate;
	private String _title;
	private String _description;
	private Date _decisionDate;
	private long _countryCategoryId;
	private long _subareaCategoryId;
	private long _summaryFileEntryId;
	private long _judgementFileEntryId;
	private String _url;

}