package com.ajp.portal.bean;

public interface CountryBean {
	String getCountryName();

	String getCountryFlagLink();

	String getCountryMapLink();

	String getCountryHomePageLink();

	void setCountryName(String countryName);

	void setCountryFlagLink(String countryFlagLink);

	void setCountryMapLink(String countryMapLink);

	void setCountryHomePageLink(String countryHomePageLink);
}
