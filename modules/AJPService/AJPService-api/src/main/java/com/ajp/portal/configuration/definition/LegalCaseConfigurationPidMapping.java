package com.ajp.portal.configuration.definition;

import com.ajp.portal.configuration.LegalCaseConfiguration;
import com.ajp.portal.constant.LegalCaseConstant;
import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;

import org.osgi.service.component.annotations.Component;

@Component(service = ConfigurationPidMapping.class)
public class LegalCaseConfigurationPidMapping implements ConfigurationPidMapping {

	@Override
	public Class<?> getConfigurationBeanClass() {
		return LegalCaseConfiguration.class;
	}

	@Override
	public String getConfigurationPid() {
		return LegalCaseConstant.SERVICE_NAME;
	}
}