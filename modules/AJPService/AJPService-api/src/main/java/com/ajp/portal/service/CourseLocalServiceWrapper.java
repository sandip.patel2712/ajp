/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CourseLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see CourseLocalService
 * @generated
 */
@ProviderType
public class CourseLocalServiceWrapper
	implements CourseLocalService, ServiceWrapper<CourseLocalService> {

	public CourseLocalServiceWrapper(CourseLocalService courseLocalService) {
		_courseLocalService = courseLocalService;
	}

	@Override
	public java.util.List<com.ajp.portal.bean.CourseBean> getArchivedCourses(
			long companyId, long groupId, java.util.List<Long> seCategories,
			java.util.List<Long> countryCategories, int courseCostType,
			boolean sortOrder, int start, int end,
			com.liferay.portal.kernel.search.SearchContext searchContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   java.text.ParseException, NumberFormatException {

		return _courseLocalService.getArchivedCourses(
			companyId, groupId, seCategories, countryCategories, courseCostType,
			sortOrder, start, end, searchContext);
	}

	@Override
	public java.util.List<com.ajp.portal.bean.CourseBean>
			getCourseJournalArticles(
				long companyId, long groupId, java.util.List<Long> seCategories,
				java.util.List<Long> countryCategories, int courseCostType,
				java.util.Date courseStartDate, java.util.Date courseEndDate,
				boolean sortOrder, int start, int end,
				com.liferay.portal.kernel.search.SearchContext searchContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   java.text.ParseException, NumberFormatException {

		return _courseLocalService.getCourseJournalArticles(
			companyId, groupId, seCategories, countryCategories, courseCostType,
			courseStartDate, courseEndDate, sortOrder, start, end,
			searchContext);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _courseLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public int getTotalArchivedCourses(
			long companyId, long groupId, java.util.List<Long> seCategories,
			java.util.List<Long> countryCategories, int courseCostType,
			boolean sortOrder,
			com.liferay.portal.kernel.search.SearchContext searchContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   java.text.ParseException, NumberFormatException {

		return _courseLocalService.getTotalArchivedCourses(
			companyId, groupId, seCategories, countryCategories, courseCostType,
			sortOrder, searchContext);
	}

	@Override
	public CourseLocalService getWrappedService() {
		return _courseLocalService;
	}

	@Override
	public void setWrappedService(CourseLocalService courseLocalService) {
		_courseLocalService = courseLocalService;
	}

	private CourseLocalService _courseLocalService;

}