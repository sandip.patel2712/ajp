/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service;

import aQute.bnd.annotation.ProviderType;

import com.ajp.portal.exception.LegalCaseCountryCategoryIdException;
import com.ajp.portal.exception.LegalCaseDecisionDateException;
import com.ajp.portal.exception.LegalCaseDescriptionException;
import com.ajp.portal.exception.LegalCaseSubareaCategoryIdException;
import com.ajp.portal.exception.LegalCaseTitleException;
import com.ajp.portal.exception.NoSuchLegalCaseException;
import com.ajp.portal.model.LegalCase;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import java.util.Date;
import java.util.List;

/**
 * Provides the local service interface for LegalCase. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see LegalCaseLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface LegalCaseLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LegalCaseLocalServiceUtil} to access the legal case local service. Add custom service methods to <code>com.ajp.portal.service.impl.LegalCaseLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	 * Adds the legal case to the database. Also notifies the appropriate model listeners.
	 *
	 * @param legalCase the legal case
	 * @return the legal case that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public LegalCase addLegalCase(LegalCase legalCase);

	/**
	 * Adds a new LegalCase with the given attributes.
	 *
	 * @param themeDisplay        object of ThemeDisplay class.
	 * @param title               the id of a user who adds the community group.
	 * @param countryCategoryId   the name for the community group.
	 * @param subareaCategoryId   the rules for the community group.
	 * @param description         the description for the community group.
	 * @param serviceContext      the service context to be applied. Can set the
	 UUID, creation date, modification date, expando
	 bridge attributes, guest permissions, group
	 permissions, asset category IDs, asset tag names,
	 asset link entry IDs, URL title, and work flow
	 actions for the web content article. Can also set
	 whether to add the default guest and group
	 permissions.
	 * @param caseSummaryFile     the File object of uploaded document.
	 * @param caseSummaryFileName the File name of uploaded document.
	 * @return the added LegalCase.
	 * @throws LegalCaseTitleException             if title is blank or too long.
	 * @throws LegalCaseDescriptionException       if description is blank or too
	 long.
	 * @throws LegalCaseDecisionDateException      if decisionDate is null.
	 * @throws LegalCaseCountryCategoryIdException if countryCategoryId is unknown
	 or zero.
	 * @throws LegalCaseSubareaCategoryIdException if same group is already exist.
	 * @throws PortalException                     if something's gone wrong.
	 * @throws IOException
	 */
	@Transactional(
		noRollbackFor = {
			LegalCaseTitleException.class, LegalCaseDescriptionException.class,
			LegalCaseCountryCategoryIdException.class,
			LegalCaseSubareaCategoryIdException.class,
			LegalCaseDecisionDateException.class
		}
	)
	@Indexable(type = IndexableType.REINDEX)
	public LegalCase addLegalCase(
			ThemeDisplay themeDisplay, long countryCategoryId,
			long subareaCategoryId, String title, String url,
			String description, Date decisionDate, File caseSummaryFile,
			String caseSummaryFileName, File caseJudgementFile,
			String caseJudgementFileName, ServiceContext serviceContext)
		throws IOException, PortalException;

	/**
	 * Counts legal cases for the given company filtering them by a user and a
	 * keyword and takes into account only public groups when the given
	 * <code>userId</code> is not positive.
	 *
	 * @param groupId the id of a site to which the community groups belong.
	 * @param userId  the id of a user who owns the community groups.
	 * @param keyword the keyword for search in title and description fields.
	 * @return the total number of found legal cases.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long countLegalCases(long groupId, long userId, String keyword);

	/**
	 * Creates a new legal case with the primary key. Does not add the legal case to the database.
	 *
	 * @param legalCaseId the primary key for the new legal case
	 * @return the new legal case
	 */
	@Transactional(enabled = false)
	public LegalCase createLegalCase(long legalCaseId);

	/**
	 * Deletes the legal case from the database. Also notifies the appropriate model listeners.
	 *
	 * @param legalCase the legal case
	 * @return the legal case that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public LegalCase deleteLegalCase(LegalCase legalCase);

	/**
	 * Deletes the legal case with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case that was removed
	 * @throws PortalException if a legal case with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public LegalCase deleteLegalCase(long legalCaseId) throws PortalException;

	/**
	 * Deletes a LegalCase with the given id.
	 *
	 * @param themeDisplay object of ThemeDisplay class.
	 * @param legalCaseId  the id of a LegalCase to delete.
	 * @return the deleted LegalCase.
	 * @throws NoSuchLegalCaseException if no LegalCase with the given id.
	 * @throws PortalException          if something's gone wrong.
	 */
	@Transactional(noRollbackFor = NoSuchLegalCaseException.class)
	@Indexable(type = IndexableType.DELETE)
	public LegalCase deleteLegalCase(
			ThemeDisplay themeDisplay, long legalCaseId)
		throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ajp.portal.model.impl.LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ajp.portal.model.impl.LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LegalCase fetchLegalCase(long legalCaseId);

	/**
	 * Returns the legal case matching the UUID and group.
	 *
	 * @param uuid the legal case's UUID
	 * @param groupId the primary key of the group
	 * @return the matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LegalCase fetchLegalCaseByUuidAndGroupId(String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * @param monthRange the number of months to get data for given month range.
	 * @return
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LegalCase> getDateRangeLegalCases(
		int monthRange, String casrRepositoryType);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the legal case with the primary key.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case
	 * @throws PortalException if a legal case with the primary key could not be found
	 */
	@Transactional(
		propagation = Propagation.SUPPORTS, readOnly = true,
		noRollbackFor = NoSuchLegalCaseException.class
	)
	public LegalCase getLegalCase(long legalCaseId) throws PortalException;

	/**
	 * Finds a LegalCase by the id of its countryCategoryId.
	 *
	 * @param countryCategoryId the id of a LegalCase's CountryCategory.
	 * @return the Collection of LegalCases.
	 * @throws NoSuchLegalCaseException if fails to get LegalCase from
	 countryCategoryId.
	 */
	@Transactional(
		propagation = Propagation.SUPPORTS, readOnly = true,
		noRollbackFor = NoSuchLegalCaseException.class
	)
	public List<LegalCase>
			getLegalCaseByCountryCategoryIdAndFilterBySubjectArea(
				long countryCategoryId, long subjectAreaCategoryId,
				int monthRange, String casrRepositoryType)
		throws NoSuchLegalCaseException;

	/**
	 * Finds a LegalCase by the id of its subareaCategoryId.
	 *
	 * @param subareaCategoryId the id of a LegalCase's SubareaCategory.
	 * @return the Collection of LegalCases.
	 * @throws NoSuchLegalCaseException if fails to get LegalCase from
	 subareaCategoryId.
	 */
	@Transactional(
		propagation = Propagation.SUPPORTS, readOnly = true,
		noRollbackFor = NoSuchLegalCaseException.class
	)
	public List<LegalCase> getLegalCaseBySubareaCategoryIdAndFilterByCountry(
			long subareaCategoryId, long countryCategoryId, int monthRange,
			String casrRepositoryType)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the legal case matching the UUID and group.
	 *
	 * @param uuid the legal case's UUID
	 * @param groupId the primary key of the group
	 * @return the matching legal case
	 * @throws PortalException if a matching legal case could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LegalCase getLegalCaseByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns a range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ajp.portal.model.impl.LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of legal cases
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LegalCase> getLegalCases(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LegalCase> getLegalCases(
		String keyword, int start, int end, String orderByCol,
		String orderByType);

	/**
	 * Returns all the legal cases matching the UUID and company.
	 *
	 * @param uuid the UUID of the legal cases
	 * @param companyId the primary key of the company
	 * @return the matching legal cases, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LegalCase> getLegalCasesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of legal cases matching the UUID and company.
	 *
	 * @param uuid the UUID of the legal cases
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching legal cases, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LegalCase> getLegalCasesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator);

	/**
	 * Returns the number of legal cases.
	 *
	 * @return the number of legal cases
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getLegalCasesCount();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Searches legal cases for the given company filtering them by a user and a
	 * keyword and returns only public groups when the given <code>userId</code> is
	 * not positive.
	 *
	 * @param groupId the id of a site to which the legal cases belong.
	 * @param userId  the id of a user who owns the legal cases.
	 * @param keyword the keyword for search in title and description fields.
	 * @param start   the lower bound of the range of legal cases.
	 * @param end     the upper bound of the range of legal cases (not inclusive).
	 * @return the range of found legal cases.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LegalCase> searchLegalCases(
		long groupId, long userId, String keyword, int start, int end);

	/**
	 * Updates the legal case in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param legalCase the legal case
	 * @return the legal case that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public LegalCase updateLegalCase(LegalCase legalCase);

	/**
	 * Updates an existing LegalCase with the given attributes.
	 *
	 * @param themeDisplay      object of ThemeDisplay class.
	 * @param communityGroupId  the id of the LegalCase.
	 * @param title             the id of a user who adds the community group.
	 * @param countryCategoryId the name for the community group.
	 * @param subareaCategoryId the rules for the community group.
	 * @param description       the description for the community group.
	 * @param caseFile          the File object of uploaded document.
	 * @param caseFileName      the File name of uploaded document.
	 * @param serviceContext    the service context to be applied. Can set the UUID,
	 creation date, modification date, expando bridge
	 attributes, guest permissions, group permissions,
	 asset category IDs, asset tag names, asset link
	 entry IDs, URL title, and work flow actions for the
	 web content article. Can also set whether to add the
	 default guest and group permissions.
	 * @return the updated LegalCase.
	 * @throws NoSuchLegalCaseException            if no LegalCase with the given
	 id.
	 * @throws LegalCaseTitleException             if title is blank or too long.
	 * @throws LegalCaseDescriptionException       if description is blank or too
	 long.
	 * @throws LegalCaseDecisionDateException      if decisionDate is null.
	 * @throws LegalCaseCountryCategoryIdException if countryCategoryId is unknown
	 or zero.
	 * @throws LegalCaseSubareaCategoryIdException if same group is already exist.
	 * @throws PortalException                     if something's gone wrong.
	 * @throws IOException
	 */
	@Transactional(
		noRollbackFor = {
			LegalCaseTitleException.class, LegalCaseDescriptionException.class,
			LegalCaseCountryCategoryIdException.class,
			LegalCaseSubareaCategoryIdException.class,
			LegalCaseDecisionDateException.class
		}
	)
	@Indexable(type = IndexableType.REINDEX)
	public LegalCase updateLegalCase(
			ThemeDisplay themeDisplay, long legalCaseId, long countryCategoryId,
			long subareaCategoryId, String title, String url,
			String description, Date decisionDate, File caseSummaryFile,
			String caseSummaryFileName, File caseJudgementFile,
			String caseJudgementFileName, ServiceContext serviceContext)
		throws IOException, PortalException;

}