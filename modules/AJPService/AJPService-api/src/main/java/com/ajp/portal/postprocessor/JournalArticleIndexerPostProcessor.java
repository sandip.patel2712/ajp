package com.ajp.portal.postprocessor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.plaf.ListUI;
import javax.xml.bind.ValidationEvent;

import org.osgi.service.component.annotations.Component;

import com.ajp.portal.constant.AJPConstant;
import com.ajp.portal.util.SAXUtil;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.IndexerPostProcessor;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(
        immediate = true,
        property = {
            "indexer.class.name=com.liferay.journal.model.JournalArticle"
        },
        service = IndexerPostProcessor.class
 )
public class JournalArticleIndexerPostProcessor implements IndexerPostProcessor{

	Log log = LogFactoryUtil.getLog(JournalArticleIndexerPostProcessor.class);
    @Override
    public void postProcessContextBooleanFilter(BooleanFilter booleanFilter, SearchContext searchContext)
            throws Exception {
        
    }

    @Override
    public void postProcessContextQuery(BooleanQuery contextQuery, SearchContext searchContext) throws Exception {
        
    }

    @Override
    public void postProcessDocument(Document document, Object obj) throws Exception {
      
        
        //Index Course Start Date and End Date
        DateFormat df = new SimpleDateFormat(AJPConstant.COURSE_DATE_FORMAT);
        JournalArticle journalArticle = (JournalArticle)obj;
//        System.out.println("indexing contenet " + journalArticle.getTitleCurrentValue() +  "  "+ journalArticle.getArticleId());
        if(journalArticle.getDDMStructure().getName(PortalUtil.getSiteDefaultLocale(journalArticle.getGroupId())).equalsIgnoreCase(AJPConstant.COURSE_STRUCTURE)){
        	 log.info("going to index course journal article");
        	 String courseStartDate = SAXUtil.getValueFromXpath(journalArticle,AJPConstant.COURSE_START_DATE_X_PATH);
             String courseEndDate = SAXUtil.getValueFromXpath(journalArticle,AJPConstant.COURSE_END_DATE_X_PATH);
             if(Validator.isNotNull(courseStartDate) && Validator.isNotNull(courseEndDate)){
                 Date startDate = df.parse(courseStartDate);
                 Date endDate = df.parse(courseEndDate);    
                 document.addDate(AJPConstant.FIELD_COURSE_START_DATE, startDate);
                 document.addDate(AJPConstant.FIELD_COURSE_END_DATE, endDate);
                System.out.println("st date " +document.getDate(AJPConstant.FIELD_COURSE_START_DATE));
                System.out.println("end date " +document.getDate(AJPConstant.FIELD_COURSE_END_DATE));
             }else{
                 throw new Exception("Not valid cousre start date and end date");
             }
             
             // Index Course Categories
             List<AssetCategory> journalArticleCategoryList = AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
             List<Long> journalArticleCategoryIds = new ArrayList<Long>(); 
             for(AssetCategory assetCategory : journalArticleCategoryList){
                 journalArticleCategoryIds.add(assetCategory.getCategoryId());
             }
             Long[] journalArticleCategoryIdsArray = new Long[journalArticleCategoryIds.size()]; 
             journalArticleCategoryIdsArray = journalArticleCategoryIds.toArray(journalArticleCategoryIdsArray);
             document.addNumber(AJPConstant.FIELD_COURSE_CATEGORIES, journalArticleCategoryIdsArray);
             
             // Index Course Cost Type
             String amountStr =SAXUtil.getValueFromXpath(journalArticle,AJPConstant.COURSE_AMOUNT_X_PATH);             
             double courseCostAmount = 0;
             if( Validator.isNotNull(amountStr) && !Validator.isBlank(amountStr)){
            	 courseCostAmount = 1;            	 
             }
             document.addNumber(AJPConstant.FIELD_COURSE_AMOUNT, courseCostAmount);
        }
        
    }

    @Override
    public void postProcessFullQuery(BooleanQuery fullQuery, SearchContext searchContext) throws Exception {
        
    }

    @Override
    public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter booleanFilter,
            SearchContext searchContext) throws Exception {
        
    }

    @Override
    public void postProcessSearchQuery(BooleanQuery searchQuery, SearchContext searchContext) throws Exception {
        
    }

    @Override
    public void postProcessSummary(Summary summary, Document document, Locale locale, String snippet) {
        
    }

}
