package com.ajp.portal.constant;

public class LegalCaseConstant {

	/**
	 * The maximum length of the 'title' property.
	 */
	public static final int TITLE_MAX_LENGTH = 255;
	/**
	 * The maximum length of the 'description' property.
	 */
	public static final int DESCRIPTION_MAX_LENGTH = 30000;
	/**
	 * DlFolder name for store case documents. 
	 */
	public static final String LEGAL_CASE_FOLDER_NAME = "cases";
	/**
	 * ConfigurationPid of LegalCaseConfiguration. 
	 */
	public static final String SERVICE_NAME = "com.ajp.portal";
	/**
	 * Archive type case repository. 
	 */
	public static final String CASE_TYPE_ARCHIVED = "archived";
	/**
	 * Live type case repository.
	 */
	public static final String CASE_TYPE_LIVE = "live";
}