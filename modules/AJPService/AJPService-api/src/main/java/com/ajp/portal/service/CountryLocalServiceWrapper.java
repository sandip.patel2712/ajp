/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CountryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see CountryLocalService
 * @generated
 */
@ProviderType
public class CountryLocalServiceWrapper
	implements CountryLocalService, ServiceWrapper<CountryLocalService> {

	public CountryLocalServiceWrapper(CountryLocalService countryLocalService) {
		_countryLocalService = countryLocalService;
	}

	/**
	 * Adds the country to the database. Also notifies the appropriate model listeners.
	 *
	 * @param country the country
	 * @return the country that was added
	 */
	@Override
	public com.ajp.portal.model.Country addCountry(
		com.ajp.portal.model.Country country) {

		return _countryLocalService.addCountry(country);
	}

	/**
	 * Check Author limit for the user's organization
	 *
	 * @param userId
	 * @return
	 */
	@Override
	public boolean checkAuthorLimit(long userId) {
		return _countryLocalService.checkAuthorLimit(userId);
	}

	/**
	 * Check Reviewer limit for the user's organization
	 *
	 * @param userId
	 * @return
	 */
	@Override
	public boolean checkReviwerLimit(long userId) {
		return _countryLocalService.checkReviwerLimit(userId);
	}

	/**
	 * Creates a new country with the primary key. Does not add the country to the database.
	 *
	 * @param countryId the primary key for the new country
	 * @return the new country
	 */
	@Override
	public com.ajp.portal.model.Country createCountry(long countryId) {
		return _countryLocalService.createCountry(countryId);
	}

	/**
	 * Deletes the country from the database. Also notifies the appropriate model listeners.
	 *
	 * @param country the country
	 * @return the country that was removed
	 */
	@Override
	public com.ajp.portal.model.Country deleteCountry(
		com.ajp.portal.model.Country country) {

		return _countryLocalService.deleteCountry(country);
	}

	/**
	 * Deletes the country with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param countryId the primary key of the country
	 * @return the country that was removed
	 * @throws PortalException if a country with the primary key could not be found
	 */
	@Override
	public com.ajp.portal.model.Country deleteCountry(long countryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _countryLocalService.deleteCountry(countryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _countryLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _countryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _countryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ajp.portal.model.impl.CountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _countryLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ajp.portal.model.impl.CountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _countryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _countryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _countryLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.ajp.portal.model.Country fetchCountry(long countryId) {
		return _countryLocalService.fetchCountry(countryId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _countryLocalService.getActionableDynamicQuery();
	}

	/**
	 * Get active users from given user id list
	 * This function is used for getting active users for assign task to others in workflow
	 *
	 * @param userIds
	 * @return
	 */
	@Override
	public long[] getActiveUserIds(long[] userIds) {
		return _countryLocalService.getActiveUserIds(userIds);
	}

	/**
	 * Set English language first as we don't want to display alphabetically ordered list
	 *
	 * @param siteGroupId
	 * @return
	 */
	@Override
	public java.util.List<java.util.Locale> getAvailableLocales(
		long siteGroupId) {

		return _countryLocalService.getAvailableLocales(siteGroupId);
	}

	/**
	 * Returns a range of all the countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ajp.portal.model.impl.CountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of countries
	 * @param end the upper bound of the range of countries (not inclusive)
	 * @return the range of countries
	 */
	@Override
	public java.util.List<com.ajp.portal.model.Country> getCountries(
		int start, int end) {

		return _countryLocalService.getCountries(start, end);
	}

	/**
	 * Get Country detail
	 */
	@Override
	public java.util.Map<String, com.ajp.portal.bean.CountryBean> getCountries(
		long groupId) {

		return _countryLocalService.getCountries(groupId);
	}

	/**
	 * Returns the number of countries.
	 *
	 * @return the number of countries
	 */
	@Override
	public int getCountriesCount() {
		return _countryLocalService.getCountriesCount();
	}

	/**
	 * Returns the country with the primary key.
	 *
	 * @param countryId the primary key of the country
	 * @return the country
	 * @throws PortalException if a country with the primary key could not be found
	 */
	@Override
	public com.ajp.portal.model.Country getCountry(long countryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _countryLocalService.getCountry(countryId);
	}

	/**
	 * Get country list for footer
	 *
	 * @return
	 */
	@Override
	public java.util.List<com.ajp.portal.bean.CountryBean> getFooterCountryList(
		long groupId) {

		return _countryLocalService.getFooterCountryList(groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _countryLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Get information to reviewer of the web content for the workflow task
	 *
	 * @param companyId
	 * @param tasks
	 * @return
	 */
	@Override
	public java.util.Map<Long, String> getInformationToReviewer(
		long companyId,
		java.util.List<com.liferay.portal.kernel.workflow.WorkflowTask> tasks) {

		return _countryLocalService.getInformationToReviewer(companyId, tasks);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _countryLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _countryLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public java.util.Map<Long, String> getWorkflowTaskAssignee(
		long companyId,
		java.util.List<com.liferay.portal.kernel.workflow.WorkflowInstance>
			workflowInstances) {

		return _countryLocalService.getWorkflowTaskAssignee(
			companyId, workflowInstances);
	}

	@Override
	public boolean isAuthor(long userId) {
		return _countryLocalService.isAuthor(userId);
	}

	@Override
	public boolean isReviewer(long userId) {
		return _countryLocalService.isReviewer(userId);
	}

	@Override
	public boolean isSiteAdmin(long userId) {
		return _countryLocalService.isSiteAdmin(userId);
	}

	/**
	 * Updates the country in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param country the country
	 * @return the country that was updated
	 */
	@Override
	public com.ajp.portal.model.Country updateCountry(
		com.ajp.portal.model.Country country) {

		return _countryLocalService.updateCountry(country);
	}

	@Override
	public CountryLocalService getWrappedService() {
		return _countryLocalService;
	}

	@Override
	public void setWrappedService(CountryLocalService countryLocalService) {
		_countryLocalService = countryLocalService;
	}

	private CountryLocalService _countryLocalService;

}