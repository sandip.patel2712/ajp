package com.ajp.portal.constant;

public class AJPConstant {

	public static final String COUNTRY_STRUCTURE = "Country Details";
	private static final String JOURNAL_ARTICLE_NODE_XPATH_PATTERN = "/root/dynamic-element[@name='?']/dynamic-content";
	
	private static String getJournalArticleXPATH(String nodeName){
		  return JOURNAL_ARTICLE_NODE_XPATH_PATTERN.replace("?", nodeName);
		 }
	
	public static final String COUNTRY_NAME = getJournalArticleXPATH("countryName");
	
	public static final String COUNTRY_FLAG_LINK = getJournalArticleXPATH("countryFlag");
	
	public static final String COUNTRY_MAP_LINK = getJournalArticleXPATH("countryImage");
	
	public static final String COUNTRY_HOME_PAGE_LINK = getJournalArticleXPATH("siteUrl");
	
	public static final String COURSE_START_DATE_X_PATH = getJournalArticleXPATH("courseStartDate");
	public static final String COURSE_END_DATE_X_PATH = getJournalArticleXPATH("courseEndDate");
	public static final String COURSE_TITLE_X_PATH = getJournalArticleXPATH("courseTitle");
	public static final String COURSE_AMOUNT_X_PATH = getJournalArticleXPATH("courseAmount");
	public static final String COURSE_BANNER_IMAGE_X_PATH = getJournalArticleXPATH("courseBannerImage");
	public static final String COURSE_THUMBNAIL_IMAGE_X_PATH = getJournalArticleXPATH("courseThumbnailImage");
	public static final String COURSE_OBJECTIVE_X_PATH = getJournalArticleXPATH("courseObjective");
	
	
	public static final String COURSE_STRUCTURE = "Training Course";
	public static final String FIELD_COURSE_START_DATE = "coursestartdate";
    public static final String FIELD_COURSE_END_DATE = "courseenddate";
    public static final String FIELD_COURSE_CATEGORIES = "courseCategories";
    public static final String FIELD_COURSE_AMOUNT = "courseAmount";
    
    public static final String COUNTRY_VOCAB="Country";
    public static final String SUBJECT_AREA_VOCAB="Subject Area";
    
    public static final String COURSE_DATE_FORMAT="yyyy-MM-dd";
    
    public static final int COURSE_FREE_TYPE=0;
    public static final int COURSE_PAID_TYPE=1;
    public static final int COURSE_COST_ALL_TYPE=-1;
	
}
