package com.ajp.portal.bean;

import java.util.Date;
import java.util.Objects;

import com.liferay.journal.model.JournalArticle;

/**
 * The UI representation for course.
 * 
 */
public class CourseBean {
    private String title;
    private String description;
    private Date courseStartDate;
    private Date courseEndDate;
    private String country;
    private String subjectArea;
    private String courseAmount;
    private String courseBannerImageUrl;
    private String courseThumbnailImageUrl;
    private JournalArticle journalArticle;
    
    public CourseBean(){
        
    }
    
    public CourseBean(JournalArticle article){
        Objects.nonNull(article);
           this.journalArticle = article;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCourseStartDate() {
        return courseStartDate;
    }

    public void setCourseStartDate(Date courseStartDate) {
        this.courseStartDate = courseStartDate;
    }

    public Date getCourseEndDate() {
        return courseEndDate;
    }

    public void setCourseEndDate(Date courseEndDate) {
        this.courseEndDate = courseEndDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSubjectArea() {
        return subjectArea;
    }

    public void setSubjectArea(String subjectArea) {
        this.subjectArea = subjectArea;
    }

    public String getCourseAmount() {
        return courseAmount;
    }

    public void setCourseAmount(String courseAmount) {
        this.courseAmount = courseAmount;
    }

    public String getCourseBannerImageUrl() {
        return courseBannerImageUrl;
    }

    public void setCourseBannerImageUrl(String courseBannerImageUrl) {
        this.courseBannerImageUrl = courseBannerImageUrl;
    }

    public JournalArticle getJournalArticle() {
        return journalArticle;
    }

    public void setJournalArticle(JournalArticle journalArticle) {
        this.journalArticle = journalArticle;
    }

	/**
	 * @return the courseThumbnailImageUrl
	 */
	public String getCourseThumbnailImageUrl() {
		return courseThumbnailImageUrl;
	}

	/**
	 * @param courseThumbnailImageUrl the courseThumbnailImageUrl to set
	 */
	public void setCourseThumbnailImageUrl(String courseThumbnailImageUrl) {
		this.courseThumbnailImageUrl = courseThumbnailImageUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
   
    
}
