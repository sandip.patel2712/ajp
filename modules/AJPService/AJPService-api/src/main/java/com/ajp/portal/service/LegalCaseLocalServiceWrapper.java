/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link LegalCaseLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see LegalCaseLocalService
 * @generated
 */
@ProviderType
public class LegalCaseLocalServiceWrapper
	implements LegalCaseLocalService, ServiceWrapper<LegalCaseLocalService> {

	public LegalCaseLocalServiceWrapper(
		LegalCaseLocalService legalCaseLocalService) {

		_legalCaseLocalService = legalCaseLocalService;
	}

	/**
	 * Adds the legal case to the database. Also notifies the appropriate model listeners.
	 *
	 * @param legalCase the legal case
	 * @return the legal case that was added
	 */
	@Override
	public com.ajp.portal.model.LegalCase addLegalCase(
		com.ajp.portal.model.LegalCase legalCase) {

		return _legalCaseLocalService.addLegalCase(legalCase);
	}

	/**
	 * Adds a new LegalCase with the given attributes.
	 *
	 * @param themeDisplay        object of ThemeDisplay class.
	 * @param title               the id of a user who adds the community group.
	 * @param countryCategoryId   the name for the community group.
	 * @param subareaCategoryId   the rules for the community group.
	 * @param description         the description for the community group.
	 * @param serviceContext      the service context to be applied. Can set the
	 UUID, creation date, modification date, expando
	 bridge attributes, guest permissions, group
	 permissions, asset category IDs, asset tag names,
	 asset link entry IDs, URL title, and work flow
	 actions for the web content article. Can also set
	 whether to add the default guest and group
	 permissions.
	 * @param caseSummaryFile     the File object of uploaded document.
	 * @param caseSummaryFileName the File name of uploaded document.
	 * @return the added LegalCase.
	 * @throws LegalCaseTitleException             if title is blank or too long.
	 * @throws LegalCaseDescriptionException       if description is blank or too
	 long.
	 * @throws LegalCaseDecisionDateException      if decisionDate is null.
	 * @throws LegalCaseCountryCategoryIdException if countryCategoryId is unknown
	 or zero.
	 * @throws LegalCaseSubareaCategoryIdException if same group is already exist.
	 * @throws PortalException                     if something's gone wrong.
	 * @throws IOException
	 */
	@Override
	public com.ajp.portal.model.LegalCase addLegalCase(
			com.liferay.portal.kernel.theme.ThemeDisplay themeDisplay,
			long countryCategoryId, long subareaCategoryId, String title,
			String url, String description, java.util.Date decisionDate,
			java.io.File caseSummaryFile, String caseSummaryFileName,
			java.io.File caseJudgementFile, String caseJudgementFileName,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   java.io.IOException {

		return _legalCaseLocalService.addLegalCase(
			themeDisplay, countryCategoryId, subareaCategoryId, title, url,
			description, decisionDate, caseSummaryFile, caseSummaryFileName,
			caseJudgementFile, caseJudgementFileName, serviceContext);
	}

	/**
	 * Counts legal cases for the given company filtering them by a user and a
	 * keyword and takes into account only public groups when the given
	 * <code>userId</code> is not positive.
	 *
	 * @param groupId the id of a site to which the community groups belong.
	 * @param userId  the id of a user who owns the community groups.
	 * @param keyword the keyword for search in title and description fields.
	 * @return the total number of found legal cases.
	 */
	@Override
	public long countLegalCases(long groupId, long userId, String keyword) {
		return _legalCaseLocalService.countLegalCases(groupId, userId, keyword);
	}

	/**
	 * Creates a new legal case with the primary key. Does not add the legal case to the database.
	 *
	 * @param legalCaseId the primary key for the new legal case
	 * @return the new legal case
	 */
	@Override
	public com.ajp.portal.model.LegalCase createLegalCase(long legalCaseId) {
		return _legalCaseLocalService.createLegalCase(legalCaseId);
	}

	/**
	 * Deletes the legal case from the database. Also notifies the appropriate model listeners.
	 *
	 * @param legalCase the legal case
	 * @return the legal case that was removed
	 */
	@Override
	public com.ajp.portal.model.LegalCase deleteLegalCase(
		com.ajp.portal.model.LegalCase legalCase) {

		return _legalCaseLocalService.deleteLegalCase(legalCase);
	}

	/**
	 * Deletes the legal case with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case that was removed
	 * @throws PortalException if a legal case with the primary key could not be found
	 */
	@Override
	public com.ajp.portal.model.LegalCase deleteLegalCase(long legalCaseId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _legalCaseLocalService.deleteLegalCase(legalCaseId);
	}

	/**
	 * Deletes a LegalCase with the given id.
	 *
	 * @param themeDisplay object of ThemeDisplay class.
	 * @param legalCaseId  the id of a LegalCase to delete.
	 * @return the deleted LegalCase.
	 * @throws NoSuchLegalCaseException if no LegalCase with the given id.
	 * @throws PortalException          if something's gone wrong.
	 */
	@Override
	public com.ajp.portal.model.LegalCase deleteLegalCase(
			com.liferay.portal.kernel.theme.ThemeDisplay themeDisplay,
			long legalCaseId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _legalCaseLocalService.deleteLegalCase(
			themeDisplay, legalCaseId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _legalCaseLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _legalCaseLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _legalCaseLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ajp.portal.model.impl.LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _legalCaseLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ajp.portal.model.impl.LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _legalCaseLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _legalCaseLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _legalCaseLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.ajp.portal.model.LegalCase fetchLegalCase(long legalCaseId) {
		return _legalCaseLocalService.fetchLegalCase(legalCaseId);
	}

	/**
	 * Returns the legal case matching the UUID and group.
	 *
	 * @param uuid the legal case's UUID
	 * @param groupId the primary key of the group
	 * @return the matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public com.ajp.portal.model.LegalCase fetchLegalCaseByUuidAndGroupId(
		String uuid, long groupId) {

		return _legalCaseLocalService.fetchLegalCaseByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _legalCaseLocalService.getActionableDynamicQuery();
	}

	/**
	 * @param monthRange the number of months to get data for given month range.
	 * @return
	 */
	@Override
	public java.util.List<com.ajp.portal.model.LegalCase>
		getDateRangeLegalCases(int monthRange, String casrRepositoryType) {

		return _legalCaseLocalService.getDateRangeLegalCases(
			monthRange, casrRepositoryType);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _legalCaseLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _legalCaseLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the legal case with the primary key.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case
	 * @throws PortalException if a legal case with the primary key could not be found
	 */
	@Override
	public com.ajp.portal.model.LegalCase getLegalCase(long legalCaseId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _legalCaseLocalService.getLegalCase(legalCaseId);
	}

	/**
	 * Finds a LegalCase by the id of its countryCategoryId.
	 *
	 * @param countryCategoryId the id of a LegalCase's CountryCategory.
	 * @return the Collection of LegalCases.
	 * @throws NoSuchLegalCaseException if fails to get LegalCase from
	 countryCategoryId.
	 */
	@Override
	public java.util.List<com.ajp.portal.model.LegalCase>
			getLegalCaseByCountryCategoryIdAndFilterBySubjectArea(
				long countryCategoryId, long subjectAreaCategoryId,
				int monthRange, String casrRepositoryType)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return _legalCaseLocalService.
			getLegalCaseByCountryCategoryIdAndFilterBySubjectArea(
				countryCategoryId, subjectAreaCategoryId, monthRange,
				casrRepositoryType);
	}

	/**
	 * Finds a LegalCase by the id of its subareaCategoryId.
	 *
	 * @param subareaCategoryId the id of a LegalCase's SubareaCategory.
	 * @return the Collection of LegalCases.
	 * @throws NoSuchLegalCaseException if fails to get LegalCase from
	 subareaCategoryId.
	 */
	@Override
	public java.util.List<com.ajp.portal.model.LegalCase>
			getLegalCaseBySubareaCategoryIdAndFilterByCountry(
				long subareaCategoryId, long countryCategoryId, int monthRange,
				String casrRepositoryType)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return _legalCaseLocalService.
			getLegalCaseBySubareaCategoryIdAndFilterByCountry(
				subareaCategoryId, countryCategoryId, monthRange,
				casrRepositoryType);
	}

	/**
	 * Returns the legal case matching the UUID and group.
	 *
	 * @param uuid the legal case's UUID
	 * @param groupId the primary key of the group
	 * @return the matching legal case
	 * @throws PortalException if a matching legal case could not be found
	 */
	@Override
	public com.ajp.portal.model.LegalCase getLegalCaseByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _legalCaseLocalService.getLegalCaseByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ajp.portal.model.impl.LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of legal cases
	 */
	@Override
	public java.util.List<com.ajp.portal.model.LegalCase> getLegalCases(
		int start, int end) {

		return _legalCaseLocalService.getLegalCases(start, end);
	}

	@Override
	public java.util.List<com.ajp.portal.model.LegalCase> getLegalCases(
		String keyword, int start, int end, String orderByCol,
		String orderByType) {

		return _legalCaseLocalService.getLegalCases(
			keyword, start, end, orderByCol, orderByType);
	}

	/**
	 * Returns all the legal cases matching the UUID and company.
	 *
	 * @param uuid the UUID of the legal cases
	 * @param companyId the primary key of the company
	 * @return the matching legal cases, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.ajp.portal.model.LegalCase>
		getLegalCasesByUuidAndCompanyId(String uuid, long companyId) {

		return _legalCaseLocalService.getLegalCasesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of legal cases matching the UUID and company.
	 *
	 * @param uuid the UUID of the legal cases
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching legal cases, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.ajp.portal.model.LegalCase>
		getLegalCasesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.ajp.portal.model.LegalCase> orderByComparator) {

		return _legalCaseLocalService.getLegalCasesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of legal cases.
	 *
	 * @return the number of legal cases
	 */
	@Override
	public int getLegalCasesCount() {
		return _legalCaseLocalService.getLegalCasesCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _legalCaseLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _legalCaseLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Searches legal cases for the given company filtering them by a user and a
	 * keyword and returns only public groups when the given <code>userId</code> is
	 * not positive.
	 *
	 * @param groupId the id of a site to which the legal cases belong.
	 * @param userId  the id of a user who owns the legal cases.
	 * @param keyword the keyword for search in title and description fields.
	 * @param start   the lower bound of the range of legal cases.
	 * @param end     the upper bound of the range of legal cases (not inclusive).
	 * @return the range of found legal cases.
	 */
	@Override
	public java.util.List<com.ajp.portal.model.LegalCase> searchLegalCases(
		long groupId, long userId, String keyword, int start, int end) {

		return _legalCaseLocalService.searchLegalCases(
			groupId, userId, keyword, start, end);
	}

	/**
	 * Updates the legal case in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param legalCase the legal case
	 * @return the legal case that was updated
	 */
	@Override
	public com.ajp.portal.model.LegalCase updateLegalCase(
		com.ajp.portal.model.LegalCase legalCase) {

		return _legalCaseLocalService.updateLegalCase(legalCase);
	}

	/**
	 * Updates an existing LegalCase with the given attributes.
	 *
	 * @param themeDisplay      object of ThemeDisplay class.
	 * @param communityGroupId  the id of the LegalCase.
	 * @param title             the id of a user who adds the community group.
	 * @param countryCategoryId the name for the community group.
	 * @param subareaCategoryId the rules for the community group.
	 * @param description       the description for the community group.
	 * @param caseFile          the File object of uploaded document.
	 * @param caseFileName      the File name of uploaded document.
	 * @param serviceContext    the service context to be applied. Can set the UUID,
	 creation date, modification date, expando bridge
	 attributes, guest permissions, group permissions,
	 asset category IDs, asset tag names, asset link
	 entry IDs, URL title, and work flow actions for the
	 web content article. Can also set whether to add the
	 default guest and group permissions.
	 * @return the updated LegalCase.
	 * @throws NoSuchLegalCaseException            if no LegalCase with the given
	 id.
	 * @throws LegalCaseTitleException             if title is blank or too long.
	 * @throws LegalCaseDescriptionException       if description is blank or too
	 long.
	 * @throws LegalCaseDecisionDateException      if decisionDate is null.
	 * @throws LegalCaseCountryCategoryIdException if countryCategoryId is unknown
	 or zero.
	 * @throws LegalCaseSubareaCategoryIdException if same group is already exist.
	 * @throws PortalException                     if something's gone wrong.
	 * @throws IOException
	 */
	@Override
	public com.ajp.portal.model.LegalCase updateLegalCase(
			com.liferay.portal.kernel.theme.ThemeDisplay themeDisplay,
			long legalCaseId, long countryCategoryId, long subareaCategoryId,
			String title, String url, String description,
			java.util.Date decisionDate, java.io.File caseSummaryFile,
			String caseSummaryFileName, java.io.File caseJudgementFile,
			String caseJudgementFileName,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   java.io.IOException {

		return _legalCaseLocalService.updateLegalCase(
			themeDisplay, legalCaseId, countryCategoryId, subareaCategoryId,
			title, url, description, decisionDate, caseSummaryFile,
			caseSummaryFileName, caseJudgementFile, caseJudgementFileName,
			serviceContext);
	}

	@Override
	public LegalCaseLocalService getWrappedService() {
		return _legalCaseLocalService;
	}

	@Override
	public void setWrappedService(LegalCaseLocalService legalCaseLocalService) {
		_legalCaseLocalService = legalCaseLocalService;
	}

	private LegalCaseLocalService _legalCaseLocalService;

}