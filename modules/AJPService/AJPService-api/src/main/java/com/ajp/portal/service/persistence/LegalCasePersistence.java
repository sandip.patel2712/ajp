/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.ajp.portal.exception.NoSuchLegalCaseException;
import com.ajp.portal.model.LegalCase;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import java.io.Serializable;

import java.util.Map;
import java.util.Set;

/**
 * The persistence interface for the legal case service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LegalCaseUtil
 * @generated
 */
@ProviderType
public interface LegalCasePersistence extends BasePersistence<LegalCase> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LegalCaseUtil} to access the legal case persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */
	@Override
	public Map<Serializable, LegalCase> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys);

	/**
	 * Returns all the legal cases where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching legal cases
	 */
	public java.util.List<LegalCase> findByUuid(String uuid);

	/**
	 * Returns a range of all the legal cases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	public java.util.List<LegalCase> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	public java.util.List<LegalCase> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	public java.util.List<LegalCase> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where uuid = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public LegalCase[] findByUuid_PrevAndNext(
			long legalCaseId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Removes all the legal cases where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of legal cases where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching legal cases
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the legal case where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchLegalCaseException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findByUUID_G(String uuid, long groupId)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the legal case where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the legal case where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache);

	/**
	 * Removes the legal case where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the legal case that was removed
	 */
	public LegalCase removeByUUID_G(String uuid, long groupId)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the number of legal cases where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching legal cases
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching legal cases
	 */
	public java.util.List<LegalCase> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	public java.util.List<LegalCase> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	public java.util.List<LegalCase> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	public java.util.List<LegalCase> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public LegalCase[] findByUuid_C_PrevAndNext(
			long legalCaseId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Removes all the legal cases where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching legal cases
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the legal cases where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @return the matching legal cases
	 */
	public java.util.List<LegalCase> findByCountryCategoryId(
		long countryCategoryId);

	/**
	 * Returns a range of all the legal cases where countryCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	public java.util.List<LegalCase> findByCountryCategoryId(
		long countryCategoryId, int start, int end);

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	public java.util.List<LegalCase> findByCountryCategoryId(
		long countryCategoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	public java.util.List<LegalCase> findByCountryCategoryId(
		long countryCategoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findByCountryCategoryId_First(
			long countryCategoryId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchByCountryCategoryId_First(
		long countryCategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findByCountryCategoryId_Last(
			long countryCategoryId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchByCountryCategoryId_Last(
		long countryCategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public LegalCase[] findByCountryCategoryId_PrevAndNext(
			long legalCaseId, long countryCategoryId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Removes all the legal cases where countryCategoryId = &#63; from the database.
	 *
	 * @param countryCategoryId the country category ID
	 */
	public void removeByCountryCategoryId(long countryCategoryId);

	/**
	 * Returns the number of legal cases where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @return the number of matching legal cases
	 */
	public int countByCountryCategoryId(long countryCategoryId);

	/**
	 * Returns all the legal cases where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @return the matching legal cases
	 */
	public java.util.List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId);

	/**
	 * Returns a range of all the legal cases where subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	public java.util.List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId, int start, int end);

	/**
	 * Returns an ordered range of all the legal cases where subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	public java.util.List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns an ordered range of all the legal cases where subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	public java.util.List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findBySubareaCategoryId_First(
			long subareaCategoryId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the first legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchBySubareaCategoryId_First(
		long subareaCategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns the last legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findBySubareaCategoryId_Last(
			long subareaCategoryId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the last legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchBySubareaCategoryId_Last(
		long subareaCategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public LegalCase[] findBySubareaCategoryId_PrevAndNext(
			long legalCaseId, long subareaCategoryId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Removes all the legal cases where subareaCategoryId = &#63; from the database.
	 *
	 * @param subareaCategoryId the subarea category ID
	 */
	public void removeBySubareaCategoryId(long subareaCategoryId);

	/**
	 * Returns the number of legal cases where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @return the number of matching legal cases
	 */
	public int countBySubareaCategoryId(long subareaCategoryId);

	/**
	 * Returns all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @return the matching legal cases
	 */
	public java.util.List<LegalCase>
		findByCountryCategoryIdAndSubareaCategoryId(
			long countryCategoryId, long subareaCategoryId);

	/**
	 * Returns a range of all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	public java.util.List<LegalCase>
		findByCountryCategoryIdAndSubareaCategoryId(
			long countryCategoryId, long subareaCategoryId, int start, int end);

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	public java.util.List<LegalCase>
		findByCountryCategoryIdAndSubareaCategoryId(
			long countryCategoryId, long subareaCategoryId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator);

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	public java.util.List<LegalCase>
		findByCountryCategoryIdAndSubareaCategoryId(
			long countryCategoryId, long subareaCategoryId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator,
			boolean retrieveFromCache);

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findByCountryCategoryIdAndSubareaCategoryId_First(
			long countryCategoryId, long subareaCategoryId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchByCountryCategoryIdAndSubareaCategoryId_First(
		long countryCategoryId, long subareaCategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public LegalCase findByCountryCategoryIdAndSubareaCategoryId_Last(
			long countryCategoryId, long subareaCategoryId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public LegalCase fetchByCountryCategoryIdAndSubareaCategoryId_Last(
		long countryCategoryId, long subareaCategoryId,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public LegalCase[] findByCountryCategoryIdAndSubareaCategoryId_PrevAndNext(
			long legalCaseId, long countryCategoryId, long subareaCategoryId,
			com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
				orderByComparator)
		throws NoSuchLegalCaseException;

	/**
	 * Removes all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63; from the database.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 */
	public void removeByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId);

	/**
	 * Returns the number of legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @return the number of matching legal cases
	 */
	public int countByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId);

	/**
	 * Caches the legal case in the entity cache if it is enabled.
	 *
	 * @param legalCase the legal case
	 */
	public void cacheResult(LegalCase legalCase);

	/**
	 * Caches the legal cases in the entity cache if it is enabled.
	 *
	 * @param legalCases the legal cases
	 */
	public void cacheResult(java.util.List<LegalCase> legalCases);

	/**
	 * Creates a new legal case with the primary key. Does not add the legal case to the database.
	 *
	 * @param legalCaseId the primary key for the new legal case
	 * @return the new legal case
	 */
	public LegalCase create(long legalCaseId);

	/**
	 * Removes the legal case with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case that was removed
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public LegalCase remove(long legalCaseId) throws NoSuchLegalCaseException;

	public LegalCase updateImpl(LegalCase legalCase);

	/**
	 * Returns the legal case with the primary key or throws a <code>NoSuchLegalCaseException</code> if it could not be found.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public LegalCase findByPrimaryKey(long legalCaseId)
		throws NoSuchLegalCaseException;

	/**
	 * Returns the legal case with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case, or <code>null</code> if a legal case with the primary key could not be found
	 */
	public LegalCase fetchByPrimaryKey(long legalCaseId);

	/**
	 * Returns all the legal cases.
	 *
	 * @return the legal cases
	 */
	public java.util.List<LegalCase> findAll();

	/**
	 * Returns a range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of legal cases
	 */
	public java.util.List<LegalCase> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of legal cases
	 */
	public java.util.List<LegalCase> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator);

	/**
	 * Returns an ordered range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of legal cases
	 */
	public java.util.List<LegalCase> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LegalCase>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Removes all the legal cases from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of legal cases.
	 *
	 * @return the number of legal cases
	 */
	public int countAll();

	@Override
	public Set<String> getBadColumnNames();

}