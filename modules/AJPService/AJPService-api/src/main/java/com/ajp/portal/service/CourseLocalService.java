/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service;

import aQute.bnd.annotation.ProviderType;

import com.ajp.portal.bean.CourseBean;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

import java.text.ParseException;

import java.util.Date;
import java.util.List;

/**
 * Provides the local service interface for Course. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see CourseLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface CourseLocalService extends BaseLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CourseLocalServiceUtil} to access the course local service. Add custom service methods to <code>com.ajp.portal.service.impl.CourseLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<CourseBean> getArchivedCourses(
			long companyId, long groupId, List<Long> seCategories,
			List<Long> countryCategories, int courseCostType, boolean sortOrder,
			int start, int end, SearchContext searchContext)
		throws NumberFormatException, ParseException, PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<CourseBean> getCourseJournalArticles(
			long companyId, long groupId, List<Long> seCategories,
			List<Long> countryCategories, int courseCostType,
			Date courseStartDate, Date courseEndDate, boolean sortOrder,
			int start, int end, SearchContext searchContext)
		throws NumberFormatException, ParseException, PortalException;

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getTotalArchivedCourses(
			long companyId, long groupId, List<Long> seCategories,
			List<Long> countryCategories, int courseCostType, boolean sortOrder,
			SearchContext searchContext)
		throws NumberFormatException, ParseException, PortalException;

}