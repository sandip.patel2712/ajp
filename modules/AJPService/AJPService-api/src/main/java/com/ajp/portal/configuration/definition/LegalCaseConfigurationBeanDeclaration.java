package com.ajp.portal.configuration.definition;

import com.ajp.portal.configuration.LegalCaseConfiguration;
import com.liferay.portal.kernel.settings.definition.ConfigurationBeanDeclaration;

import org.osgi.service.component.annotations.Component;

@Component
public class LegalCaseConfigurationBeanDeclaration implements ConfigurationBeanDeclaration {

	@Override
	public Class<?> getConfigurationBeanClass() {
		return LegalCaseConfiguration.class;
	}
}
