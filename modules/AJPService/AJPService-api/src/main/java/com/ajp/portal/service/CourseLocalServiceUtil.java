/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Course. This utility wraps
 * <code>com.ajp.portal.service.impl.CourseLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see CourseLocalService
 * @generated
 */
@ProviderType
public class CourseLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.ajp.portal.service.impl.CourseLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static java.util.List<com.ajp.portal.bean.CourseBean>
			getArchivedCourses(
				long companyId, long groupId, java.util.List<Long> seCategories,
				java.util.List<Long> countryCategories, int courseCostType,
				boolean sortOrder, int start, int end,
				com.liferay.portal.kernel.search.SearchContext searchContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   java.text.ParseException, NumberFormatException {

		return getService().getArchivedCourses(
			companyId, groupId, seCategories, countryCategories, courseCostType,
			sortOrder, start, end, searchContext);
	}

	public static java.util.List<com.ajp.portal.bean.CourseBean>
			getCourseJournalArticles(
				long companyId, long groupId, java.util.List<Long> seCategories,
				java.util.List<Long> countryCategories, int courseCostType,
				java.util.Date courseStartDate, java.util.Date courseEndDate,
				boolean sortOrder, int start, int end,
				com.liferay.portal.kernel.search.SearchContext searchContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   java.text.ParseException, NumberFormatException {

		return getService().getCourseJournalArticles(
			companyId, groupId, seCategories, countryCategories, courseCostType,
			courseStartDate, courseEndDate, sortOrder, start, end,
			searchContext);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static int getTotalArchivedCourses(
			long companyId, long groupId, java.util.List<Long> seCategories,
			java.util.List<Long> countryCategories, int courseCostType,
			boolean sortOrder,
			com.liferay.portal.kernel.search.SearchContext searchContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   java.text.ParseException, NumberFormatException {

		return getService().getTotalArchivedCourses(
			companyId, groupId, seCategories, countryCategories, courseCostType,
			sortOrder, searchContext);
	}

	public static CourseLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<CourseLocalService, CourseLocalService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(CourseLocalService.class);

		ServiceTracker<CourseLocalService, CourseLocalService> serviceTracker =
			new ServiceTracker<CourseLocalService, CourseLocalService>(
				bundle.getBundleContext(), CourseLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}