package com.ajp.portal.model;

import com.ajp.portal.model.LegalCase;

import java.util.Date;

/**
 * The UI representation of legal cases.
 *
 */
public class LegalCaseBean {

	private long companyId;
	private long groupId;
	private int status;
	private long countryCategoryId;
	private long subareaCategoryId;
	private String subjectArea;
	private String country;
	private long legalCaseId;
	private String title;
	private String url;
	private String description;
	private String legalCaseSummaryURL;
	private String legalCaseSummaryName;
	private String legalCaseJudgementURL;
	private String legalCaseJudgementName;
	private Date decisionDate;

	public LegalCaseBean() {
		super();
	}

	public LegalCaseBean(LegalCase legalCase) {
		this.companyId = legalCase.getCompanyId();
		this.groupId = legalCase.getGroupId();
		this.status = legalCase.getStatus();
		this.title = legalCase.getTitle();
		this.description = legalCase.getDescription();
		this.decisionDate = legalCase.getDecisionDate();
		this.countryCategoryId = legalCase.getCountryCategoryId();
		this.subareaCategoryId = legalCase.getSubareaCategoryId();
		this.legalCaseId = legalCase.getLegalCaseId();
		this.url = legalCase.getUrl();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLegalCaseJudgementURL() {
		return legalCaseJudgementURL;
	}

	public void setLegalCaseJudgementURL(String legalCaseJudgementURL) {
		this.legalCaseJudgementURL = legalCaseJudgementURL;
	}

	public String getLegalCaseJudgementName() {
		return legalCaseJudgementName;
	}

	public void setLegalCaseJudgementName(String legalCaseJudgementName) {
		this.legalCaseJudgementName = legalCaseJudgementName;
	}

	public String getSubjectArea() {
		return subjectArea;
	}

	public void setSubjectArea(String subjectArea) {
		this.subjectArea = subjectArea;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCountryCategoryId() {
		return countryCategoryId;
	}

	public void setCountryCategoryId(long countryCategoryId) {
		this.countryCategoryId = countryCategoryId;
	}

	public long getSubareaCategoryId() {
		return subareaCategoryId;
	}

	public void setSubareaCategoryId(long subareaCategoryId) {
		this.subareaCategoryId = subareaCategoryId;
	}

	public long getLegalCaseId() {
		return legalCaseId;
	}

	public void setLegalCaseId(long legalCaseId) {
		this.legalCaseId = legalCaseId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLegalCaseSummaryURL() {
		return legalCaseSummaryURL;
	}

	public void setLegalCaseSummaryURL(String legalCaseSummaryURL) {
		this.legalCaseSummaryURL = legalCaseSummaryURL;
	}

	public String getLegalCaseSummaryName() {
		return legalCaseSummaryName;
	}

	public void setLegalCaseSummaryName(String legalCaseSummaryName) {
		this.legalCaseSummaryName = legalCaseSummaryName;
	}

	public Date getDecisionDate() {
		return decisionDate;
	}

	public void setDecisionDate(Date decisionDate) {
		this.decisionDate = decisionDate;
	}

}
