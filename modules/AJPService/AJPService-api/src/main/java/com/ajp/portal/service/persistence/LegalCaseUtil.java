/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.ajp.portal.model.LegalCase;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the legal case service. This utility wraps <code>com.ajp.portal.service.persistence.impl.LegalCasePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LegalCasePersistence
 * @generated
 */
@ProviderType
public class LegalCaseUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(LegalCase legalCase) {
		getPersistence().clearCache(legalCase);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, LegalCase> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LegalCase> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LegalCase> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LegalCase> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static LegalCase update(LegalCase legalCase) {
		return getPersistence().update(legalCase);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static LegalCase update(
		LegalCase legalCase, ServiceContext serviceContext) {

		return getPersistence().update(legalCase, serviceContext);
	}

	/**
	 * Returns all the legal cases where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching legal cases
	 */
	public static List<LegalCase> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the legal cases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	public static List<LegalCase> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	public static List<LegalCase> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	public static List<LegalCase> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findByUuid_First(
			String uuid, OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchByUuid_First(
		String uuid, OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findByUuid_Last(
			String uuid, OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchByUuid_Last(
		String uuid, OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where uuid = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public static LegalCase[] findByUuid_PrevAndNext(
			long legalCaseId, String uuid,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByUuid_PrevAndNext(
			legalCaseId, uuid, orderByComparator);
	}

	/**
	 * Removes all the legal cases where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of legal cases where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching legal cases
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the legal case where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchLegalCaseException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findByUUID_G(String uuid, long groupId)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the legal case where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the legal case where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	 * Removes the legal case where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the legal case that was removed
	 */
	public static LegalCase removeByUUID_G(String uuid, long groupId)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of legal cases where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching legal cases
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching legal cases
	 */
	public static List<LegalCase> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	public static List<LegalCase> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	public static List<LegalCase> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	public static List<LegalCase> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public static LegalCase[] findByUuid_C_PrevAndNext(
			long legalCaseId, String uuid, long companyId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByUuid_C_PrevAndNext(
			legalCaseId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the legal cases where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching legal cases
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the legal cases where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @return the matching legal cases
	 */
	public static List<LegalCase> findByCountryCategoryId(
		long countryCategoryId) {

		return getPersistence().findByCountryCategoryId(countryCategoryId);
	}

	/**
	 * Returns a range of all the legal cases where countryCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	public static List<LegalCase> findByCountryCategoryId(
		long countryCategoryId, int start, int end) {

		return getPersistence().findByCountryCategoryId(
			countryCategoryId, start, end);
	}

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	public static List<LegalCase> findByCountryCategoryId(
		long countryCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().findByCountryCategoryId(
			countryCategoryId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	public static List<LegalCase> findByCountryCategoryId(
		long countryCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByCountryCategoryId(
			countryCategoryId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findByCountryCategoryId_First(
			long countryCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByCountryCategoryId_First(
			countryCategoryId, orderByComparator);
	}

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchByCountryCategoryId_First(
		long countryCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().fetchByCountryCategoryId_First(
			countryCategoryId, orderByComparator);
	}

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findByCountryCategoryId_Last(
			long countryCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByCountryCategoryId_Last(
			countryCategoryId, orderByComparator);
	}

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchByCountryCategoryId_Last(
		long countryCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().fetchByCountryCategoryId_Last(
			countryCategoryId, orderByComparator);
	}

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public static LegalCase[] findByCountryCategoryId_PrevAndNext(
			long legalCaseId, long countryCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByCountryCategoryId_PrevAndNext(
			legalCaseId, countryCategoryId, orderByComparator);
	}

	/**
	 * Removes all the legal cases where countryCategoryId = &#63; from the database.
	 *
	 * @param countryCategoryId the country category ID
	 */
	public static void removeByCountryCategoryId(long countryCategoryId) {
		getPersistence().removeByCountryCategoryId(countryCategoryId);
	}

	/**
	 * Returns the number of legal cases where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @return the number of matching legal cases
	 */
	public static int countByCountryCategoryId(long countryCategoryId) {
		return getPersistence().countByCountryCategoryId(countryCategoryId);
	}

	/**
	 * Returns all the legal cases where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @return the matching legal cases
	 */
	public static List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId) {

		return getPersistence().findBySubareaCategoryId(subareaCategoryId);
	}

	/**
	 * Returns a range of all the legal cases where subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	public static List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId, int start, int end) {

		return getPersistence().findBySubareaCategoryId(
			subareaCategoryId, start, end);
	}

	/**
	 * Returns an ordered range of all the legal cases where subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	public static List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().findBySubareaCategoryId(
			subareaCategoryId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the legal cases where subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	public static List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findBySubareaCategoryId(
			subareaCategoryId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	 * Returns the first legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findBySubareaCategoryId_First(
			long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findBySubareaCategoryId_First(
			subareaCategoryId, orderByComparator);
	}

	/**
	 * Returns the first legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchBySubareaCategoryId_First(
		long subareaCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().fetchBySubareaCategoryId_First(
			subareaCategoryId, orderByComparator);
	}

	/**
	 * Returns the last legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findBySubareaCategoryId_Last(
			long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findBySubareaCategoryId_Last(
			subareaCategoryId, orderByComparator);
	}

	/**
	 * Returns the last legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchBySubareaCategoryId_Last(
		long subareaCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().fetchBySubareaCategoryId_Last(
			subareaCategoryId, orderByComparator);
	}

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public static LegalCase[] findBySubareaCategoryId_PrevAndNext(
			long legalCaseId, long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findBySubareaCategoryId_PrevAndNext(
			legalCaseId, subareaCategoryId, orderByComparator);
	}

	/**
	 * Removes all the legal cases where subareaCategoryId = &#63; from the database.
	 *
	 * @param subareaCategoryId the subarea category ID
	 */
	public static void removeBySubareaCategoryId(long subareaCategoryId) {
		getPersistence().removeBySubareaCategoryId(subareaCategoryId);
	}

	/**
	 * Returns the number of legal cases where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @return the number of matching legal cases
	 */
	public static int countBySubareaCategoryId(long subareaCategoryId) {
		return getPersistence().countBySubareaCategoryId(subareaCategoryId);
	}

	/**
	 * Returns all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @return the matching legal cases
	 */
	public static List<LegalCase> findByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId) {

		return getPersistence().findByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId);
	}

	/**
	 * Returns a range of all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	public static List<LegalCase> findByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId, int start, int end) {

		return getPersistence().findByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId, start, end);
	}

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	public static List<LegalCase> findByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().findByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId, start, end,
			orderByComparator);
	}

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	public static List<LegalCase> findByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findByCountryCategoryIdAndSubareaCategoryId_First(
			long countryCategoryId, long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().
			findByCountryCategoryIdAndSubareaCategoryId_First(
				countryCategoryId, subareaCategoryId, orderByComparator);
	}

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchByCountryCategoryIdAndSubareaCategoryId_First(
		long countryCategoryId, long subareaCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().
			fetchByCountryCategoryIdAndSubareaCategoryId_First(
				countryCategoryId, subareaCategoryId, orderByComparator);
	}

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	public static LegalCase findByCountryCategoryIdAndSubareaCategoryId_Last(
			long countryCategoryId, long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().
			findByCountryCategoryIdAndSubareaCategoryId_Last(
				countryCategoryId, subareaCategoryId, orderByComparator);
	}

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	public static LegalCase fetchByCountryCategoryIdAndSubareaCategoryId_Last(
		long countryCategoryId, long subareaCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().
			fetchByCountryCategoryIdAndSubareaCategoryId_Last(
				countryCategoryId, subareaCategoryId, orderByComparator);
	}

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public static LegalCase[]
			findByCountryCategoryIdAndSubareaCategoryId_PrevAndNext(
				long legalCaseId, long countryCategoryId,
				long subareaCategoryId,
				OrderByComparator<LegalCase> orderByComparator)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().
			findByCountryCategoryIdAndSubareaCategoryId_PrevAndNext(
				legalCaseId, countryCategoryId, subareaCategoryId,
				orderByComparator);
	}

	/**
	 * Removes all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63; from the database.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 */
	public static void removeByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId) {

		getPersistence().removeByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId);
	}

	/**
	 * Returns the number of legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @return the number of matching legal cases
	 */
	public static int countByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId) {

		return getPersistence().countByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId);
	}

	/**
	 * Caches the legal case in the entity cache if it is enabled.
	 *
	 * @param legalCase the legal case
	 */
	public static void cacheResult(LegalCase legalCase) {
		getPersistence().cacheResult(legalCase);
	}

	/**
	 * Caches the legal cases in the entity cache if it is enabled.
	 *
	 * @param legalCases the legal cases
	 */
	public static void cacheResult(List<LegalCase> legalCases) {
		getPersistence().cacheResult(legalCases);
	}

	/**
	 * Creates a new legal case with the primary key. Does not add the legal case to the database.
	 *
	 * @param legalCaseId the primary key for the new legal case
	 * @return the new legal case
	 */
	public static LegalCase create(long legalCaseId) {
		return getPersistence().create(legalCaseId);
	}

	/**
	 * Removes the legal case with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case that was removed
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public static LegalCase remove(long legalCaseId)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().remove(legalCaseId);
	}

	public static LegalCase updateImpl(LegalCase legalCase) {
		return getPersistence().updateImpl(legalCase);
	}

	/**
	 * Returns the legal case with the primary key or throws a <code>NoSuchLegalCaseException</code> if it could not be found.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	public static LegalCase findByPrimaryKey(long legalCaseId)
		throws com.ajp.portal.exception.NoSuchLegalCaseException {

		return getPersistence().findByPrimaryKey(legalCaseId);
	}

	/**
	 * Returns the legal case with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case, or <code>null</code> if a legal case with the primary key could not be found
	 */
	public static LegalCase fetchByPrimaryKey(long legalCaseId) {
		return getPersistence().fetchByPrimaryKey(legalCaseId);
	}

	/**
	 * Returns all the legal cases.
	 *
	 * @return the legal cases
	 */
	public static List<LegalCase> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of legal cases
	 */
	public static List<LegalCase> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of legal cases
	 */
	public static List<LegalCase> findAll(
		int start, int end, OrderByComparator<LegalCase> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of legal cases
	 */
	public static List<LegalCase> findAll(
		int start, int end, OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Removes all the legal cases from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of legal cases.
	 *
	 * @return the number of legal cases
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static LegalCasePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LegalCasePersistence, LegalCasePersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(LegalCasePersistence.class);

		ServiceTracker<LegalCasePersistence, LegalCasePersistence>
			serviceTracker =
				new ServiceTracker<LegalCasePersistence, LegalCasePersistence>(
					bundle.getBundleContext(), LegalCasePersistence.class,
					null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}