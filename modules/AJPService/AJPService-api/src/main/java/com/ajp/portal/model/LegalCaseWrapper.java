/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link LegalCase}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LegalCase
 * @generated
 */
@ProviderType
public class LegalCaseWrapper implements LegalCase, ModelWrapper<LegalCase> {

	public LegalCaseWrapper(LegalCase legalCase) {
		_legalCase = legalCase;
	}

	@Override
	public Class<?> getModelClass() {
		return LegalCase.class;
	}

	@Override
	public String getModelClassName() {
		return LegalCase.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("legalCaseId", getLegalCaseId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());
		attributes.put("title", getTitle());
		attributes.put("description", getDescription());
		attributes.put("decisionDate", getDecisionDate());
		attributes.put("countryCategoryId", getCountryCategoryId());
		attributes.put("subareaCategoryId", getSubareaCategoryId());
		attributes.put("summaryFileEntryId", getSummaryFileEntryId());
		attributes.put("judgementFileEntryId", getJudgementFileEntryId());
		attributes.put("url", getUrl());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long legalCaseId = (Long)attributes.get("legalCaseId");

		if (legalCaseId != null) {
			setLegalCaseId(legalCaseId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Date decisionDate = (Date)attributes.get("decisionDate");

		if (decisionDate != null) {
			setDecisionDate(decisionDate);
		}

		Long countryCategoryId = (Long)attributes.get("countryCategoryId");

		if (countryCategoryId != null) {
			setCountryCategoryId(countryCategoryId);
		}

		Long subareaCategoryId = (Long)attributes.get("subareaCategoryId");

		if (subareaCategoryId != null) {
			setSubareaCategoryId(subareaCategoryId);
		}

		Long summaryFileEntryId = (Long)attributes.get("summaryFileEntryId");

		if (summaryFileEntryId != null) {
			setSummaryFileEntryId(summaryFileEntryId);
		}

		Long judgementFileEntryId = (Long)attributes.get(
			"judgementFileEntryId");

		if (judgementFileEntryId != null) {
			setJudgementFileEntryId(judgementFileEntryId);
		}

		String url = (String)attributes.get("url");

		if (url != null) {
			setUrl(url);
		}
	}

	@Override
	public Object clone() {
		return new LegalCaseWrapper((LegalCase)_legalCase.clone());
	}

	@Override
	public int compareTo(LegalCase legalCase) {
		return _legalCase.compareTo(legalCase);
	}

	/**
	 * Returns the company ID of this legal case.
	 *
	 * @return the company ID of this legal case
	 */
	@Override
	public long getCompanyId() {
		return _legalCase.getCompanyId();
	}

	/**
	 * Returns the country category ID of this legal case.
	 *
	 * @return the country category ID of this legal case
	 */
	@Override
	public long getCountryCategoryId() {
		return _legalCase.getCountryCategoryId();
	}

	/**
	 * Returns the create date of this legal case.
	 *
	 * @return the create date of this legal case
	 */
	@Override
	public Date getCreateDate() {
		return _legalCase.getCreateDate();
	}

	/**
	 * Returns the decision date of this legal case.
	 *
	 * @return the decision date of this legal case
	 */
	@Override
	public Date getDecisionDate() {
		return _legalCase.getDecisionDate();
	}

	/**
	 * Returns the description of this legal case.
	 *
	 * @return the description of this legal case
	 */
	@Override
	public String getDescription() {
		return _legalCase.getDescription();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _legalCase.getExpandoBridge();
	}

	/**
	 * Returns the group ID of this legal case.
	 *
	 * @return the group ID of this legal case
	 */
	@Override
	public long getGroupId() {
		return _legalCase.getGroupId();
	}

	/**
	 * Returns the judgement file entry ID of this legal case.
	 *
	 * @return the judgement file entry ID of this legal case
	 */
	@Override
	public long getJudgementFileEntryId() {
		return _legalCase.getJudgementFileEntryId();
	}

	/**
	 * Returns the legal case ID of this legal case.
	 *
	 * @return the legal case ID of this legal case
	 */
	@Override
	public long getLegalCaseId() {
		return _legalCase.getLegalCaseId();
	}

	/**
	 * Returns the modified date of this legal case.
	 *
	 * @return the modified date of this legal case
	 */
	@Override
	public Date getModifiedDate() {
		return _legalCase.getModifiedDate();
	}

	/**
	 * Returns the primary key of this legal case.
	 *
	 * @return the primary key of this legal case
	 */
	@Override
	public long getPrimaryKey() {
		return _legalCase.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _legalCase.getPrimaryKeyObj();
	}

	/**
	 * Returns the status of this legal case.
	 *
	 * @return the status of this legal case
	 */
	@Override
	public int getStatus() {
		return _legalCase.getStatus();
	}

	/**
	 * Returns the status by user ID of this legal case.
	 *
	 * @return the status by user ID of this legal case
	 */
	@Override
	public long getStatusByUserId() {
		return _legalCase.getStatusByUserId();
	}

	/**
	 * Returns the status by user name of this legal case.
	 *
	 * @return the status by user name of this legal case
	 */
	@Override
	public String getStatusByUserName() {
		return _legalCase.getStatusByUserName();
	}

	/**
	 * Returns the status by user uuid of this legal case.
	 *
	 * @return the status by user uuid of this legal case
	 */
	@Override
	public String getStatusByUserUuid() {
		return _legalCase.getStatusByUserUuid();
	}

	/**
	 * Returns the status date of this legal case.
	 *
	 * @return the status date of this legal case
	 */
	@Override
	public Date getStatusDate() {
		return _legalCase.getStatusDate();
	}

	/**
	 * Returns the subarea category ID of this legal case.
	 *
	 * @return the subarea category ID of this legal case
	 */
	@Override
	public long getSubareaCategoryId() {
		return _legalCase.getSubareaCategoryId();
	}

	/**
	 * Returns the summary file entry ID of this legal case.
	 *
	 * @return the summary file entry ID of this legal case
	 */
	@Override
	public long getSummaryFileEntryId() {
		return _legalCase.getSummaryFileEntryId();
	}

	/**
	 * Returns the title of this legal case.
	 *
	 * @return the title of this legal case
	 */
	@Override
	public String getTitle() {
		return _legalCase.getTitle();
	}

	/**
	 * Returns the url of this legal case.
	 *
	 * @return the url of this legal case
	 */
	@Override
	public String getUrl() {
		return _legalCase.getUrl();
	}

	/**
	 * Returns the user ID of this legal case.
	 *
	 * @return the user ID of this legal case
	 */
	@Override
	public long getUserId() {
		return _legalCase.getUserId();
	}

	/**
	 * Returns the user name of this legal case.
	 *
	 * @return the user name of this legal case
	 */
	@Override
	public String getUserName() {
		return _legalCase.getUserName();
	}

	/**
	 * Returns the user uuid of this legal case.
	 *
	 * @return the user uuid of this legal case
	 */
	@Override
	public String getUserUuid() {
		return _legalCase.getUserUuid();
	}

	/**
	 * Returns the uuid of this legal case.
	 *
	 * @return the uuid of this legal case
	 */
	@Override
	public String getUuid() {
		return _legalCase.getUuid();
	}

	@Override
	public int hashCode() {
		return _legalCase.hashCode();
	}

	/**
	 * Returns <code>true</code> if this legal case is approved.
	 *
	 * @return <code>true</code> if this legal case is approved; <code>false</code> otherwise
	 */
	@Override
	public boolean isApproved() {
		return _legalCase.isApproved();
	}

	@Override
	public boolean isCachedModel() {
		return _legalCase.isCachedModel();
	}

	/**
	 * Returns <code>true</code> if this legal case is denied.
	 *
	 * @return <code>true</code> if this legal case is denied; <code>false</code> otherwise
	 */
	@Override
	public boolean isDenied() {
		return _legalCase.isDenied();
	}

	/**
	 * Returns <code>true</code> if this legal case is a draft.
	 *
	 * @return <code>true</code> if this legal case is a draft; <code>false</code> otherwise
	 */
	@Override
	public boolean isDraft() {
		return _legalCase.isDraft();
	}

	@Override
	public boolean isEscapedModel() {
		return _legalCase.isEscapedModel();
	}

	/**
	 * Returns <code>true</code> if this legal case is expired.
	 *
	 * @return <code>true</code> if this legal case is expired; <code>false</code> otherwise
	 */
	@Override
	public boolean isExpired() {
		return _legalCase.isExpired();
	}

	/**
	 * Returns <code>true</code> if this legal case is inactive.
	 *
	 * @return <code>true</code> if this legal case is inactive; <code>false</code> otherwise
	 */
	@Override
	public boolean isInactive() {
		return _legalCase.isInactive();
	}

	/**
	 * Returns <code>true</code> if this legal case is incomplete.
	 *
	 * @return <code>true</code> if this legal case is incomplete; <code>false</code> otherwise
	 */
	@Override
	public boolean isIncomplete() {
		return _legalCase.isIncomplete();
	}

	@Override
	public boolean isNew() {
		return _legalCase.isNew();
	}

	/**
	 * Returns <code>true</code> if this legal case is pending.
	 *
	 * @return <code>true</code> if this legal case is pending; <code>false</code> otherwise
	 */
	@Override
	public boolean isPending() {
		return _legalCase.isPending();
	}

	/**
	 * Returns <code>true</code> if this legal case is scheduled.
	 *
	 * @return <code>true</code> if this legal case is scheduled; <code>false</code> otherwise
	 */
	@Override
	public boolean isScheduled() {
		return _legalCase.isScheduled();
	}

	@Override
	public void persist() {
		_legalCase.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_legalCase.setCachedModel(cachedModel);
	}

	/**
	 * Sets the company ID of this legal case.
	 *
	 * @param companyId the company ID of this legal case
	 */
	@Override
	public void setCompanyId(long companyId) {
		_legalCase.setCompanyId(companyId);
	}

	/**
	 * Sets the country category ID of this legal case.
	 *
	 * @param countryCategoryId the country category ID of this legal case
	 */
	@Override
	public void setCountryCategoryId(long countryCategoryId) {
		_legalCase.setCountryCategoryId(countryCategoryId);
	}

	/**
	 * Sets the create date of this legal case.
	 *
	 * @param createDate the create date of this legal case
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_legalCase.setCreateDate(createDate);
	}

	/**
	 * Sets the decision date of this legal case.
	 *
	 * @param decisionDate the decision date of this legal case
	 */
	@Override
	public void setDecisionDate(Date decisionDate) {
		_legalCase.setDecisionDate(decisionDate);
	}

	/**
	 * Sets the description of this legal case.
	 *
	 * @param description the description of this legal case
	 */
	@Override
	public void setDescription(String description) {
		_legalCase.setDescription(description);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_legalCase.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_legalCase.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_legalCase.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets the group ID of this legal case.
	 *
	 * @param groupId the group ID of this legal case
	 */
	@Override
	public void setGroupId(long groupId) {
		_legalCase.setGroupId(groupId);
	}

	/**
	 * Sets the judgement file entry ID of this legal case.
	 *
	 * @param judgementFileEntryId the judgement file entry ID of this legal case
	 */
	@Override
	public void setJudgementFileEntryId(long judgementFileEntryId) {
		_legalCase.setJudgementFileEntryId(judgementFileEntryId);
	}

	/**
	 * Sets the legal case ID of this legal case.
	 *
	 * @param legalCaseId the legal case ID of this legal case
	 */
	@Override
	public void setLegalCaseId(long legalCaseId) {
		_legalCase.setLegalCaseId(legalCaseId);
	}

	/**
	 * Sets the modified date of this legal case.
	 *
	 * @param modifiedDate the modified date of this legal case
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_legalCase.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_legalCase.setNew(n);
	}

	/**
	 * Sets the primary key of this legal case.
	 *
	 * @param primaryKey the primary key of this legal case
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_legalCase.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_legalCase.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the status of this legal case.
	 *
	 * @param status the status of this legal case
	 */
	@Override
	public void setStatus(int status) {
		_legalCase.setStatus(status);
	}

	/**
	 * Sets the status by user ID of this legal case.
	 *
	 * @param statusByUserId the status by user ID of this legal case
	 */
	@Override
	public void setStatusByUserId(long statusByUserId) {
		_legalCase.setStatusByUserId(statusByUserId);
	}

	/**
	 * Sets the status by user name of this legal case.
	 *
	 * @param statusByUserName the status by user name of this legal case
	 */
	@Override
	public void setStatusByUserName(String statusByUserName) {
		_legalCase.setStatusByUserName(statusByUserName);
	}

	/**
	 * Sets the status by user uuid of this legal case.
	 *
	 * @param statusByUserUuid the status by user uuid of this legal case
	 */
	@Override
	public void setStatusByUserUuid(String statusByUserUuid) {
		_legalCase.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	 * Sets the status date of this legal case.
	 *
	 * @param statusDate the status date of this legal case
	 */
	@Override
	public void setStatusDate(Date statusDate) {
		_legalCase.setStatusDate(statusDate);
	}

	/**
	 * Sets the subarea category ID of this legal case.
	 *
	 * @param subareaCategoryId the subarea category ID of this legal case
	 */
	@Override
	public void setSubareaCategoryId(long subareaCategoryId) {
		_legalCase.setSubareaCategoryId(subareaCategoryId);
	}

	/**
	 * Sets the summary file entry ID of this legal case.
	 *
	 * @param summaryFileEntryId the summary file entry ID of this legal case
	 */
	@Override
	public void setSummaryFileEntryId(long summaryFileEntryId) {
		_legalCase.setSummaryFileEntryId(summaryFileEntryId);
	}

	/**
	 * Sets the title of this legal case.
	 *
	 * @param title the title of this legal case
	 */
	@Override
	public void setTitle(String title) {
		_legalCase.setTitle(title);
	}

	/**
	 * Sets the url of this legal case.
	 *
	 * @param url the url of this legal case
	 */
	@Override
	public void setUrl(String url) {
		_legalCase.setUrl(url);
	}

	/**
	 * Sets the user ID of this legal case.
	 *
	 * @param userId the user ID of this legal case
	 */
	@Override
	public void setUserId(long userId) {
		_legalCase.setUserId(userId);
	}

	/**
	 * Sets the user name of this legal case.
	 *
	 * @param userName the user name of this legal case
	 */
	@Override
	public void setUserName(String userName) {
		_legalCase.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this legal case.
	 *
	 * @param userUuid the user uuid of this legal case
	 */
	@Override
	public void setUserUuid(String userUuid) {
		_legalCase.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this legal case.
	 *
	 * @param uuid the uuid of this legal case
	 */
	@Override
	public void setUuid(String uuid) {
		_legalCase.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<LegalCase>
		toCacheModel() {

		return _legalCase.toCacheModel();
	}

	@Override
	public LegalCase toEscapedModel() {
		return new LegalCaseWrapper(_legalCase.toEscapedModel());
	}

	@Override
	public String toString() {
		return _legalCase.toString();
	}

	@Override
	public LegalCase toUnescapedModel() {
		return new LegalCaseWrapper(_legalCase.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _legalCase.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LegalCaseWrapper)) {
			return false;
		}

		LegalCaseWrapper legalCaseWrapper = (LegalCaseWrapper)obj;

		if (Objects.equals(_legalCase, legalCaseWrapper._legalCase)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _legalCase.getStagedModelType();
	}

	@Override
	public LegalCase getWrappedModel() {
		return _legalCase;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _legalCase.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _legalCase.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_legalCase.resetOriginalValues();
	}

	private final LegalCase _legalCase;

}