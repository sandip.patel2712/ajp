package com.ajp.portal.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
	category = "Case",
	scope = ExtendedObjectClassDefinition.Scope.SYSTEM
)
@Meta.OCD(
	id = "com.ajp.portal.configuration.LegalCaseConfiguration",
	localization = "content/Language",
	name = "Case Configuration"
)
public interface LegalCaseConfiguration {

	@Meta.AD(
		deflt = "4",
		required = false,
		name = "Month Range",
		description = "Enter the number of months you requied"
	)
	public int getMonthRange();
}
