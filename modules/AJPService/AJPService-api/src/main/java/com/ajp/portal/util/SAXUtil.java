package com.ajp.portal.util;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

public class SAXUtil {

	private static final Log LOG = LogFactoryUtil.getLog(SAXUtil.class);

	public static String getValueFromXpath(JournalArticle journalArticle, String xPath) {
		String value = StringPool.BLANK;

		try {
			Document document = SAXReaderUtil
					.read(journalArticle.getContentByLocale(LocaleUtil.toLanguageId(LocaleUtil.getDefault())));
			Node node = document.selectSingleNode(xPath);
			if (Validator.isNotNull(node)) {
				value = node.getText();
			}
		} catch (DocumentException e) {
			LOG.error(e);
		}

		return value;

	}
}
