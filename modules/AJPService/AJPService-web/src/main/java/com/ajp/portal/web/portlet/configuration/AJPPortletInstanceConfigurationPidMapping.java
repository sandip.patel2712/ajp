package com.ajp.portal.web.portlet.configuration;

import com.ajp.portal.web.constants.AJPServicePortletKeys;
import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;

import org.osgi.service.component.annotations.Component;

@Component(service = ConfigurationPidMapping.class)
public class AJPPortletInstanceConfigurationPidMapping implements ConfigurationPidMapping {

	@Override
	public Class<?> getConfigurationBeanClass() {
		return AJPPortletInstanceConfiguration.class;
	}

	@Override
	public String getConfigurationPid() {
		return AJPServicePortletKeys.AJPSERVICE;
	}
}