package com.ajp.portal.web.portlet.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
    category = "Case Configuration",
    scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
)
@Meta.OCD(
    id = "com.ajp.portal.web.portlet.configuration.AJPPortletInstanceConfiguration",
    localization = "content/Language", 
    name = "Case Configuration"
)
public interface AJPPortletInstanceConfiguration {
	@Meta.AD(deflt = "SubjectArea Page Description", required = false)
	public String subjectAreaPageDescription();

	@Meta.AD(deflt = "SubjectArea Page Title", required = false)
	public String subjectAreaPageTitle();

	@Meta.AD(deflt = "Country Page Description", required = false)
	public String countryPageDescription();

	@Meta.AD(deflt = "Country Page Title", required = false)
	public String countryAreaPageTitle();
	
	@Meta.AD(deflt = "4", required = false)
	public int monthRange();
	
	@Meta.AD(required = false)
	public String caseRepositoryType();
}