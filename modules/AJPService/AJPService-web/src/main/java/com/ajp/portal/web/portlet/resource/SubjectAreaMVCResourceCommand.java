package com.ajp.portal.web.portlet.resource;

import com.ajp.portal.exception.NoSuchLegalCaseException;
import com.ajp.portal.model.LegalCase;
import com.ajp.portal.model.LegalCaseBean;
import com.ajp.portal.service.LegalCaseLocalService;
import com.ajp.portal.web.constants.AJPServiceConstants;
import com.ajp.portal.web.constants.AJPServicePortletKeys;
import com.ajp.portal.web.portlet.display.context.LegalCasesDisplayContext;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		immediate = true,
		property = {
	        "javax.portlet.name=" + AJPServicePortletKeys.AJPSERVICE,
	        "mvc.command.name=getSubjectAreaDetails"
	    },
	    service = MVCResourceCommand.class
	)
public class SubjectAreaMVCResourceCommand extends BaseMVCResourceCommand {

	private static Log _log = LogFactoryUtil.getLog(SubjectAreaMVCResourceCommand.class);
	
	private LegalCaseLocalService _legalCaseLocalService;

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws Exception {

		final ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		LegalCasesDisplayContext legalCasesDisplayContext = new LegalCasesDisplayContext();
		List<LegalCaseBean> legalCaseBeans = null;
		Map<Long, String> countryMap = new HashMap<>();
		long countryCategoryId = ParamUtil.getLong(resourceRequest, "countryCategoryId");
		long subjectAreaCategoryId = ParamUtil.getLong(resourceRequest, "subjectAreaCategoryId");
		long pageClicked = ParamUtil.getLong(resourceRequest, "pageClicked");
		int monthRange = ParamUtil.getInteger(resourceRequest, "monthRange");
		String caseRepositoryType = ParamUtil.getString(resourceRequest, "caseRepositoryType");
		String backURL = ParamUtil.getString(resourceRequest, "backURL");
		resourceRequest.setAttribute("backHomeURL", backURL);
		long itemsOnPage = AJPServiceConstants.SUBJECT_AREA_ITEMS_ON_PAGE;
		int totalItems = 0;
		int listStartIndex = (int) ((pageClicked - 1) * itemsOnPage);
		int listEndIndex = (int) (pageClicked * itemsOnPage);
		try {
			List<LegalCase> legalCaseList = _legalCaseLocalService.getLegalCaseBySubareaCategoryIdAndFilterByCountry(subjectAreaCategoryId, countryCategoryId, monthRange, caseRepositoryType);
			totalItems = legalCaseList.size();
			List<LegalCase> modifiableLegalCaseList = new ArrayList<LegalCase>(legalCaseList);
			Collections.sort(modifiableLegalCaseList);
			if(modifiableLegalCaseList.size() < listEndIndex) {
				listEndIndex = modifiableLegalCaseList.size();
			} else if (listStartIndex < 0) {
				listStartIndex = 0;
			}
			legalCaseBeans = legalCasesDisplayContext.convertToLegalCaseBeanList(modifiableLegalCaseList.subList(listStartIndex, listEndIndex), themeDisplay);
		} catch (NoSuchLegalCaseException e) {
			_log.error(e.getMessage(), e);
		}
		for (LegalCaseBean legalCaseBean: legalCaseBeans) {
			AssetCategory country = AssetCategoryLocalServiceUtil.getAssetCategory(legalCaseBean.getCountryCategoryId());
			countryMap.put(legalCaseBean.getLegalCaseId(), country.getName());
		}
        resourceRequest.setAttribute("legalCaseBeansList", legalCaseBeans);
        resourceRequest.setAttribute("countryMap", countryMap);
        resourceRequest.setAttribute("subjectAreatotalItems", totalItems);
        resourceRequest.setAttribute("currentSubjectAreaCategoryId", subjectAreaCategoryId);
        include(resourceRequest, resourceResponse, "/jsp/legal_case_list.jsp");
	}

	@Reference(unbind = "-")
    protected void setLegalCaseService(LegalCaseLocalService legalCaseLocalService) {
		_legalCaseLocalService = legalCaseLocalService;
    }
}
