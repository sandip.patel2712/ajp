package com.ajp.portal.web.portlet;

import com.ajp.portal.web.constants.AJPServicePortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author Pc
 */
@Component(
	immediate = true,
	configurationPid = "com.ajp.portal.web.portlet.configuration.AJPPortletInstanceConfiguration",
	property = {
		"com.liferay.portlet.display-category=AJP",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.header-portlet-css=/css/legalcase-details-page.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=AJP Case Management Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/jsp/view.jsp",
		"javax.portlet.init-param.config-template=/jsp/configuration.jsp",
		"javax.portlet.name=" + AJPServicePortletKeys.AJPSERVICE,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class AJPServicePortlet extends MVCPortlet {
}