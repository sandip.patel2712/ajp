package com.ajp.portal.web.portlet.configuration;

import com.ajp.portal.web.constants.AJPServicePortletKeys;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
	immediate = true,
	property= "javax.portlet.name=" + AJPServicePortletKeys.AJPSERVICE,
	service = ConfigurationAction.class
)
public class AJPConfigurationAction extends DefaultConfigurationAction {

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.ajp.portal.web)", unbind = "-")
	public void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}
}
