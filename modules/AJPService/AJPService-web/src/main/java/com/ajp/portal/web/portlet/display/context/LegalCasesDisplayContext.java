package com.ajp.portal.web.portlet.display.context;

import com.ajp.portal.constant.LegalCaseConstant;
import com.ajp.portal.model.LegalCase;
import com.ajp.portal.model.LegalCaseBean;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.document.library.kernel.util.DLUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringPool;

import java.util.ArrayList;
import java.util.List;

/**
 * The display context for a list of legal cases.
 *
 */
public class LegalCasesDisplayContext {

	/**
	 * Set data in LegalCaseBean from LegalCase model.
	 * 
	 * @throws PortalException
	 */

	public List<LegalCaseBean> convertToLegalCaseBeanList(List<LegalCase> legalCaseList, ThemeDisplay themeDisplay)
			throws PortalException {

		DLFolder caseDlFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(), 0,
				LegalCaseConstant.LEGAL_CASE_FOLDER_NAME);
		List<LegalCaseBean> legalCaseBeanList = new ArrayList<LegalCaseBean>();
		if (legalCaseList.size() > 0) {

			for (LegalCase legalCase : legalCaseList) {
				LegalCaseBean legalCaseBean = new LegalCaseBean();
				String legalCaseSummaryData = getLegalCaseDocumentData(legalCase, caseDlFolder, themeDisplay,
						legalCase.getSummaryFileEntryId());
				String legalCaseJudgementData = getLegalCaseDocumentData(legalCase, caseDlFolder, themeDisplay,
						legalCase.getJudgementFileEntryId());

				if (legalCaseSummaryData != StringPool.BLANK && legalCaseSummaryData != null) {
					String[] legalCaseSummaryDetails = legalCaseSummaryData.split("&&");
					legalCaseBean.setLegalCaseSummaryURL(legalCaseSummaryDetails[0]);
					legalCaseBean.setLegalCaseSummaryName(legalCaseSummaryDetails[1]);
				}
				if (legalCaseJudgementData != StringPool.BLANK && legalCaseJudgementData != null) {
					String[] legalCaseJudgementDetails = legalCaseJudgementData.split("&&");
					legalCaseBean.setLegalCaseJudgementURL(legalCaseJudgementDetails[0]);
					legalCaseBean.setLegalCaseJudgementName(legalCaseJudgementDetails[1]);
				}

				legalCaseBean.setTitle(legalCase.getTitle());
				legalCaseBean.setUrl(legalCase.getUrl());
				legalCaseBean.setCompanyId(legalCase.getCompanyId());
				legalCaseBean.setGroupId(legalCase.getGroupId());
				legalCaseBean.setDescription(legalCase.getDescription());
				legalCaseBean.setDecisionDate(legalCase.getDecisionDate());
				legalCaseBean.setCountryCategoryId(legalCase.getCountryCategoryId());
				legalCaseBean.setSubareaCategoryId(legalCase.getSubareaCategoryId());
				legalCaseBean.setLegalCaseId(legalCase.getLegalCaseId());

				legalCaseBeanList.add(legalCaseBean);
			}
		}
		return legalCaseBeanList;
	}

//	private String getLegalCaseDocumentData(LegalCase legalCase, DLFolder caseDlFolder, ThemeDisplay themeDisplay)
//			throws PortalException {
//		DLFolder caseSubDlFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(),
//				caseDlFolder.getFolderId(), String.valueOf(legalCase.getLegalCaseId()));
//		List<FileEntry> caseFileEntris = null;
//		if (caseSubDlFolder != null) {
//			caseFileEntris = DLAppServiceUtil.getFileEntries(themeDisplay.getCompanyGroupId(),
//					caseSubDlFolder.getFolderId());
//		}
//
//		if (caseFileEntris != null && caseFileEntris.size() > 0) {
//			FileEntry caseFileEntry = caseFileEntris.get(0);
//			String legalCaseDocumentName = caseFileEntry.getTitle();
//			String legalCaseDocumentURL = DLUtil.getPreviewURL(caseFileEntry, caseFileEntry.getFileVersion(),
//					themeDisplay, StringPool.BLANK);
//			return legalCaseDocumentURL + "&&" + legalCaseDocumentName;
//		}
//		return null;
//	}

	private String getLegalCaseDocumentData(LegalCase legalCase, DLFolder caseDlFolder, ThemeDisplay themeDisplay,
			long caseFileEntryId) throws PortalException {
		DLFolder caseSubDlFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(),
				caseDlFolder.getFolderId(), String.valueOf(legalCase.getLegalCaseId()));

		if (caseSubDlFolder != null && caseFileEntryId > 0) {
			FileEntry caseFileEntry = DLAppServiceUtil.getFileEntry(caseFileEntryId);
			String legalCaseDocumentName = caseFileEntry.getTitle();
			String legalCaseDocumentURL = DLUtil.getPreviewURL(caseFileEntry, caseFileEntry.getFileVersion(),
					themeDisplay, StringPool.BLANK);
			return legalCaseDocumentURL + "&&" + legalCaseDocumentName;
		}

		return null;
	}
}