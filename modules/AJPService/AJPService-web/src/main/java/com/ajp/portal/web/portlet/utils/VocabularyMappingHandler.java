package com.ajp.portal.web.portlet.utils;

import com.ajp.portal.web.constants.AJPServiceConstants;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetCategoryProperty;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetCategoryPropertyLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class VocabularyMappingHandler {

	private static Log _log = LogFactoryUtil.getLog(VocabularyMappingHandler.class);

	/**
	 * Get category list by vocabulary name
	 * 
	 * @param groupId
	 * @param vocabularyName
	 * @return
	 */
	public List<AssetCategory> getAssetCategoryListByVocabulary(long groupId, String vocabularyName) {

		AssetVocabulary vocabulary = null;
		List<AssetCategory> assetCategoryList = null;
		List<AssetCategory> emptyList = new ArrayList<AssetCategory>();

		try {
			vocabulary = AssetVocabularyLocalServiceUtil.getGroupVocabulary(groupId, vocabularyName);
		} catch (PortalException e) {
			_log.debug("No vocabulary exist for vocabulary " + vocabularyName);
			_log.error(e.getMessage());
		}

		Comparator<AssetCategory> orderComparator = new Comparator<AssetCategory>() {

			@Override
	         public int compare(AssetCategory c1, AssetCategory c2) {
	        	 AssetCategoryProperty property1= null;
	        	 AssetCategoryProperty property2 = null;
				try {
					property1 = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(c1.getCategoryId(), AJPServiceConstants.CATEGORY_ORDER_KEY);
					property2 = AssetCategoryPropertyLocalServiceUtil.getCategoryProperty(c2.getCategoryId(), AJPServiceConstants.CATEGORY_ORDER_KEY);
					return (Integer.parseInt(property1.getValue()) - Integer.parseInt(property2.getValue()));
				} catch (PortalException e) {
					_log.debug(e.getMessage());
				}

				return 1;
	         }
	     };

		if(Validator.isNotNull(vocabulary)) {
			try {
				assetCategoryList = AssetCategoryLocalServiceUtil.getVocabularyRootCategories(vocabulary.getVocabularyId(), -1, -1, null);
				List<AssetCategory> modifiableassetCategoryList = new ArrayList<AssetCategory>(assetCategoryList);
				Collections.sort(modifiableassetCategoryList, orderComparator);
				return modifiableassetCategoryList;
			} catch (Exception e) {
				_log.info("No asset category exist with vocabulary " + vocabulary.getVocabularyId());
				_log.error(e.getMessage());
			}
		}
		return emptyList;
	}
}