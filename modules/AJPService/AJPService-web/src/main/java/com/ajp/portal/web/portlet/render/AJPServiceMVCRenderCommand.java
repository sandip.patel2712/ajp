package com.ajp.portal.web.portlet.render;

import com.ajp.portal.model.LegalCase;
import com.ajp.portal.model.LegalCaseBean;
import com.ajp.portal.service.LegalCaseLocalService;
import com.ajp.portal.web.constants.AJPServiceConstants;
import com.ajp.portal.web.constants.AJPServicePortletKeys;
import com.ajp.portal.web.portlet.AJPServicePortlet;
import com.ajp.portal.web.portlet.configuration.AJPPortletInstanceConfiguration;
import com.ajp.portal.web.portlet.display.context.LegalCasesDisplayContext;
import com.ajp.portal.web.portlet.utils.VocabularyMappingHandler;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		immediate = true,
		configurationPid = "com.ajp.portal.configuration.LegalCaseConfiguration",
		property = { 
			"javax.portlet.name=" + AJPServicePortletKeys.AJPSERVICE,
			"mvc.command.name=/"
		},
		service = MVCRenderCommand.class
)
public class AJPServiceMVCRenderCommand implements MVCRenderCommand{

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		int monthRange = getMonthRange(renderRequest);
		String caseRepositoryType = getCaseRepositoryType(renderRequest);		
		setSubjectAreaTabDetails(renderRequest, monthRange, caseRepositoryType);
		setCountryTabDetails(renderRequest, monthRange, caseRepositoryType);

		return "/jsp/view.jsp";
	}

	private String getCaseRepositoryType(RenderRequest renderRequest) {
		if (ajpPortletInstanceConfiguration != null) {
			String caseRepositoryType = ajpPortletInstanceConfiguration.caseRepositoryType();
			renderRequest.setAttribute("caseRepositoryType", ajpPortletInstanceConfiguration.caseRepositoryType());
			return caseRepositoryType;
		}
		return StringPool.BLANK;
	}

	private int getMonthRange(RenderRequest renderRequest) {
		final ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		try {
			ajpPortletInstanceConfiguration = themeDisplay.getPortletDisplay().getPortletInstanceConfiguration(AJPPortletInstanceConfiguration.class);
			renderRequest.setAttribute("monthRange", ajpPortletInstanceConfiguration.monthRange());
			return ajpPortletInstanceConfiguration.monthRange();
		} catch (ConfigurationException e) {
			_log.error(e.getMessage(), e);
		}
		return 0;
	}

	private void setCountryTabDetails(RenderRequest renderRequest, int monthRange, String caseRepositoryType) {

		VocabularyMappingHandler vocabularyMappingHandler = new VocabularyMappingHandler();
		final ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		List<AssetCategory> countryCategoriesList = vocabularyMappingHandler.getAssetCategoryListByVocabulary(
				themeDisplay.getCompanyGroupId(), AJPServiceConstants.COUNTRY_CATEGORY_NAME);
		Map<Long, List<LegalCaseBean>> countryCategoryMap = new HashMap<>();
		Map<Long, Integer> totalSubjectAreaRecordsMap = new HashMap<>();
		Map<Long, String> subjectAreaNamesMap = new HashMap<>();

		if (countryCategoriesList.size() > 0) {
			renderRequest.setAttribute("countryCategoriesList", countryCategoriesList);
			for (AssetCategory assetCategory : countryCategoriesList) {
				try {
					List<LegalCase> legalCaseList = _legalCaseLocalService
							.getLegalCaseByCountryCategoryIdAndFilterBySubjectArea(assetCategory.getCategoryId(), 0, monthRange, caseRepositoryType);
					int lastIndex = AJPServiceConstants.COUNTRY_ITEMS_ON_PAGE > legalCaseList.size()
							? legalCaseList.size()
							: AJPServiceConstants.COUNTRY_ITEMS_ON_PAGE;
					List<LegalCaseBean> legalCaseBeanList = legalCasesDisplayContext
							.convertToLegalCaseBeanList(legalCaseList.subList(0, lastIndex), themeDisplay);
					for (LegalCaseBean legalCaseBean: legalCaseBeanList) {
						AssetCategory subjectArea = AssetCategoryLocalServiceUtil.getAssetCategory(legalCaseBean.getSubareaCategoryId());
						subjectAreaNamesMap.put(legalCaseBean.getLegalCaseId(), subjectArea.getName());
					}
					countryCategoryMap.put(assetCategory.getCategoryId(), legalCaseBeanList);
					totalSubjectAreaRecordsMap.put(assetCategory.getCategoryId(), legalCaseList.size());
				} catch (Exception e) {
					_log.error(e.getMessage(), e);
				}
			}
			renderRequest.setAttribute("subjectAreaNamesMap", subjectAreaNamesMap);
			renderRequest.setAttribute("totalSubjectAreaRecordsMap", totalSubjectAreaRecordsMap);
			renderRequest.setAttribute("countryCategoryMap", countryCategoryMap);
		}
	}

	private void setSubjectAreaTabDetails(RenderRequest renderRequest, int monthRange, String caseRepositoryType) {

		VocabularyMappingHandler vocabularyMappingHandler = new VocabularyMappingHandler();
		final ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		List<AssetCategory> subjectAreaCategoriesList = vocabularyMappingHandler.getAssetCategoryListByVocabulary(
				themeDisplay.getCompanyGroupId(), AJPServiceConstants.SUBJECT_AREA_CATEGORY_NAME);
		Map<Long, List<LegalCaseBean>> subjectAreaCategoryMap = new HashMap<>();
		Map<Long, Integer> totalCountryRecordsMap = new HashMap<>();
		Map<Long, String> countryNamesMap = new HashMap<>();

		if (subjectAreaCategoriesList.size() > 0) {
			renderRequest.setAttribute("subjectAreaCategoriesList", subjectAreaCategoriesList);
			for (AssetCategory assetCategory : subjectAreaCategoriesList) {
				try {
					List<LegalCase> legalCaseList = _legalCaseLocalService
							.getLegalCaseBySubareaCategoryIdAndFilterByCountry(assetCategory.getCategoryId(), 0, monthRange, caseRepositoryType);
					int lastIndex = AJPServiceConstants.SUBJECT_AREA_ITEMS_ON_PAGE > legalCaseList.size()
							? legalCaseList.size()
							: AJPServiceConstants.SUBJECT_AREA_ITEMS_ON_PAGE;
					List<LegalCaseBean> legalCaseBeanList = legalCasesDisplayContext
							.convertToLegalCaseBeanList(legalCaseList.subList(0, lastIndex), themeDisplay);
					for (LegalCaseBean legalCaseBean: legalCaseBeanList) {
						AssetCategory subjectArea = AssetCategoryLocalServiceUtil.getAssetCategory(legalCaseBean.getCountryCategoryId());
						countryNamesMap.put(legalCaseBean.getLegalCaseId(), subjectArea.getName());
					}
					subjectAreaCategoryMap.put(assetCategory.getCategoryId(), legalCaseBeanList);
					totalCountryRecordsMap.put(assetCategory.getCategoryId(), legalCaseList.size());
				} catch (Exception e) {
					_log.error(e.getMessage(), e);
				}
			}
			renderRequest.setAttribute("totalCountryRecordsMap", totalCountryRecordsMap);
			renderRequest.setAttribute("subjectAreaCategoryMap", subjectAreaCategoryMap);
			renderRequest.setAttribute("countryNamesMap", countryNamesMap);
		}

	}

	@Reference(unbind = "-")
    protected void setLegalCaseService(LegalCaseLocalService legalCaseLocalService) {
		_legalCaseLocalService = legalCaseLocalService;
    }

	private LegalCaseLocalService _legalCaseLocalService;
	
	private AJPPortletInstanceConfiguration ajpPortletInstanceConfiguration;

	private final LegalCasesDisplayContext legalCasesDisplayContext = new LegalCasesDisplayContext();

	private static Log _log = LogFactoryUtil.getLog(AJPServicePortlet.class);
}
