package com.ajp.portal.web.portlet.render;

import com.ajp.portal.model.LegalCase;
import com.ajp.portal.service.LegalCaseLocalService;
import com.ajp.portal.web.constants.AJPServicePortletKeys;
import com.ajp.portal.web.portlet.AJPServicePortlet;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		immediate = true,
		configurationPid = "com.ajp.portal.configuration.LegalCaseConfiguration",
		property = { 
			"javax.portlet.name=" + AJPServicePortletKeys.AJPSERVICE,
			"mvc.command.name=/view/leagCaseDetail"
		},
		service = MVCRenderCommand.class
)
public class ViewLegalCaseMVCRenderCommand implements MVCRenderCommand{

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		setLegalCaseDetails(renderRequest);

		return "/jsp/view_legal_case_details.jsp";
	}

	private void setLegalCaseDetails(RenderRequest renderRequest) {
		long legalCaseId = ParamUtil.getLong(renderRequest, "legalCaseId");
		String backURL = ParamUtil.getString(renderRequest, "backURL");
		renderRequest.setAttribute("backHomeURL", backURL);
		try {
			LegalCase legalCase = _legalCaseLocalService.getLegalCase(legalCaseId);
			renderRequest.setAttribute("legalCase", legalCase);
		} catch (PortalException e) {
			_log.error(e.getMessage(), e);
		}
	}

	@Reference(unbind = "-")
    protected void setLegalCaseService(LegalCaseLocalService legalCaseLocalService) {
		_legalCaseLocalService = legalCaseLocalService;
    }

	private LegalCaseLocalService _legalCaseLocalService;

	private static Log _log = LogFactoryUtil.getLog(AJPServicePortlet.class);
}
