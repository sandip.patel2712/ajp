package com.ajp.portal.web.constants;

public class AJPServiceConstants {

	public static final String CATEGORY_ORDER_KEY = "order";

	public static final String COUNTRY_CATEGORY_NAME = "Case Repository Country";
	public static final String SUBJECT_AREA_CATEGORY_NAME = "Case Repository Subject Area";

	public static final int SUBJECT_AREA_ITEMS_ON_PAGE = 5;
	public static final int COUNTRY_ITEMS_ON_PAGE = 5;

}
