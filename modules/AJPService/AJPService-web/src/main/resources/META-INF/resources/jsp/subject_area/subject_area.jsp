<%@ include file="/init.jsp"%>

<%
	final AJPPortletInstanceConfiguration ajpPortletInstanceConfiguration = portletDisplay
			.getPortletInstanceConfiguration(AJPPortletInstanceConfiguration.class);
%>

<portlet:resourceURL var="getSubjectAreaDetails" id="getSubjectAreaDetails">
	<portlet:param name="backURL" value="<%=themeDisplay.getURLCurrent()%>"/>
</portlet:resourceURL>

<aui:container id="legal-case-fields" class="row legal-case-fields">
	<div class="row main-content cases">
		<div class="page-header">
			<h3><%=ajpPortletInstanceConfiguration.subjectAreaPageTitle()%></h3>
			<p class="tabtitle"><%=ajpPortletInstanceConfiguration.subjectAreaPageDescription()%></p>
		</div>
		<c:forEach items="${subjectAreaCategoriesList}"
			var="subjectAreaCategory" varStatus="count">
			<aui:fieldset-group markupView="lexicon">
				<div class="text-center pull-right row">
					<div class="form-group ">
						<aui:select class="select" 
							id="countryFilter${subjectAreaCategory.categoryId}" name=""
							onChange="getSubjectAreaPaginationDetails(${subjectAreaCategory.categoryId}, this.value, 1, 1)">
							<aui:option label="All" value="0">
							</aui:option>
							<c:forEach items="${countryCategoriesList}" var="assetCategory">
								<aui:option label="${assetCategory.name}"
									value="${assetCategory.categoryId}">
								</aui:option>
							</c:forEach>
						</aui:select>
					</div>
				</div>
				<h3 class="category-title">${subjectAreaCategory.name}</h3>
				<div id="legalCaseContainer${subjectAreaCategory.categoryId}"
					class="legal-case-container">
					<c:choose>
						<c:when
							test="${fn:length(subjectAreaCategoryMap[subjectAreaCategory.categoryId]) gt 0}">
							<c:forEach
								items="${subjectAreaCategoryMap[subjectAreaCategory.categoryId]}"
								var="legalCaseBean">
								<div class="row">
									<div class="col-sm-7">
										<c:choose>
											<c:when test="${not empty legalCaseBean.url}">
												<a href="${legalCaseBean.url}"><h4>${legalCaseBean.title}</h4></a>
											</c:when>
											<c:otherwise>
												<h4>${legalCaseBean.title}</h4>
											</c:otherwise>
										</c:choose>

										<c:if test="${not empty legalCaseBean.legalCaseJudgementURL}">
											<a href="${legalCaseBean.legalCaseJudgementURL}" target="_blank" style="color:darkblue!important;"> &nbsp;
											<liferay-ui:message key="judgement"/> </a>
										</c:if>
					
										<c:if test="${not empty legalCaseBean.legalCaseSummaryURL}">
											<a href="${legalCaseBean.legalCaseSummaryURL}" target="_blank" style="color:darkblue!important;"> 
												&nbsp;<liferay-ui:message key="case-summary"/>
											</a>
										</c:if>

										<c:set var="currentSubjectAreaPageUrl"  value="<%=themeDisplay.getURLCurrent()%>"></c:set>
										<portlet:renderURL var="viewSubjectAreaLegalCaseDetails">
											<portlet:param name="mvcRenderCommandName" value="/view/leagCaseDetail" />
											<portlet:param name="legalCaseId" value="${legalCaseBean.legalCaseId}" />
											<portlet:param name="backURL" value="${not empty backHomeURL ? backHomeURL : currentSubjectAreaPageUrl}" />
										</portlet:renderURL>
										<div class="case-description more">${legalCaseBean.description}</div>
										<c:set var="subjectAreaDescriptionLength" value="${fn:length(legalCaseBean.description)}" ></c:set>
										<c:if test="${subjectAreaDescriptionLength > 340}">
											<div class="row show-more-link">
												<a href="${viewSubjectAreaLegalCaseDetails }">
													 <liferay-ui:message key="label-show-more" />
												</a>
											</div>
										</c:if>
										<br/>
										<fmt:formatDate pattern="dd MMM yyyy"
											value="${legalCaseBean.decisionDate}" var="decisionDate" />
										<p class="pull-left pad-right">Decision Date:
											${decisionDate}</p>
										<c:set var="countryCategoryId"
											value="${legalCaseBean.countryCategoryId}" />
										<p>Country: ${countryNamesMap[legalCaseBean.legalCaseId]}</p>
									</div>
								</div>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<div >
								<!-- <liferay-ui:message key="no-records" /> -->
							</div>	
						</c:otherwise>
					</c:choose>
				</div>
				<div
					data-totalrecords="${totalCountryRecordsMap[subjectAreaCategory.categoryId]}"
					data-categoryId="${subjectAreaCategory.categoryId}"
					data-portlet-namespace="<portlet:namespace/>">
					<div
						id="subjectAreaPaginationContainer${subjectAreaCategory.categoryId}">
					</div>
				</div>
			</aui:fieldset-group>
		</c:forEach>
	</div>
</aui:container>

<%@ include file="/jsp/subject_area/subject_area_javascript.jsp"%>