<%@ include file="/init.jsp"%>

<c:choose>
	<c:when test="${fn:length(legalCaseBeansList) gt 0}">
		<c:forEach items="${legalCaseBeansList}" var="legalCaseBean">
			<div class="row cases">
				<div class="col-sm-7">
					<c:choose>
						<c:when test="${not empty legalCaseBean.url}">
							<a href="${legalCaseBean.url}"><h4>${legalCaseBean.title}</h4></a>
						</c:when>
						<c:otherwise>
							<h4>${legalCaseBean.title}</h4>
						</c:otherwise>
					</c:choose>

					<c:if test="${not empty legalCaseBean.legalCaseJudgementURL}">
						<a href="${legalCaseBean.legalCaseJudgementURL}" target="_blank" style="color:darkblue!important;"> &nbsp;
						<liferay-ui:message key="judgement"/> </a>
					</c:if>

					<c:if test="${not empty legalCaseBean.legalCaseSummaryURL}">
						<a href="${legalCaseBean.legalCaseSummaryURL}" target="_blank" style="color:darkblue!important;"> 
							&nbsp;<liferay-ui:message key="case-summary"/>
						</a>
					</c:if>

					<c:set var="caseListPageUrl"  value="<%=themeDisplay.getURLCurrent()%>"></c:set>
					<portlet:renderURL var="viewLegalCaseListDetails">
						<portlet:param name="mvcRenderCommandName" value="/view/leagCaseDetail" />
						<portlet:param name="legalCaseId" value="${legalCaseBean.legalCaseId}" />
						<portlet:param name="backURL" value="${not empty backHomeURL ? backHomeURL : caseListPageUrl}" />
					</portlet:renderURL>
					<div class="case-description more">${legalCaseBean.description}</div>
					<c:set var="caseListDescriptionLength" value="${fn:length(legalCaseBean.description)}" ></c:set>
					<c:if test="${caseListDescriptionLength > 340}">
						<div class="row show-more-link">
							<a href="${viewLegalCaseListDetails }">
								 <liferay-ui:message key="label-show-more" />
							</a>
						</div>
					</c:if>
					<br/>
					<fmt:formatDate pattern="dd MMM yyyy"
						value="${legalCaseBean.decisionDate}" var="decisionDate" />
					<p class="pull-left pad-right">Decision Date: ${decisionDate}</p>
					<c:choose>
						<c:when test="${not empty subjectAreaMap}">
							<p>Subject Area: ${subjectAreaMap[legalCaseBean.legalCaseId]}</p>
						</c:when>
						<c:otherwise>
							<p>Country: ${countryMap[legalCaseBean.legalCaseId]}</p>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<div >
				<!-- <liferay-ui:message key="no-records" /> -->
		</div>	
	</c:otherwise>
</c:choose>

<c:if test="${not empty currentCountryCategoryId}">
	<aui:input id="countryTotalItems${currentCountryCategoryId}"
		name="countryTotalItems${currentCountryCategoryId}" type="hidden"
		value="${countryCategoryTotalItems}" />
</c:if>
<c:if test="${not empty currentSubjectAreaCategoryId}">
	<aui:input id="subjectAreaTotalItems${currentSubjectAreaCategoryId}"
		name="subjectAreaTotalItems${currentSubjectAreaCategoryId}"
		type="hidden" value="${subjectAreatotalItems}" />
</c:if>