<%@ include file="../init.jsp"%>
<div class="case-tab">
	<liferay-ui:tabs names='<%="By Subject Area, By Country"%>' refresh="<%=false%>">
		<liferay-ui:section>
			<%@ include file="/jsp/subject_area/subject_area.jsp"%>
		</liferay-ui:section>
		<liferay-ui:section>
			<%@ include file="/jsp/country/country.jsp"%>
		</liferay-ui:section>
	</liferay-ui:tabs>
</div>