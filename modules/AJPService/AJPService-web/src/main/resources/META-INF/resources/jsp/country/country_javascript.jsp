<%@ include file="/init.jsp"%>
<script src="<%= request.getContextPath() %>/js/simplePagination.js"></script>

<script>
$(document).ready(function() {
	var countryPaginationContainers = $("[id*='countryPaginationContainer']");
	countryPaginationContainers.each(function(index) {
		var id = $(this).attr('id');
		var totalPages = $(this).parent().data('totalrecords');

		if (totalPages != 0) {
			$("#"+id).pagination({
		       items: totalPages,
		       itemsOnPage: <%= AJPServiceConstants.COUNTRY_ITEMS_ON_PAGE %>,
		       cssStyle: 'light-theme'
			});
		}
	});
});

 function getCountryPaginationDetails(countryCategoryId, subjectAreaCategoryId, pageClicked, isFilterCall) {
	 AUI().use('aui-io-request',function(A) {
		A.io.request('${getCountryDetails}', {
			method : 'post',
			data : {
				'<portlet:namespace/>countryCategoryId' : countryCategoryId,
				'<portlet:namespace/>subjectAreaCategoryId' : subjectAreaCategoryId,
				'<portlet:namespace/>pageClicked' : pageClicked,
				'<portlet:namespace/>monthRange' : ${monthRange}
			},
			on : {
				success : function() {
					var response = this.get('responseData');
					var legalCaseContainer = document.getElementById("legalCaseContainer"+countryCategoryId);
					legalCaseContainer.innerHTML = response;
					
					if(isFilterCall) {
						var countryPaginationContainer = document.getElementById("countryPaginationContainer"+countryCategoryId);
						countryPaginationContainer.innerHTML = null;
						var totalItems = document.getElementById("<portlet:namespace />countryTotalItems"+countryCategoryId);
						if(legalCaseContainer.innerText == "") {
							//pagination not required
							$("#countryPaginationContainer"+countryCategoryId).removeClass();
						} else {
							$("#countryPaginationContainer"+countryCategoryId).pagination({
							       items: totalItems.value,
							       itemsOnPage: <%= AJPServiceConstants.COUNTRY_ITEMS_ON_PAGE %>,
							       cssStyle: 'light-theme'
							});
						}
					}
				}
			}
		});
	});
  }
</script>