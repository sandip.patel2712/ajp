<%@ include file="/init.jsp"%>
<script src="<%= request.getContextPath() %>/js/simplePagination.js"></script>

<script>
	$(document).ready(function() {
		var subjectAreaPaginationContainers = $("[id*='subjectAreaPaginationContainer']");
		subjectAreaPaginationContainers.each(function(index) {
			var id = $(this).attr('id');
			var totalPages = $(this).parent().data('totalrecords');

			if (totalPages != 0) {
				$("#"+id).pagination({
				       items: totalPages,
				       itemsOnPage: <%= AJPServiceConstants.SUBJECT_AREA_ITEMS_ON_PAGE%>,
				       cssStyle: 'light-theme'
				});
			}			
		});
	});

	 function getSubjectAreaPaginationDetails(subjectAreaCategoryId, countryCategoryId, pageClicked, isFilterCall) {	  
		  AUI().use('aui-io-request',function(A) {
				A.io.request('${getSubjectAreaDetails}', {
					method : 'post',
					data : {
						'<portlet:namespace/>countryCategoryId' : countryCategoryId,
						'<portlet:namespace/>subjectAreaCategoryId' : subjectAreaCategoryId,
						'<portlet:namespace/>pageClicked' : pageClicked,
						'<portlet:namespace/>monthRange' : ${monthRange}
					},
					on : {
						success : function() {
							var response = this.get('responseData');
							var legalCaseContainer = document.getElementById("legalCaseContainer"+subjectAreaCategoryId);
							legalCaseContainer.innerHTML = response;

							if(isFilterCall) {
								var subjectAreaPaginationContainer = document.getElementById("subjectAreaPaginationContainer"+subjectAreaCategoryId);
								subjectAreaPaginationContainer.innerHTML = null;
								var totalItems = document.getElementById("<portlet:namespace />subjectAreaTotalItems"+subjectAreaCategoryId);
								if(legalCaseContainer.innerText == "") {
									//pagination not required
									$("#subjectAreaPaginationContainer"+subjectAreaCategoryId).removeClass();
								} else {
									$("#subjectAreaPaginationContainer"+subjectAreaCategoryId).pagination({
									       items: totalItems.value,
									       itemsOnPage: <%= AJPServiceConstants.SUBJECT_AREA_ITEMS_ON_PAGE %>,
									       cssStyle: 'light-theme'
									});
								}
								
							}
						}
					}
				});
			});
	  }
</script>