<%@ include file="../init.jsp"%>

<%
	final AJPPortletInstanceConfiguration ajpPortletInstanceConfiguration = portletDisplay.getPortletInstanceConfiguration(AJPPortletInstanceConfiguration.class);
    final String countryAreaPageTitle = ajpPortletInstanceConfiguration.countryAreaPageTitle();
    final String countryPageDescription = ajpPortletInstanceConfiguration.countryPageDescription();
    final String subjectAreaPageTitle = ajpPortletInstanceConfiguration.subjectAreaPageTitle();
    final String subjectAreaPageDescription = ajpPortletInstanceConfiguration.subjectAreaPageDescription();
    final String caseRepositoryType = ajpPortletInstanceConfiguration.caseRepositoryType();
    final int monthRange = ajpPortletInstanceConfiguration.monthRange();
%>

<liferay-portlet:actionURL portletConfiguration="<%=true%>"
	var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="<%=true%>"
	var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL%>" method="post" name="fm">
	<aui:input name="<%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />

	<aui:input name="redirect" type="hidden"
		value="<%=configurationRenderURL%>" />

	<aui:fieldset>
		<aui:input type="text" label="label-country-page-title" name="preferences--countryAreaPageTitle--"
			value="<%=countryAreaPageTitle %>">
			<aui:validator name="required" errorMessage="error-required" />
			<aui:validator name="maxLength" errorMessage="error-maxLength">255</aui:validator>
		</aui:input>
		<aui:input type="textarea" label="label-country-page-desc"
			name="preferences--countryPageDescription--" value="<%=countryPageDescription %>">
			<aui:validator name="required" />
			<aui:validator name="maxLength" errorMessage="error-maxLength">3000</aui:validator>
		</aui:input>
		<aui:input type="text" label="label-subject-area-page-title" name="preferences--subjectAreaPageTitle--"
			value="<%=subjectAreaPageTitle %>">
			<aui:validator name="required" errorMessage="error-required" />
			<aui:validator name="maxLength" errorMessage="error-maxLength">255</aui:validator>
		</aui:input>
		<aui:input type="textarea" label="label-subject-area-page-desc"
			name="preferences--subjectAreaPageDescription--" value="<%=subjectAreaPageDescription %>">
			<aui:validator name="required" />
			<aui:validator name="maxLength" errorMessage="error-maxLength">3000</aui:validator>
		</aui:input>
		<aui:input type="text" label="label-case-month-range" name="preferences--monthRange--"
			value="<%=monthRange %>">
			<aui:validator name="required" errorMessage="error-required" />
		</aui:input>
		<aui:select name="preferences--caseRepositoryType--" label="label-case-repository-type">
			<aui:option label="label-live"
				selected="<%= caseRepositoryType.equalsIgnoreCase("live") %>" value="live" />
			<aui:option label="label-archived"
				selected="<%= caseRepositoryType.equalsIgnoreCase("archived") %>" value="archived" />
		</aui:select>
	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit"></aui:button>
	</aui:button-row>
</aui:form>