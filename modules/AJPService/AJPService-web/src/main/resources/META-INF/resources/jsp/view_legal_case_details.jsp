<%@ include file="/init.jsp"%>

<liferay-ui:header title="back" backURL="${backHomeURL}" />

<div class="container-fluid legalcaselistpanel">
	<div class="row content legal-case-info">
		<h2 class="legalcaseheader">${legalCase.title }</h2>
		<fmt:formatDate pattern="EEEE, dd MMM yyyy"
			value="${legalCase.decisionDate}" var="decisionDate" />

		<p class="legalCaseDecisionDate">${decisionDate }</p>
		<br> <br>
		<div class="legalCaseDescription">
			<p>${legalCase.description }</p>
		</div>

	</div>
</div>