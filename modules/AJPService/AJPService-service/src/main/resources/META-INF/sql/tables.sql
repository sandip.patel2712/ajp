create table if not exists ajp_Country (
	countryId LONG not null primary key
);

create table if not exists ajp_LegalCase (
	uuid_ VARCHAR(75) null,
	legalCaseId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null,
	title VARCHAR(255) null,
	description TEXT null,
	decisionDate DATE null,
	countryCategoryId LONG,
	subareaCategoryId LONG,
	summaryFileEntryId LONG,
	judgementFileEntryId LONG,
	url VARCHAR(1000) null
);
