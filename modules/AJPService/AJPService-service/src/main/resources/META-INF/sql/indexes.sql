create index IX_1097A12E on ajp_LegalCase (countryCategoryId, subareaCategoryId);
create index IX_F65D881F on ajp_LegalCase (subareaCategoryId);
create index IX_CF30FA4B on ajp_LegalCase (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_50B9978D on ajp_LegalCase (uuid_[$COLUMN_LENGTH:75$], groupId);