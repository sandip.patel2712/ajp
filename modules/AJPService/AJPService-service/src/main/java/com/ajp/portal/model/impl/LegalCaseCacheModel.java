/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.ajp.portal.model.LegalCase;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing LegalCase in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LegalCaseCacheModel
	implements CacheModel<LegalCase>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LegalCaseCacheModel)) {
			return false;
		}

		LegalCaseCacheModel legalCaseCacheModel = (LegalCaseCacheModel)obj;

		if (legalCaseId == legalCaseCacheModel.legalCaseId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, legalCaseId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", legalCaseId=");
		sb.append(legalCaseId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append(", title=");
		sb.append(title);
		sb.append(", description=");
		sb.append(description);
		sb.append(", decisionDate=");
		sb.append(decisionDate);
		sb.append(", countryCategoryId=");
		sb.append(countryCategoryId);
		sb.append(", subareaCategoryId=");
		sb.append(subareaCategoryId);
		sb.append(", summaryFileEntryId=");
		sb.append(summaryFileEntryId);
		sb.append(", judgementFileEntryId=");
		sb.append(judgementFileEntryId);
		sb.append(", url=");
		sb.append(url);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public LegalCase toEntityModel() {
		LegalCaseImpl legalCaseImpl = new LegalCaseImpl();

		if (uuid == null) {
			legalCaseImpl.setUuid("");
		}
		else {
			legalCaseImpl.setUuid(uuid);
		}

		legalCaseImpl.setLegalCaseId(legalCaseId);
		legalCaseImpl.setGroupId(groupId);
		legalCaseImpl.setCompanyId(companyId);
		legalCaseImpl.setUserId(userId);

		if (userName == null) {
			legalCaseImpl.setUserName("");
		}
		else {
			legalCaseImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			legalCaseImpl.setCreateDate(null);
		}
		else {
			legalCaseImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			legalCaseImpl.setModifiedDate(null);
		}
		else {
			legalCaseImpl.setModifiedDate(new Date(modifiedDate));
		}

		legalCaseImpl.setStatus(status);
		legalCaseImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			legalCaseImpl.setStatusByUserName("");
		}
		else {
			legalCaseImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			legalCaseImpl.setStatusDate(null);
		}
		else {
			legalCaseImpl.setStatusDate(new Date(statusDate));
		}

		if (title == null) {
			legalCaseImpl.setTitle("");
		}
		else {
			legalCaseImpl.setTitle(title);
		}

		if (description == null) {
			legalCaseImpl.setDescription("");
		}
		else {
			legalCaseImpl.setDescription(description);
		}

		if (decisionDate == Long.MIN_VALUE) {
			legalCaseImpl.setDecisionDate(null);
		}
		else {
			legalCaseImpl.setDecisionDate(new Date(decisionDate));
		}

		legalCaseImpl.setCountryCategoryId(countryCategoryId);
		legalCaseImpl.setSubareaCategoryId(subareaCategoryId);
		legalCaseImpl.setSummaryFileEntryId(summaryFileEntryId);
		legalCaseImpl.setJudgementFileEntryId(judgementFileEntryId);

		if (url == null) {
			legalCaseImpl.setUrl("");
		}
		else {
			legalCaseImpl.setUrl(url);
		}

		legalCaseImpl.resetOriginalValues();

		return legalCaseImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		legalCaseId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		status = objectInput.readInt();

		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();
		title = objectInput.readUTF();
		description = objectInput.readUTF();
		decisionDate = objectInput.readLong();

		countryCategoryId = objectInput.readLong();

		subareaCategoryId = objectInput.readLong();

		summaryFileEntryId = objectInput.readLong();

		judgementFileEntryId = objectInput.readLong();
		url = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(legalCaseId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeInt(status);

		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}

		objectOutput.writeLong(decisionDate);

		objectOutput.writeLong(countryCategoryId);

		objectOutput.writeLong(subareaCategoryId);

		objectOutput.writeLong(summaryFileEntryId);

		objectOutput.writeLong(judgementFileEntryId);

		if (url == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(url);
		}
	}

	public String uuid;
	public long legalCaseId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;
	public String title;
	public String description;
	public long decisionDate;
	public long countryCategoryId;
	public long subareaCategoryId;
	public long summaryFileEntryId;
	public long judgementFileEntryId;
	public String url;

}