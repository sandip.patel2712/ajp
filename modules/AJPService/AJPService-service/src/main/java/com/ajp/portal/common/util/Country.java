package com.ajp.portal.common.util;

public enum Country {
	
	BRUNEI ("Singapore"),
	COMBODIA ("Combodia"),
	INDONESIA ("Indonesia"),
	LAOS ("Laos"),
	MALASIA ("Malaysia"),
	MYANMAR ("Myanmar"),
	PHILIPPINES ("Philippines"),
	SINGAPORE ("Singapore"),
	THAILAND ("Thailand"),
	VIETNAM ("Vietnam");
	
	private final String countryCode;
	
	Country(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getCountryCode(){
		return this.countryCode;
	}
}
