package com.ajp.portal.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

/**
 * 
 * @author Chandrakant.Lotke
 *
 */
public class CommonUtil {
	private static final Log LOG = LogFactoryUtil.getLog(CommonUtil.class);

	/**
	 * @param structureName
	 * @return first ddmstructure that matches the name
	 */
	public static Optional<DDMStructure> getFirstStructureByName(String structureName) {
		List<DDMStructure> structureList = new ArrayList<>();
		DynamicQuery structureSearchQuery = DDMStructureLocalServiceUtil.dynamicQuery()
				// TODO:
				// .add(PropertyFactoryUtil.forName(ContentConstant.COLUMN_NAME)
				.add(PropertyFactoryUtil.forName("name").like(StringPool.PERCENT + structureName + StringPool.PERCENT));
		try {
			structureList = DDMStructureLocalServiceUtil.dynamicQuery(structureSearchQuery);
		} catch (SystemException e) {
			LOG.error("error getting structures ", e);
		}
		return structureList.stream().findFirst();
	}

}
