package com.ajp.portal.service.persistence.impl;

import com.ajp.portal.model.LegalCase;
import com.ajp.portal.model.impl.LegalCaseImpl;
import com.ajp.portal.service.persistence.LegalCaseFinder;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.List;

public class LegalCaseFinderImpl extends LegalCaseFinderBaseImpl implements LegalCaseFinder{

	public List<LegalCase> findByAssetCategoryOrder(String keyword, String columnName, String orderByType, int start, int end) {

		Session session = null;
		try {
			session = openSession();

			String sql = "SELECT * FROM ajp_LegalCase JOIN AssetCategory ON ajp_LegalCase."+ columnName +" = AssetCategory.categoryId WHERE (ajp_LegalCase.title LIKE '%"+keyword +"%' OR ajp_LegalCase.description LIKE '%"+ keyword +"%') ORDER BY AssetCategory.name "+ orderByType;
			SQLQuery q = session.createSQLQuery(sql);
			q.addEntity("ajp_LegalCase", LegalCaseImpl.class);
         	q.setCacheable(false);

         	List<LegalCase> legalCases = (List<LegalCase>) QueryUtil.list(q, getDialect(), start, end);
	        return legalCases;

		} catch (Exception e) {
			_log.error(e.getMessage(),e);
		} finally {
	        closeSession(session);
	    }
		return null;
	}

	private static final Log _log = LogFactoryUtil.getLog(LegalCaseFinderImpl.class);
}
