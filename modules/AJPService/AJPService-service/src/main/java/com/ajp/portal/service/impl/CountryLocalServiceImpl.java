/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.ajp.portal.bean.CountryBean;
import com.ajp.portal.bean.impl.CountryBeanImpl;
import com.ajp.portal.common.util.CommonUtil;
import com.ajp.portal.constant.AJPConstant;
import com.ajp.portal.service.base.CountryLocalServiceBaseImpl;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.UserGroupRoleLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowException;
import com.liferay.portal.kernel.workflow.WorkflowInstance;
import com.liferay.portal.kernel.workflow.WorkflowInstanceManagerUtil;
import com.liferay.portal.kernel.workflow.WorkflowTask;
import com.liferay.portal.kernel.workflow.WorkflowTaskManagerUtil;

/**
 * The implementation of the country local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.ajp.portal.service.CountryLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Chandrakant Lotke
 * @see CountryLocalServiceBaseImpl
 * @see com.ajp.portal.service.CountryLocalServiceUtil
 */
public class CountryLocalServiceImpl extends CountryLocalServiceBaseImpl {

	private static final Log _log = LogFactoryUtil.getLog(CountryLocalServiceImpl.class);

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * com.ajp.portal.service.CountryLocalServiceUtil} to access the country local
	 * service.
	 */
	/**
	 * Get Country detail
	 */
	public Map<String, CountryBean> getCountries(long groupId) {
		Map<String, CountryBean> countryMap = new HashMap<String,CountryBean>();
		DDMStructure countryStructure = getCountryStructure();		
		if (Validator.isNotNull(countryStructure)) {
			List<JournalArticle> journalArticles = JournalArticleLocalServiceUtil.getArticlesByStructureId(groupId,
					countryStructure.getStructureKey(), WorkflowConstants.STATUS_APPROVED, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null);
			for (JournalArticle journalArticle : journalArticles) {
				CountryBean countryBean = new CountryBeanImpl(journalArticle);
				countryMap.put(countryBean.getCountryName(), countryBean);
			}
		} else {
			_log.debug("Country Structure not available");
		}		
		return countryMap;
	}
	
	/**
	 * Get country list for footer
	 * @return
	 */
	public List<CountryBean> getFooterCountryList(long groupId) {
		List<CountryBean> countryList = new ArrayList<CountryBean>();
		DDMStructure countryStructure = getCountryStructure();
		if (Validator.isNotNull(countryStructure)) {
			List<JournalArticle> journalArticles = JournalArticleLocalServiceUtil.getArticlesByStructureId(groupId,
					countryStructure.getStructureKey(), WorkflowConstants.STATUS_APPROVED, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null);
			for (JournalArticle journalArticle : journalArticles) {
				CountryBean countryBean = new CountryBeanImpl(journalArticle);
				countryList.add(countryBean);
			}
			
			countryList.sort((c1,c2)->c1.getCountryName().compareTo(c2.getCountryName()));
		} else {
			_log.debug("Country Structure not available");
		}		
		return countryList;
	}
	
	/**
	 * Set English language first as we don't want to display alphabetically ordered list
	 * 
	 * @param siteGroupId
	 * @return
	 */
	public List<Locale> getAvailableLocales(long siteGroupId){
		List<Locale>  locales = new ArrayList<>();
		Set<Locale> availableLocales = LanguageUtil.getAvailableLocales(siteGroupId);	
		//add english language in first
		for (Locale locale : availableLocales) {
			if(locale.getDisplayLanguage() == "English"){
				locales.add(locale);				
			}
		}		
		//add rest of language in alphabetically order
		for (Locale localeObj : availableLocales) {
			if(localeObj.getDisplayLanguage() != "English"){
				locales.add(localeObj);				
			}
		}		
				
		return locales;
		
	}
	
	/**
	 * Check Author limit for the user's organization
	 *  
	 * @param userId
	 * @return
	 */
	public boolean checkAuthorLimit(long userId){
		
		int authorLimit = 0;
		int authorCount = 0;		
		boolean isAuthorLimitExceed = false;
		
		if(userId > 0){	
			 try{
				 User user = UserLocalServiceUtil.getUser(userId);
				 List<Organization> orgs = user.getOrganizations();
					
				 Organization userOrg = (orgs.size() > 0)?orgs.get(0):null;
				 if(Validator.isNotNull(userOrg)){
					 authorLimit = (int)userOrg.getExpandoBridge().getAttribute("Author Limit");
					 List<User> users = UserLocalServiceUtil.getOrganizationUsers(userOrg.getOrganizationId());
					
					for(User userObj: users){
						if(userObj.isActive()){
							List<UserGroupRole> currentUserGroupRoles = UserGroupRoleLocalServiceUtil.getUserGroupRoles(userObj.getUserId());
							for (UserGroupRole groupRole : currentUserGroupRoles) {
								Role role = RoleLocalServiceUtil.getRole(groupRole.getRoleId());						
								if(role.getTitleCurrentValue().equals("Author")){
									authorCount++;
								}
							}
					  }
					}		
				 }
				
					
			 }catch (PortalException nsoe) {}

			if(authorLimit <= authorCount ){
				isAuthorLimitExceed = true;				
			}
		}
		return isAuthorLimitExceed;
	}
	/**
	 *  Check Reviewer limit for the user's organization
	 *  
	 * @param userId
	 * @return
	 */
	public boolean checkReviwerLimit(long userId){
		
		int reviewerLimit = 0;
		int reviewerCount = 0;		
		boolean isReviewerLimitLimitExceed = false;
		
		if(userId > 0){//if organization view	
			 try{
				 User user = UserLocalServiceUtil.getUser(userId);
				 List<Organization> orgs = user.getOrganizations();
					
				 Organization userOrg = (orgs.size() > 0)?orgs.get(0):null;
				 if(Validator.isNotNull(userOrg)){
					 reviewerLimit = (int)userOrg.getExpandoBridge().getAttribute("Reviewer Limit ");					 
					 List<User> users = UserLocalServiceUtil.getOrganizationUsers(userOrg.getOrganizationId());
					
					for(User userObj: users){
						if(userObj.isActive()){
							List<UserGroupRole> currentUserGroupRoles = UserGroupRoleLocalServiceUtil.getUserGroupRoles(userObj.getUserId());
							for (UserGroupRole groupRole : currentUserGroupRoles) {
								Role role = RoleLocalServiceUtil.getRole(groupRole.getRoleId());						
								if(role.getTitleCurrentValue().equals("Reviewer")){
									reviewerCount++;
								}							
							}
						}
					}		
				 }				
					
			 }catch (PortalException nsoe) {}

			if(reviewerLimit <= reviewerCount ){
				isReviewerLimitLimitExceed = true;				
			}
		}
		return isReviewerLimitLimitExceed;
	}
	
	public boolean isAuthor(long userId){
		boolean isAuthor=false;
		List<UserGroupRole> currentUserGroupRoles = UserGroupRoleLocalServiceUtil.getUserGroupRoles(userId);
		for (UserGroupRole groupRole : currentUserGroupRoles) {
			Role role;
			try {
				role = RoleLocalServiceUtil.getRole(groupRole.getRoleId());
				if(role.getTitleCurrentValue().equals("Author")){
					isAuthor = true;
				}				
			} catch (PortalException e) {
				_log.error(e);
			}
			 
		}
		return isAuthor;
		
	}
	
	public boolean isReviewer(long userId){

		boolean isReviewer=false;
		List<UserGroupRole> currentUserGroupRoles = UserGroupRoleLocalServiceUtil.getUserGroupRoles(userId);
		for (UserGroupRole groupRole : currentUserGroupRoles) {
			Role role;
			try {
				role = RoleLocalServiceUtil.getRole(groupRole.getRoleId());
				if(role.getTitleCurrentValue().equals("Reviewer")){
					isReviewer = true;
				}				
			} catch (PortalException e) {
				_log.error(e);
			}
			 
		}
		return isReviewer;		
	}
	
	public boolean isSiteAdmin(long userId){

		boolean siteAdmin = false;
		List<UserGroupRole> currentUserGroupRoles = UserGroupRoleLocalServiceUtil.getUserGroupRoles(userId);
		for (UserGroupRole groupRole : currentUserGroupRoles) {
			Role role;
			try {
				role = RoleLocalServiceUtil.getRole(groupRole.getRoleId());				
				if(role.getTitleCurrentValue().equals("Site Admin")){
					siteAdmin = true;
				}
			} catch (PortalException e) {
				_log.error(e);
			}
			 
		}
		return siteAdmin;
		
	}
	
	/**
	 * Get active users from given user id list
	 * This function is used for getting active users for assign task to others in workflow 
	 * 
	 * @param userIds
	 * @return
	 */
	public long[] getActiveUserIds(long[] userIds){
		
			
		DynamicQuery query = UserLocalServiceUtil.dynamicQuery();
		query.add(PropertyFactoryUtil.forName("userId").in(userIds));
		
		List<User> users = UserLocalServiceUtil.dynamicQuery(query);
		List<Long> activeUsersIds = users.stream()
										.filter(p->p.isActive())
										.map(p->p.getUserId())
										.collect(Collectors.toList());
		long[] activeUsers = new long[activeUsersIds.size()];
		int i = 0;
		for (long id : activeUsersIds) {
			activeUsers[i++] =id;
		}
		
		return activeUsers;
		
	}
	
	/**
	 * Get information to reviewer of the web content for the workflow task
	 * 
	 * @param companyId
	 * @param tasks
	 * @return
	 */
	public Map<Long, String> getInformationToReviewer(long companyId,List<WorkflowTask> tasks){
		Map<Long, String>  informationToReviwer = new HashMap<>();
		String summary = StringPool.BLANK;		
		for (WorkflowTask workflowTask : tasks) {
			
			Map<String, Serializable> workflowContext = getWorkflowContext(workflowTask,companyId);

				long entryClassPk =  GetterUtil.getLong((String)workflowContext.get(WorkflowConstants.CONTEXT_ENTRY_CLASS_PK));
				try {
					summary = JournalArticleLocalServiceUtil.getArticle(entryClassPk).getDescriptionCurrentValue();
					informationToReviwer.put(workflowTask.getWorkflowTaskId(), summary);
				} catch (PortalException e) {
					_log.error(e);
				}
		}
		return informationToReviwer;		
	} 
	
	protected Map<String, Serializable> getWorkflowContext(WorkflowTask workflowTask,long companyId){
		Map<String, Serializable> workflowContext = null;
		
		try {
			WorkflowInstance workflowInstance = WorkflowInstanceManagerUtil.getWorkflowInstance(companyId, workflowTask.getWorkflowInstanceId());
			workflowContext  = workflowInstance.getWorkflowContext();		
			
		} catch (WorkflowException e) {
			_log.error(e);			
		} 
		return workflowContext;
		
	}
	
	
	public Map<Long, String> getWorkflowTaskAssignee(long companyId,List<WorkflowInstance> workflowInstances){
		Map<Long, String>  taskAssignee = new HashMap<>();
		for (WorkflowInstance workflowInstance : workflowInstances) {
			try {
				List<WorkflowTask> tasks = WorkflowTaskManagerUtil.getWorkflowTasksByWorkflowInstance(companyId, null, workflowInstance.getWorkflowInstanceId(), null, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);				
				if(tasks.size()>0 && tasks.get(0).getAssigneeUserId() > 0){
					if(tasks.size() == 1){
						
						taskAssignee.put(workflowInstance.getWorkflowInstanceId(), getUser(tasks.get(0).getAssigneeUserId()).getFullName());
					}else{	
						String userName = StringPool.BLANK;
							
						
						Optional<WorkflowTask> pendingTask=  tasks.stream().filter(p->!p.isCompleted()).findFirst();
						if(pendingTask.isPresent()){//Find pending task and get assignee of that task
							userName = PortalUtil.getUserName(pendingTask.get().getAssigneeUserId(), StringPool.BLANK);//taskAssignee.put(workflowInstance.getWorkflowInstanceId(), PortalUtil.getUserName(pendingTask.get().getAssigneeUserId(), StringPool.BLANK) );
						}else{			//get first user from completed task user assignees											
								for (WorkflowTask workflowTask : tasks) {
									if(workflowTask.isAssignedToSingleUser() && workflowTask.isCompleted()){
										userName =  PortalUtil.getUserName(workflowTask.getAssigneeUserId(), StringPool.BLANK);
										break;
									}									
								}
							}
						
							if(userName.equalsIgnoreCase(StringPool.BLANK)){
								userName = "None";
							}
							
						taskAssignee.put(workflowInstance.getWorkflowInstanceId(), userName );												
					}
										
				}else{
					taskAssignee.put(workflowInstance.getWorkflowInstanceId(), "None");
				}
			} catch (WorkflowException e) {
				_log.error(e);
			} catch (PortalException e) {
				_log.error(e);
			}
			
		}
		return taskAssignee;
	}
	
	protected User getUser(long userId) throws PortalException {
		User user = UserLocalServiceUtil.getUser(userId);		
		return user;
	}
	
	/**
	 * Get country Structure 
	 * @return
	 */
	private DDMStructure getCountryStructure(){
		Optional<DDMStructure> countryStructure = CommonUtil.getFirstStructureByName(AJPConstant.COUNTRY_STRUCTURE);
		DDMStructure ddmStructure = null;
		if(countryStructure.isPresent())
		{
			ddmStructure = countryStructure.get();
		}		
		return ddmStructure;
	}
	
	/**
	 * Get Global site group id
	 * @return
	 */
	private long getDefaultGroupId(){
		long groupId = 0l;
		try {
			groupId = GroupLocalServiceUtil.getCompanyGroup(PortalUtil.getDefaultCompanyId()).getGroupId();		
		} catch (PortalException e) {
			_log.error(e.getMessage());
		}	
		return groupId;
	}
}