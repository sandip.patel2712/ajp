package com.ajp.portal.bean.impl;

import com.ajp.portal.bean.CountryBean;
import com.ajp.portal.constant.AJPConstant;
import com.ajp.portal.util.SAXUtil;
import com.liferay.journal.model.JournalArticle;

public class CountryBeanImpl implements CountryBean {
	private String countryName;
	private String countryFlagLink;
	private String countryMapLink;
	private String countryHomePageLink;

	public CountryBeanImpl() {
		super();
	}
	public CountryBeanImpl(JournalArticle article) {
		super();
		this.countryName = SAXUtil.getValueFromXpath(article,AJPConstant.COUNTRY_NAME);
		this.countryFlagLink = SAXUtil.getValueFromXpath(article,AJPConstant.COUNTRY_FLAG_LINK);
		this.countryMapLink = SAXUtil.getValueFromXpath(article,AJPConstant.COUNTRY_MAP_LINK);
		this.countryHomePageLink = SAXUtil.getValueFromXpath(article,AJPConstant.COUNTRY_HOME_PAGE_LINK);
	}
	@Override
	public String getCountryName() {
		return this.countryName;
	}

	@Override
	public String getCountryFlagLink() {
		return this.countryFlagLink;
	}

	@Override
	public String getCountryMapLink() {
		return this.countryMapLink;
	}

	@Override
	public String getCountryHomePageLink() {
		return this.countryHomePageLink;
	}

	@Override
	public void setCountryName(String countryName) {
		this.countryName = countryName;

	}

	@Override
	public void setCountryFlagLink(String countryFlagLink) {
		this.countryHomePageLink = countryFlagLink;

	}

	@Override
	public void setCountryMapLink(String countryMapLink) {
		this.countryMapLink = countryMapLink;

	}

	@Override
	public void setCountryHomePageLink(String countryHomePageLink) {
		this.countryHomePageLink = countryHomePageLink;

	}

}
