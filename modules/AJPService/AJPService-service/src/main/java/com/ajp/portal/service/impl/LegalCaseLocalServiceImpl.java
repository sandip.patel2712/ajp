/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service.impl;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.ajp.portal.constant.LegalCaseConstant;
import com.ajp.portal.exception.LegalCaseCountryCategoryIdException;
import com.ajp.portal.exception.LegalCaseDecisionDateException;
import com.ajp.portal.exception.LegalCaseDescriptionException;
import com.ajp.portal.exception.LegalCaseSubareaCategoryIdException;
import com.ajp.portal.exception.LegalCaseTitleException;
import com.ajp.portal.exception.NoSuchLegalCaseException;
import com.ajp.portal.model.LegalCase;
import com.ajp.portal.service.LegalCaseLocalServiceUtil;
import com.ajp.portal.service.base.LegalCaseLocalServiceBaseImpl;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * The implementation of the legal case local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.ajp.portal.service.LegalCaseLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LegalCaseLocalServiceBaseImpl
 */
public class LegalCaseLocalServiceImpl extends LegalCaseLocalServiceBaseImpl {

	private static final Log _log = LogFactoryUtil.getLog(LegalCaseLocalServiceImpl.class);

	/**
	 * Adds a new LegalCase with the given attributes.
	 *
	 * @param themeDisplay        object of ThemeDisplay class.
	 * @param title               the id of a user who adds the community group.
	 * @param countryCategoryId   the name for the community group.
	 * @param subareaCategoryId   the rules for the community group.
	 * @param description         the description for the community group.
	 * @param serviceContext      the service context to be applied. Can set the
	 *                            UUID, creation date, modification date, expando
	 *                            bridge attributes, guest permissions, group
	 *                            permissions, asset category IDs, asset tag names,
	 *                            asset link entry IDs, URL title, and work flow
	 *                            actions for the web content article. Can also set
	 *                            whether to add the default guest and group
	 *                            permissions.
	 * @param caseSummaryFile     the File object of uploaded document.
	 * @param caseSummaryFileName the File name of uploaded document.
	 * @return the added LegalCase.
	 * @throws LegalCaseTitleException             if title is blank or too long.
	 * @throws LegalCaseDescriptionException       if description is blank or too
	 *                                             long.
	 * @throws LegalCaseDecisionDateException      if decisionDate is null.
	 * @throws LegalCaseCountryCategoryIdException if countryCategoryId is unknown
	 *                                             or zero.
	 * @throws LegalCaseSubareaCategoryIdException if same group is already exist.
	 * @throws PortalException                     if something's gone wrong.
	 * @throws IOException
	 */
	@Transactional(noRollbackFor = { LegalCaseTitleException.class, LegalCaseDescriptionException.class,
			LegalCaseCountryCategoryIdException.class, LegalCaseSubareaCategoryIdException.class,
			LegalCaseDecisionDateException.class })
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public LegalCase addLegalCase(ThemeDisplay themeDisplay, long countryCategoryId, long subareaCategoryId,
			String title, String url, String description, Date decisionDate, File caseSummaryFile, String caseSummaryFileName,
			File caseJudgementFile, String caseJudgementFileName, ServiceContext serviceContext)
			throws PortalException, IOException {

		if (Validator.isNotNull(serviceContext)) {
			validateTitle(title);
			validateDescription(description);
			validateCountryCategoryId(countryCategoryId);
			validateSubareaCategoryId(subareaCategoryId);
			validateDecisionDate(decisionDate);

			User user = userLocalService.getUserById(serviceContext.getUserId());
			long repositoryId = themeDisplay.getCompanyGroupId();

			long legalCaseId = counterLocalService.increment(LegalCase.class.getName());
			LegalCase legalCase = legalCaseLocalService.createLegalCase(legalCaseId);

			if (caseSummaryFile != null && caseSummaryFileName != StringPool.BLANK) {
				long caseSummaryFileEntryId = uploadDocument(legalCase, repositoryId, themeDisplay, serviceContext,
						caseSummaryFile, caseSummaryFileName);
				if (caseSummaryFileEntryId != 0)
					legalCase.setSummaryFileEntryId(caseSummaryFileEntryId);
			}
			if (caseJudgementFile != null && caseJudgementFileName != StringPool.BLANK) {
				long caseJudgementFileEntryId = uploadDocument(legalCase, repositoryId, themeDisplay, serviceContext,
						caseJudgementFile, caseJudgementFileName);
				if (caseJudgementFileEntryId != 0)
					legalCase.setJudgementFileEntryId(caseJudgementFileEntryId);
			}

			legalCase.setCompanyId(serviceContext.getCompanyId());
			legalCase.setTitle(title);
			legalCase.setUrl(url);
			legalCase.setDescription(description);
			legalCase.setCountryCategoryId(countryCategoryId);
			legalCase.setSubareaCategoryId(subareaCategoryId);
			legalCase.setDecisionDate(decisionDate);
			legalCase.setUserName(user.getFullName());
			legalCase.setCreateDate(new Date());
			legalCase.setModifiedDate(new Date());
			legalCase.setGroupId(serviceContext.getScopeGroupId());
			legalCase.setStatus(WorkflowConstants.STATUS_APPROVED);
			legalCase = legalCaseLocalService.addLegalCase(legalCase);

			return legalCase;
		}
		return null;
	}

	private long uploadDocument(LegalCase legalCase, long repositoryId, ThemeDisplay themeDisplay,
			ServiceContext serviceContext, File caseFile, String caseFileName) {
		InputStream caseDocumentInputstream = null;
		String caseMimeType = MimeTypesUtil.getContentType(caseFile.getName());
		DLFolder caseDlFolder = DLFolderLocalServiceUtil.fetchFolder(repositoryId, 0,
				LegalCaseConstant.LEGAL_CASE_FOLDER_NAME);

		try {
			Role guest = RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(), RoleConstants.GUEST);

			if (Validator.isNull(caseDlFolder)) {

				caseDlFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), repositoryId, repositoryId,
						false, 0, LegalCaseConstant.LEGAL_CASE_FOLDER_NAME, StringPool.BLANK, false, serviceContext);
			}

			ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),
					DLFolder.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
					String.valueOf(caseDlFolder.getFolderId()), guest.getRoleId(), new String[] { ActionKeys.VIEW });

			DLFolder caseSubDlFolder = DLFolderLocalServiceUtil.fetchFolder(repositoryId, caseDlFolder.getFolderId(),
					String.valueOf(legalCase.getLegalCaseId()));
			if (Validator.isNull(caseSubDlFolder)) {

				caseSubDlFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), repositoryId,
						repositoryId, false, caseDlFolder.getFolderId(), String.valueOf(legalCase.getLegalCaseId()),
						StringPool.BLANK, false, serviceContext);
			}
			caseDocumentInputstream = new FileInputStream(caseFile);

			FileEntry fileEntry = DLAppServiceUtil.addFileEntry(repositoryId, caseSubDlFolder.getFolderId(),
					caseFileName, caseMimeType, caseFileName, StringPool.BLANK, StringPool.BLANK,
					caseDocumentInputstream, caseFile.length(), serviceContext);

			ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),
					DLFileEntry.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
					String.valueOf(fileEntry.getFileEntryId()), guest.getRoleId(), new String[] { ActionKeys.VIEW });
			ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),
					DLFolder.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
					String.valueOf(caseSubDlFolder.getFolderId()), guest.getRoleId(), new String[] { ActionKeys.VIEW });

			return fileEntry.getFileEntryId();
		} catch (PortalException | FileNotFoundException e) {
			_log.error(e.getMessage(), e);
		} finally {
			if (caseDocumentInputstream != null) {
				try {
					caseDocumentInputstream.close();
				} catch (IOException e) {
					_log.error(e.getMessage(), e);
				}
			}
		}
		return 0;
	}

	/**
	 * Updates an existing LegalCase with the given attributes.
	 *
	 * @param themeDisplay      object of ThemeDisplay class.
	 * @param communityGroupId  the id of the LegalCase.
	 * @param title             the id of a user who adds the community group.
	 * @param countryCategoryId the name for the community group.
	 * @param subareaCategoryId the rules for the community group.
	 * @param description       the description for the community group.
	 * @param caseFile          the File object of uploaded document.
	 * @param caseFileName      the File name of uploaded document.
	 * @param serviceContext    the service context to be applied. Can set the UUID,
	 *                          creation date, modification date, expando bridge
	 *                          attributes, guest permissions, group permissions,
	 *                          asset category IDs, asset tag names, asset link
	 *                          entry IDs, URL title, and work flow actions for the
	 *                          web content article. Can also set whether to add the
	 *                          default guest and group permissions.
	 * @return the updated LegalCase.
	 * @throws NoSuchLegalCaseException            if no LegalCase with the given
	 *                                             id.
	 * @throws LegalCaseTitleException             if title is blank or too long.
	 * @throws LegalCaseDescriptionException       if description is blank or too
	 *                                             long.
	 * @throws LegalCaseDecisionDateException      if decisionDate is null.
	 * @throws LegalCaseCountryCategoryIdException if countryCategoryId is unknown
	 *                                             or zero.
	 * @throws LegalCaseSubareaCategoryIdException if same group is already exist.
	 * @throws PortalException                     if something's gone wrong.
	 * @throws IOException
	 */
	@Transactional(noRollbackFor = { LegalCaseTitleException.class, LegalCaseDescriptionException.class,
			LegalCaseCountryCategoryIdException.class, LegalCaseSubareaCategoryIdException.class,
			LegalCaseDecisionDateException.class })
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public LegalCase updateLegalCase(ThemeDisplay themeDisplay, long legalCaseId, long countryCategoryId,
			long subareaCategoryId, String title, String url, String description, Date decisionDate, File caseSummaryFile,
			String caseSummaryFileName, File caseJudgementFile, String caseJudgementFileName,
			ServiceContext serviceContext) throws PortalException, IOException {

		if (Validator.isNotNull(serviceContext)) {
			validateTitle(title);
			validateDescription(description);
			validateCountryCategoryId(countryCategoryId);
			validateSubareaCategoryId(subareaCategoryId);
			validateDecisionDate(decisionDate);

			long repositoryId = themeDisplay.getCompanyGroupId();
			LegalCase legalCase = getLegalCase(legalCaseId);

			if (caseSummaryFile != null && caseSummaryFileName != StringPool.BLANK) {
				long caseSummaryFileEntryId = editDocument(legalCase, repositoryId, themeDisplay, serviceContext,
						caseSummaryFile, caseSummaryFileName, legalCase.getSummaryFileEntryId());
				if (caseSummaryFileEntryId != 0)
					legalCase.setSummaryFileEntryId(caseSummaryFileEntryId);
			}
			if (caseJudgementFile != null && caseJudgementFileName != StringPool.BLANK) {
				long caseJudgementFileEntryId = editDocument(legalCase, repositoryId, themeDisplay, serviceContext,
						caseJudgementFile, caseJudgementFileName, legalCase.getJudgementFileEntryId());
				if (caseJudgementFileEntryId != 0)
					legalCase.setJudgementFileEntryId(caseJudgementFileEntryId);
			}

			legalCase.setTitle(title);
			legalCase.setUrl(url);
			legalCase.setDescription(description);
			legalCase.setCountryCategoryId(countryCategoryId);
			legalCase.setSubareaCategoryId(subareaCategoryId);
			legalCase.setDecisionDate(decisionDate);
			legalCaseLocalService.updateLegalCase(legalCase);

			return legalCase;
		}
		return null;
	}

	private long editDocument(LegalCase legalCase, long repositoryId, ThemeDisplay themeDisplay,
			ServiceContext serviceContext, File caseFile, String caseFileName, long documentFileEntryId)
			throws IOException, PortalException {
		String caseMimeType = MimeTypesUtil.getContentType(caseFile.getName());
		InputStream caseDocumentInputstream = null;

		try {
			Role guest = RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(), RoleConstants.GUEST);
			DLFolder caseDlFolder = DLFolderLocalServiceUtil.fetchFolder(repositoryId, 0,
					LegalCaseConstant.LEGAL_CASE_FOLDER_NAME);

			if (Validator.isNull(caseDlFolder)) {
				caseDlFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), repositoryId, repositoryId,
						false, 0, LegalCaseConstant.LEGAL_CASE_FOLDER_NAME, StringPool.BLANK, false, serviceContext);
				ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),
						DLFolder.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
						String.valueOf(caseDlFolder.getFolderId()), guest.getRoleId(),
						new String[] { ActionKeys.VIEW });
			}

			DLFolder caseSubDlFolder = DLFolderLocalServiceUtil.fetchFolder(repositoryId, caseDlFolder.getFolderId(),
					String.valueOf(legalCase.getLegalCaseId()));

			if (Validator.isNull(caseSubDlFolder)) {
				caseSubDlFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), repositoryId,
						repositoryId, false, caseDlFolder.getFolderId(), String.valueOf(legalCase.getLegalCaseId()),
						StringPool.BLANK, false, serviceContext);
			}

			caseDocumentInputstream = new FileInputStream(caseFile);
			_log.info("user.getFullName()::" + themeDisplay.getUser().getFullName() + " : folder id:"
					+ caseSubDlFolder.getFolderId());

			if (documentFileEntryId != 0) {
				FileEntry fileEntry = DLAppServiceUtil.updateFileEntry(documentFileEntryId,
						caseFileName, caseMimeType, caseFileName, StringPool.BLANK, StringPool.BLANK,
						true, caseFile, serviceContext);
				return fileEntry.getFileEntryId();
			} else {
				FileEntry fileEntry = DLAppServiceUtil.addFileEntry(repositoryId, caseSubDlFolder.getFolderId(),
						caseFileName, caseMimeType, caseFileName, StringPool.BLANK, StringPool.BLANK,
						caseDocumentInputstream, caseFile.length(), serviceContext);

				ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),
						DLFileEntry.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
						String.valueOf(fileEntry.getFileEntryId()), guest.getRoleId(),
						new String[] { ActionKeys.VIEW });
				ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),
						DLFolder.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
						String.valueOf(caseSubDlFolder.getFolderId()), guest.getRoleId(),
						new String[] { ActionKeys.VIEW });
				return fileEntry.getFileEntryId();
			}
		} catch (PortalException | FileNotFoundException e) {
			_log.error(e.getMessage(), e);
		} finally {
			if (caseDocumentInputstream != null) {
				caseDocumentInputstream.close();
			}
		}
		return 0;
	}

	/**
	 * Deletes a LegalCase with the given id.
	 *
	 * @param themeDisplay object of ThemeDisplay class.
	 * @param legalCaseId  the id of a LegalCase to delete.
	 * @return the deleted LegalCase.
	 * @throws NoSuchLegalCaseException if no LegalCase with the given id.
	 * @throws PortalException          if something's gone wrong.
	 */
	@Transactional(noRollbackFor = NoSuchLegalCaseException.class)
	@Indexable(type = IndexableType.DELETE)
	@Override
	public LegalCase deleteLegalCase(ThemeDisplay themeDisplay, long legalCaseId) throws PortalException {
		LegalCase legalCase = getLegalCase(legalCaseId);
		deleteDLFolder(themeDisplay, legalCase);
		return legalCaseLocalService.deleteLegalCase(legalCase);
	}

	private void deleteDLFolder(ThemeDisplay themeDisplay, LegalCase legalCase) throws PortalException {
		DLFolder caseDlFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(), 0,
				LegalCaseConstant.LEGAL_CASE_FOLDER_NAME);
		if (caseDlFolder != null) {
			DLFolder caseSubDlFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(),
					caseDlFolder.getFolderId(), String.valueOf(legalCase.getLegalCaseId()));
			if(caseSubDlFolder != null) {
				DLFolderLocalServiceUtil.deleteFolder(caseSubDlFolder, true);
			}
		}
	}

	/**
	 * Retrieves a legal case by its id.
	 *
	 * @param legalCaseId the id of a LegalCase to retrieve.
	 * @return the retrieved LegalCase.
	 * @throws NoSuchLegalCaseException if no LegalCase with the given id.
	 * @throws PortalException          if something's gone wrong.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, noRollbackFor = NoSuchLegalCaseException.class)
	@Override
	public LegalCase getLegalCase(long legalCaseId) throws PortalException {
		return legalCasePersistence.findByPrimaryKey(legalCaseId);
	}

	/**
	 * Finds a LegalCase by the id of its countryCategoryId.
	 *
	 * @param countryCategoryId the id of a LegalCase's CountryCategory.
	 * @return the Collection of LegalCases.
	 * @throws NoSuchLegalCaseException if fails to get LegalCase from
	 *                                  countryCategoryId.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, noRollbackFor = NoSuchLegalCaseException.class)
	@Override
	public List<LegalCase> getLegalCaseByCountryCategoryIdAndFilterBySubjectArea(long countryCategoryId,
			long subjectAreaCategoryId, int monthRange, String casrRepositoryType) throws NoSuchLegalCaseException {
		List<LegalCase> legalCases = new ArrayList<LegalCase>();
		if (subjectAreaCategoryId == 0) {
			legalCases = legalCasePersistence.findByCountryCategoryId(countryCategoryId);
		} else {
			legalCases = legalCasePersistence.findByCountryCategoryIdAndSubareaCategoryId(countryCategoryId,
					subjectAreaCategoryId);
		}
		List<LegalCase> monthRangelegalCases = getDateRangeLegalCases(monthRange, casrRepositoryType);
		List<LegalCase> monthRangelegalCaseList = new ArrayList<LegalCase>();
		for (LegalCase legalCase : legalCases) {
			if (monthRangelegalCases.contains(legalCase)) {
				monthRangelegalCaseList.add(legalCase);
			}
		}
		return monthRangelegalCaseList;
	}

	/**
	 * Finds a LegalCase by the id of its subareaCategoryId.
	 *
	 * @param subareaCategoryId the id of a LegalCase's SubareaCategory.
	 * @return the Collection of LegalCases.
	 * @throws NoSuchLegalCaseException if fails to get LegalCase from
	 *                                  subareaCategoryId.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, noRollbackFor = NoSuchLegalCaseException.class)
	@Override
	public List<LegalCase> getLegalCaseBySubareaCategoryIdAndFilterByCountry(long subareaCategoryId,
			long countryCategoryId, int monthRange, String casrRepositoryType) throws NoSuchLegalCaseException {
		List<LegalCase> legalCases = new ArrayList<LegalCase>();
		if (countryCategoryId == 0) {
			legalCases = legalCasePersistence.findBySubareaCategoryId(subareaCategoryId);
		} else {
			legalCases = legalCasePersistence.findByCountryCategoryIdAndSubareaCategoryId(countryCategoryId,
					subareaCategoryId);
		}
		List<LegalCase> monthRangelegalCases = getDateRangeLegalCases(monthRange, casrRepositoryType);
		List<LegalCase> monthRangelegalCaseList = new ArrayList<LegalCase>();
		for (LegalCase legalCase : legalCases) {
			if (monthRangelegalCases.contains(legalCase)) {
				monthRangelegalCaseList.add(legalCase);
			}
		}
		return monthRangelegalCaseList;
	}

	/**
	 * Searches legal cases for the given company filtering them by a user and a
	 * keyword and returns only public groups when the given <code>userId</code> is
	 * not positive.
	 *
	 * @param groupId the id of a site to which the legal cases belong.
	 * @param userId  the id of a user who owns the legal cases.
	 * @param keyword the keyword for search in title and description fields.
	 * @param start   the lower bound of the range of legal cases.
	 * @param end     the upper bound of the range of legal cases (not inclusive).
	 * @return the range of found legal cases.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	@Override
	public List<LegalCase> searchLegalCases(long groupId, long userId, String keyword, int start, int end) {
		DynamicQuery dynamicQuery = queryLegalCases(groupId, userId, keyword);
		dynamicQuery.addOrder(OrderFactoryUtil.desc("modifiedDate"));
		dynamicQuery.setLimit(start, end);
		return legalCaseLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Counts legal cases for the given company filtering them by a user and a
	 * keyword and takes into account only public groups when the given
	 * <code>userId</code> is not positive.
	 *
	 * @param groupId the id of a site to which the community groups belong.
	 * @param userId  the id of a user who owns the community groups.
	 * @param keyword the keyword for search in title and description fields.
	 * @return the total number of found legal cases.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	@Override
	public long countLegalCases(long groupId, long userId, String keyword) {
		DynamicQuery dynamicQuery = queryLegalCases(groupId, userId, keyword);
		return legalCaseLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * @param monthRange the number of months to get data for given month range.
	 * @return
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	@Override
	public List<LegalCase> getDateRangeLegalCases(int monthRange, String casrRepositoryType) {
		Calendar calender = Calendar.getInstance();
		Date endDate = calender.getTime();
		calender.add(Calendar.MONTH, -monthRange);
		Date startDate = calender.getTime();
		DynamicQuery dynamicQuery = LegalCaseLocalServiceUtil.dynamicQuery();
		if (LegalCaseConstant.CASE_TYPE_ARCHIVED.equalsIgnoreCase(casrRepositoryType)) {
			dynamicQuery.add(PropertyFactoryUtil.forName("decisionDate").le(startDate));
			dynamicQuery.addOrder(OrderFactoryUtil.desc("decisionDate"));
		} else {
			dynamicQuery.add(PropertyFactoryUtil.forName("decisionDate").between(startDate, endDate));
			dynamicQuery.addOrder(OrderFactoryUtil.desc("decisionDate"));
		}
		
		List<LegalCase> legalCases = LegalCaseLocalServiceUtil.dynamicQuery(dynamicQuery);
		return legalCases;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	@Override
	public List<LegalCase> getLegalCases(String keyword, int start, int end, String orderByCol, String orderByType) {
		DynamicQuery dynamicQuery = legalCaseLocalService.dynamicQuery();
		if (!orderByCol.isEmpty() && orderByCol != null) {
			if (orderByCol.equalsIgnoreCase("countryCategoryId")) {
				return getSortableAssetCategoryList(keyword, orderByCol, orderByType, start, end);
			} else if (orderByCol.equalsIgnoreCase("subareaCategoryId")) {
				return getSortableAssetCategoryList(keyword, orderByCol, orderByType, start, end);
			}

			if (orderByType.equalsIgnoreCase("asc")) {
				dynamicQuery.addOrder(OrderFactoryUtil.asc(orderByCol));
			} else {
				dynamicQuery.addOrder(OrderFactoryUtil.desc(orderByCol));
			}
		} else {
			if (isNotBlank(keyword)) {
				Criterion keywordCriterion = RestrictionsFactoryUtil.or(
						RestrictionsFactoryUtil.ilike("title", StringPool.PERCENT + keyword + StringPool.PERCENT),
						RestrictionsFactoryUtil.ilike("description",
								StringPool.PERCENT + keyword + StringPool.PERCENT));
				dynamicQuery.add(keywordCriterion);
			}
			dynamicQuery.addOrder(OrderFactoryUtil.desc("decisionDate"));
		}
		dynamicQuery.setLimit(start, end);
		return legalCaseLocalService.dynamicQuery(dynamicQuery);
	}

	private List<LegalCase> getSortableAssetCategoryList(String keyword, String orderByCol, String orderByType,
			int start, int end) {
		return legalCaseFinder.findByAssetCategoryOrder(keyword, orderByCol, orderByType, start, end);
	}

	private DynamicQuery queryLegalCases(long groupId, long userId, String keyword) {
		DynamicQuery dynamicQuery = legalCaseLocalService.dynamicQuery();

		dynamicQuery.add(PropertyFactoryUtil.forName("groupId").eq(groupId));
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(WorkflowConstants.STATUS_APPROVED));

		if (userId > 0) {
			dynamicQuery.add(PropertyFactoryUtil.forName("userId").eq(userId));
		}

		if (isNotBlank(keyword)) {
			Criterion keywordCriterion = RestrictionsFactoryUtil.or(
					RestrictionsFactoryUtil.ilike("title", StringPool.PERCENT + keyword + StringPool.PERCENT),
					RestrictionsFactoryUtil.ilike("description", StringPool.PERCENT + keyword + StringPool.PERCENT));
			dynamicQuery.add(keywordCriterion);
		}

		return dynamicQuery;
	}

	private void validateDecisionDate(Date decisionDate) throws PortalException {
		if (Validator.isNull(decisionDate)) {
			throw new LegalCaseDecisionDateException("DecisionDate is unknown");
		}
	}

	private void validateCountryCategoryId(long countryCategoryId) throws PortalException {
		if (Validator.isNull(countryCategoryId)) {
			throw new LegalCaseCountryCategoryIdException("CountryCategoryId is unknown");
		}
		if (countryCategoryId == 0) {
			throw new LegalCaseCountryCategoryIdException("CountryCategoryId is zero");
		}
	}

	private void validateSubareaCategoryId(long subareaCategoryId) throws PortalException {
		if (Validator.isNull(subareaCategoryId)) {
			throw new LegalCaseSubareaCategoryIdException("CountryCategoryId is unknown");
		}
		if (subareaCategoryId == 0) {
			throw new LegalCaseSubareaCategoryIdException("CountryCategoryId is zero");
		}
	}

	private void validateDescription(String description) throws PortalException {
		 
		if (StringUtils.length(description) > LegalCaseConstant.DESCRIPTION_MAX_LENGTH) {
			throw new LegalCaseDescriptionException("Description is too long");
		}
	}

	private void validateTitle(String title) throws PortalException {
		if (StringUtils.isBlank(title)) {
			throw new LegalCaseTitleException("Title is blank");
		}
		if (StringUtils.length(title) > LegalCaseConstant.TITLE_MAX_LENGTH) {
			throw new LegalCaseTitleException("Title is too long");
		}
	}
}