/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.ajp.portal.bean.CourseBean;
import com.ajp.portal.common.util.CommonUtil;
import com.ajp.portal.constant.AJPConstant;
import com.ajp.portal.service.base.CourseLocalServiceBaseImpl;
import com.ajp.portal.util.SAXUtil;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.search.JournalArticleIndexer;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.BooleanQueryFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.IndexSearcherHelperUtil;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.liferay.portal.kernel.search.TermQuery;
import com.liferay.portal.kernel.search.TermQueryFactoryUtil;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

/**
 * The implementation of the course local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy
 * their definitions into the <code>com.ajp.portal.service.CourseLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourseLocalServiceBaseImpl
 */
public class CourseLocalServiceImpl extends CourseLocalServiceBaseImpl {

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Use <code>com.ajp.portal.service.CourseLocalService</code> via injection or
     * a <code>org.osgi.util.tracker.ServiceTracker</code> or use
     * <code>com.ajp.portal.service.CourseLocalServiceUtil</code>.
     */
    private static final String _DATE_FORMAT_PATTERN = "yyyyMMddHHmmss";
    private static final String _DEFAULT_END_RANGE_DATE = "20990101235959";
    
    private static final Log log = LogFactoryUtil.getLog(CourseLocalServiceImpl.class);
    public List<CourseBean> getCourseJournalArticles(long companyId, long groupId, List<Long> seCategories,List<Long> countryCategories,
            int courseCostType, Date courseStartDate, Date courseEndDate, boolean sortOrder ,int start, int end,
            SearchContext searchContext) throws NumberFormatException, PortalException, ParseException {

    	long defaultGroupId = getDefaultGroupId();
    	
    	log.info("defaultGroupId->" + defaultGroupId);
    	
        List<AssetVocabulary> vocabList = AssetVocabularyLocalServiceUtil.getGroupVocabularies(defaultGroupId);
        long countryVocabId = 0L;
        long subjectAreaVocabId = 0L;
        for (AssetVocabulary vocab : vocabList) {
            if (vocab.getTitle(searchContext.getLanguageId()).equals(AJPConstant.COUNTRY_VOCAB)) {
                countryVocabId = vocab.getVocabularyId();
            }
            if (vocab.getTitle(searchContext.getLanguageId()).equals(AJPConstant.SUBJECT_AREA_VOCAB)) {
                subjectAreaVocabId = vocab.getVocabularyId();
            }
        }
        
        log.info("countryVocabId->" + countryVocabId + "subjectAreaVocabId -> " + subjectAreaVocabId);

        String courseStartDateString = null;
        if(Validator.isNotNull(courseStartDate)){
        	courseStartDateString=FastDateFormatFactoryUtil.getSimpleDateFormat(
                _DATE_FORMAT_PATTERN).format(courseStartDate);
        }
        
        String courseEndDateString = null; 
        if(Validator.isNotNull(courseEndDate)){
        	courseEndDateString =	FastDateFormatFactoryUtil.getSimpleDateFormat(
                _DATE_FORMAT_PATTERN).format(courseEndDate);
        }
        
        DDMStructure courseStructue = getCourseStructure();

       
        if (Validator.isNull(courseStructue)) {
            return new ArrayList<>();
        }

        BooleanQuery searchQuery = BooleanQueryFactoryUtil.create(searchContext);

        searchQuery.addRequiredTerm(Field.ENTRY_CLASS_NAME, JournalArticle.class.getName());

        searchQuery.addRequiredTerm(Field.COMPANY_ID, companyId);

        searchQuery.addRequiredTerm(Field.GROUP_ID, groupId);

        searchQuery.addRequiredTerm("head", Boolean.TRUE);

        if (countryCategories.size() > 0) {
            BooleanQuery courseCatQuery = new BooleanQueryImpl();
            for(long courseCat : countryCategories){
                BooleanQuery courseCatQuery2 = new BooleanQueryImpl();
                courseCatQuery2.addTerm(AJPConstant.FIELD_COURSE_CATEGORIES, courseCat);
                courseCatQuery.add(courseCatQuery2, BooleanClauseOccur.SHOULD);
            }
            searchQuery.add(courseCatQuery, BooleanClauseOccur.MUST);
        }
        
        if (seCategories.size() > 0) {
            BooleanQuery courseCatQuery = new BooleanQueryImpl();
            for(long courseCat : seCategories){
                BooleanQuery courseCatQuery2 = new BooleanQueryImpl();
                courseCatQuery2.addTerm(AJPConstant.FIELD_COURSE_CATEGORIES, courseCat);
                courseCatQuery.add(courseCatQuery2, BooleanClauseOccur.SHOULD);
            }
            searchQuery.add(courseCatQuery, BooleanClauseOccur.MUST);
        }
       
       log.info("courseStructure key->" + courseStructue.getStructureKey());
        
        //searchQuery.
        
        if (Validator.isNotNull(courseStartDate) && Validator.isNotNull(courseEndDate)) {
            BooleanQuery dateQuery = new BooleanQueryImpl();
            dateQuery.addRangeTerm(AJPConstant.FIELD_COURSE_START_DATE,
                    Long.valueOf(courseStartDateString).longValue(), Long.valueOf(courseEndDateString).longValue());
            searchQuery.add(dateQuery, BooleanClauseOccur.MUST);
        }else if(Validator.isNotNull(courseStartDate)){
        	 BooleanQuery dateQuery = new BooleanQueryImpl();
             dateQuery.addRangeTerm(AJPConstant.FIELD_COURSE_START_DATE,  Long.valueOf(courseStartDateString).longValue(),
                    Long.valueOf(_DEFAULT_END_RANGE_DATE).longValue()); 
        	// dateQuery.addRequiredTerm(AJPConstant.FIELD_COURSE_START_DATE, Long.valueOf(courseStartDateString).longValue());
             searchQuery.add(dateQuery, BooleanClauseOccur.MUST);
        }else if(Validator.isNotNull(courseEndDate)){
        	 BooleanQuery dateQuery = new BooleanQueryImpl();
        	 dateQuery.addRangeTerm(AJPConstant.FIELD_COURSE_START_DATE, -1,  Long.valueOf(courseEndDateString).longValue());
        	// dateQuery.addRequiredTerm(AJPConstant.FIELD_COURSE_START_DATE, Long.valueOf(courseEndDateString).longValue());
             searchQuery.add(dateQuery, BooleanClauseOccur.MUST);
        }
        
        if(courseCostType != AJPConstant.COURSE_COST_ALL_TYPE){
            if(courseCostType==AJPConstant.COURSE_FREE_TYPE){
                BooleanQuery courseCostQuery = new BooleanQueryImpl();
                courseCostQuery.addRequiredTerm(AJPConstant.FIELD_COURSE_AMOUNT, 0D);
                searchQuery.add(courseCostQuery, BooleanClauseOccur.MUST);                          
            }else{
                BooleanQuery courseCostQuery = new BooleanQueryImpl();
                courseCostQuery.addRequiredTerm(AJPConstant.FIELD_COURSE_AMOUNT, 0D);
                searchQuery.add(courseCostQuery, BooleanClauseOccur.MUST_NOT);              
            }
        }
        
        
        //Filter archive courses 
        DateFormat df = new SimpleDateFormat(AJPConstant.COURSE_DATE_FORMAT);
        
        Date today = df.parse(df.format(new Date()));
        String todayDateStr =FastDateFormatFactoryUtil.getSimpleDateFormat(
                _DATE_FORMAT_PATTERN).format(today);
        
        BooleanQuery dateQuery = new BooleanQueryImpl();
   	 	dateQuery.addRangeTerm(AJPConstant.FIELD_COURSE_END_DATE, Long.valueOf(todayDateStr).longValue(),  Long.valueOf(_DEFAULT_END_RANGE_DATE).longValue());                     
        searchQuery.add(dateQuery, BooleanClauseOccur.MUST);
       
        BooleanQuery ddmStatusQuery = new BooleanQueryImpl();
        ddmStatusQuery.addExactTerm(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
        searchQuery.add(ddmStatusQuery, BooleanClauseOccur.MUST);

        BooleanQuery ddmStructureQuery = new BooleanQueryImpl();
        ddmStructureQuery.addExactTerm("ddmStructureKey", courseStructue.getStructureKey());
        searchQuery.add(ddmStructureQuery, BooleanClauseOccur.MUST);

        Sort sortByCoustStartDate = SortFactoryUtil.create(AJPConstant.FIELD_COURSE_START_DATE, Sort.LONG_TYPE,
                sortOrder);
         
        searchContext.setSorts(sortByCoustStartDate);
        searchContext.setStart(start);
        searchContext.setEnd(end);

        JournalArticleIndexer indexer = (JournalArticleIndexer) IndexerRegistryUtil.getIndexer(JournalArticle.class);
        searchContext.setSearchEngineId(indexer.getSearchEngineId());
        
        //searchContext.setSearchEngineId();
        
        Hits hits = IndexSearcherHelperUtil.search(searchContext, searchQuery);

        Document[] documents = hits.getDocs();
        int totalHits = hits.getLength();

        List<CourseBean> courseBeanList = new ArrayList<>();
       

        if(totalHits>0 && start <= totalHits){
            courseBeanList = getCourses(documents, groupId, searchContext, countryVocabId, subjectAreaVocabId);
        }
               
        return courseBeanList;
    }
    
    public List<CourseBean> getArchivedCourses(long companyId, long groupId, List<Long> seCategories,List<Long> countryCategories,
            int courseCostType, boolean sortOrder ,int start, int end,
            SearchContext searchContext) throws NumberFormatException, PortalException, ParseException {

    	long defaultGroupId = getDefaultGroupId();
        List<AssetVocabulary> vocabList = AssetVocabularyLocalServiceUtil.getGroupVocabularies(defaultGroupId);
        long countryVocabId = 0L;
        long subjectAreaVocabId = 0L;
        String DEFAULT_END_RANGE_DATE = "20180101235959";
        
        for (AssetVocabulary vocab : vocabList) {
            if (vocab.getTitle(searchContext.getLanguageId()).equalsIgnoreCase(AJPConstant.COUNTRY_VOCAB)) {
                countryVocabId = vocab.getVocabularyId();
            }
            if (vocab.getTitle(searchContext.getLanguageId()).equalsIgnoreCase(AJPConstant.SUBJECT_AREA_VOCAB)) {
                subjectAreaVocabId = vocab.getVocabularyId();
            }
        }

        
        DDMStructure courseStructue = getCourseStructure();

       
        if (Validator.isNull(courseStructue)) {
            return new ArrayList<>();
        }

        BooleanQuery searchQuery = BooleanQueryFactoryUtil.create(searchContext);

        searchQuery.addRequiredTerm(Field.ENTRY_CLASS_NAME, JournalArticle.class.getName());

        searchQuery.addRequiredTerm(Field.COMPANY_ID, companyId);

        searchQuery.addRequiredTerm(Field.GROUP_ID, groupId);

        searchQuery.addRequiredTerm("head", Boolean.TRUE);

        if (countryCategories.size() > 0) {
            BooleanQuery courseCatQuery = new BooleanQueryImpl();
            for(long courseCat : countryCategories){
                BooleanQuery courseCatQuery2 = new BooleanQueryImpl();
                courseCatQuery2.addTerm(AJPConstant.FIELD_COURSE_CATEGORIES, courseCat);
                courseCatQuery.add(courseCatQuery2, BooleanClauseOccur.SHOULD);
            }
            searchQuery.add(courseCatQuery, BooleanClauseOccur.MUST);
        }
        
        if (seCategories.size() > 0) {
            BooleanQuery courseCatQuery = new BooleanQueryImpl();
            for(long courseCat : seCategories){
                BooleanQuery courseCatQuery2 = new BooleanQueryImpl();
                courseCatQuery2.addTerm(AJPConstant.FIELD_COURSE_CATEGORIES, courseCat);
                courseCatQuery.add(courseCatQuery2, BooleanClauseOccur.SHOULD);
            }
            searchQuery.add(courseCatQuery, BooleanClauseOccur.MUST);
        }
       
        
        if(courseCostType != AJPConstant.COURSE_COST_ALL_TYPE){
            if(courseCostType==AJPConstant.COURSE_FREE_TYPE){
                BooleanQuery courseCostQuery = new BooleanQueryImpl();
                courseCostQuery.addRequiredTerm(AJPConstant.FIELD_COURSE_AMOUNT, 0D);
                searchQuery.add(courseCostQuery, BooleanClauseOccur.MUST);                          
            }else{
                BooleanQuery courseCostQuery = new BooleanQueryImpl();
                courseCostQuery.addRequiredTerm(AJPConstant.FIELD_COURSE_AMOUNT, 0D);
                searchQuery.add(courseCostQuery, BooleanClauseOccur.MUST_NOT);              
            }
        }
        
        
        //Filter archive courses 
        DateFormat df = new SimpleDateFormat(AJPConstant.COURSE_DATE_FORMAT);
        
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        
        Date yesterday = df.parse(df.format(calendar.getTime()));
        String yesterdayDateStr =FastDateFormatFactoryUtil.getSimpleDateFormat(
                _DATE_FORMAT_PATTERN).format(yesterday);
        
        BooleanQuery dateQuery = new BooleanQueryImpl();
   	 	dateQuery.addRangeTerm(AJPConstant.FIELD_COURSE_END_DATE, Long.valueOf(DEFAULT_END_RANGE_DATE).longValue(),  Long.valueOf(yesterdayDateStr).longValue());                     
        searchQuery.add(dateQuery, BooleanClauseOccur.MUST);
       
        BooleanQuery ddmStatusQuery = new BooleanQueryImpl();
        ddmStatusQuery.addExactTerm(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
        searchQuery.add(ddmStatusQuery, BooleanClauseOccur.MUST);

        BooleanQuery ddmStructureQuery = new BooleanQueryImpl();
        ddmStructureQuery.addExactTerm("ddmStructureKey", courseStructue.getStructureKey());
        searchQuery.add(ddmStructureQuery, BooleanClauseOccur.MUST);

        Sort sortByCoustStartDate = SortFactoryUtil.create(AJPConstant.FIELD_COURSE_START_DATE, Sort.LONG_TYPE,
                sortOrder);
         
        searchContext.setSorts(sortByCoustStartDate);
        searchContext.setStart(start);
        searchContext.setEnd(end);

        JournalArticleIndexer indexer = (JournalArticleIndexer) IndexerRegistryUtil.getIndexer(JournalArticle.class);
        searchContext.setSearchEngineId(indexer.getSearchEngineId());
        
        //searchContext.setSearchEngineId();
        
        Hits hits = IndexSearcherHelperUtil.search(searchContext, searchQuery);

        Document[] documents = hits.getDocs();
        int totalHits = hits.getLength();

        List<CourseBean> courseBeanList = new ArrayList<>();
       

        if(totalHits>0 && start <= totalHits){
        	  courseBeanList = getCourses(documents, groupId, searchContext, countryVocabId, subjectAreaVocabId);
        }
               
        return courseBeanList;
    }
    
    
    public int getTotalArchivedCourses(long companyId, long groupId, List<Long> seCategories,List<Long> countryCategories,
            int courseCostType, boolean sortOrder,
            SearchContext searchContext) throws NumberFormatException, PortalException, ParseException {

    	long defaultGroupId = getDefaultGroupId();
        List<AssetVocabulary> vocabList = AssetVocabularyLocalServiceUtil.getGroupVocabularies(defaultGroupId);
       
        String DEFAULT_END_RANGE_DATE = "20180101235959";
        
        DDMStructure courseStructue = getCourseStructure();

       
        if (Validator.isNull(courseStructue)) {
            return 0;
        }

        BooleanQuery searchQuery = BooleanQueryFactoryUtil.create(searchContext);

        searchQuery.addRequiredTerm(Field.ENTRY_CLASS_NAME, JournalArticle.class.getName());

        searchQuery.addRequiredTerm(Field.COMPANY_ID, companyId);

        searchQuery.addRequiredTerm(Field.GROUP_ID, groupId);

        searchQuery.addRequiredTerm("head", Boolean.TRUE);

        if (countryCategories.size() > 0) {
            BooleanQuery courseCatQuery = new BooleanQueryImpl();
            for(long courseCat : countryCategories){
                BooleanQuery courseCatQuery2 = new BooleanQueryImpl();
                courseCatQuery2.addTerm(AJPConstant.FIELD_COURSE_CATEGORIES, courseCat);
                courseCatQuery.add(courseCatQuery2, BooleanClauseOccur.SHOULD);
            }
            searchQuery.add(courseCatQuery, BooleanClauseOccur.MUST);
        }
        
        if (seCategories.size() > 0) {
            BooleanQuery courseCatQuery = new BooleanQueryImpl();
            for(long courseCat : seCategories){
                BooleanQuery courseCatQuery2 = new BooleanQueryImpl();
                courseCatQuery2.addTerm(AJPConstant.FIELD_COURSE_CATEGORIES, courseCat);
                courseCatQuery.add(courseCatQuery2, BooleanClauseOccur.SHOULD);
            }
            searchQuery.add(courseCatQuery, BooleanClauseOccur.MUST);
        }
       
        
        if(courseCostType != AJPConstant.COURSE_COST_ALL_TYPE){
            if(courseCostType==AJPConstant.COURSE_FREE_TYPE){
                BooleanQuery courseCostQuery = new BooleanQueryImpl();
                courseCostQuery.addRequiredTerm(AJPConstant.FIELD_COURSE_AMOUNT, 0D);
                searchQuery.add(courseCostQuery, BooleanClauseOccur.MUST);                          
            }else{
                BooleanQuery courseCostQuery = new BooleanQueryImpl();
                courseCostQuery.addRequiredTerm(AJPConstant.FIELD_COURSE_AMOUNT, 0D);
                searchQuery.add(courseCostQuery, BooleanClauseOccur.MUST_NOT);              
            }
        }
        
        
        //Filter archive courses 
        DateFormat df = new SimpleDateFormat(AJPConstant.COURSE_DATE_FORMAT);
        
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        
        Date yesterday = df.parse(df.format(calendar.getTime()));
        String yesterdayDateStr =FastDateFormatFactoryUtil.getSimpleDateFormat(
                _DATE_FORMAT_PATTERN).format(yesterday);
        
        BooleanQuery dateQuery = new BooleanQueryImpl();
   	 	dateQuery.addRangeTerm(AJPConstant.FIELD_COURSE_END_DATE, Long.valueOf(DEFAULT_END_RANGE_DATE).longValue(),  Long.valueOf(yesterdayDateStr).longValue());                     
        searchQuery.add(dateQuery, BooleanClauseOccur.MUST);
       
        BooleanQuery ddmStatusQuery = new BooleanQueryImpl();
        ddmStatusQuery.addExactTerm(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
        searchQuery.add(ddmStatusQuery, BooleanClauseOccur.MUST);

        BooleanQuery ddmStructureQuery = new BooleanQueryImpl();
        ddmStructureQuery.addExactTerm("ddmStructureKey", courseStructue.getStructureKey());
        searchQuery.add(ddmStructureQuery, BooleanClauseOccur.MUST);

        Sort sortByCoustStartDate = SortFactoryUtil.create(AJPConstant.FIELD_COURSE_START_DATE, Sort.LONG_TYPE,
                sortOrder);
         
        searchContext.setSorts(sortByCoustStartDate);
       
        JournalArticleIndexer indexer = (JournalArticleIndexer) IndexerRegistryUtil.getIndexer(JournalArticle.class);
        searchContext.setSearchEngineId(indexer.getSearchEngineId());
        
        //searchContext.setSearchEngineId();
        
        Hits hits = IndexSearcherHelperUtil.search(searchContext, searchQuery);

        int totalHits = hits.getLength();      
               
        return totalHits;
    }

    
    private List<CourseBean> getCourses(Document[] documents,long groupId,SearchContext searchContext,long countryVocabId, long subjectAreaVocabId) throws ParseException, PortalException{
    	
    	 DateFormat df = new SimpleDateFormat(AJPConstant.COURSE_DATE_FORMAT);
    	 List<CourseBean> courseBeanList = new ArrayList<>();
    	 for (Document document : documents) {
            JournalArticle journalArticle = JournalArticleLocalServiceUtil
                    .getLatestArticle(Long.parseLong(document.get(Field.ENTRY_CLASS_PK)));
            
            String countryCatName = getCategoryNameFromArticleAndVocab(groupId, countryVocabId, journalArticle,
                    searchContext);
            String subjectAreaCatName = getCategoryNameFromArticleAndVocab(groupId, subjectAreaVocabId, journalArticle,
                    searchContext);
            CourseBean courseBean = new CourseBean(journalArticle);
            courseBean.setTitle(SAXUtil.getValueFromXpath(journalArticle, AJPConstant.COURSE_TITLE_X_PATH));
            courseBean.setCountry(countryCatName);
            courseBean.setSubjectArea(subjectAreaCatName);
            
            String amountStr = SAXUtil.getValueFromXpath(journalArticle,AJPConstant.COURSE_AMOUNT_X_PATH);             
//            double courseCostAmount = 0;
//            if( Validator.isNotNull(amountStr) && !Validator.isBlank(amountStr)){
//           	 try{
//           		 courseCostAmount =  Double.parseDouble(amountStr);
//           	 }catch(NumberFormatException nfev){
//           		 log.error(nfev.getMessage(),nfev);
//           	 }
//           	 
//            }
            courseBean.setCourseAmount(amountStr);
            courseBean.setCourseBannerImageUrl(
                    SAXUtil.getValueFromXpath(journalArticle, AJPConstant.COURSE_BANNER_IMAGE_X_PATH));
            courseBean.setCourseThumbnailImageUrl(
                    SAXUtil.getValueFromXpath(journalArticle, AJPConstant.COURSE_THUMBNAIL_IMAGE_X_PATH));

            String courseArticleStartDate = SAXUtil.getValueFromXpath(journalArticle,AJPConstant.COURSE_START_DATE_X_PATH);
            String courseArticleEndDate = SAXUtil.getValueFromXpath(journalArticle, AJPConstant.COURSE_END_DATE_X_PATH);
            if (Validator.isNotNull(courseArticleStartDate) && Validator.isNotNull(courseArticleEndDate)) {
                Date startDate = df.parse(courseArticleStartDate);
                Date endDate = df.parse(courseArticleEndDate);
                courseBean.setCourseStartDate(startDate);
                courseBean.setCourseEndDate(endDate);
            }
            courseBean.setDescription(SAXUtil.getValueFromXpath(journalArticle, AJPConstant.COURSE_OBJECTIVE_X_PATH));
            courseBean.setJournalArticle(journalArticle);
            courseBeanList.add(courseBean);
        }
    	
    	return courseBeanList;
    }
    /**
     * Get course Structure
     * 
     * @return
     */
    private DDMStructure getCourseStructure() {
        Optional<DDMStructure> courseStructure = CommonUtil.getFirstStructureByName(AJPConstant.COURSE_STRUCTURE);
        DDMStructure ddmStructure = null;
        if (courseStructure.isPresent()) {
            ddmStructure = courseStructure.get();
        }
        return ddmStructure;
    }

    private String getCategoryNameFromArticleAndVocab(long groupId, long vocabId, JournalArticle article,
            SearchContext searchContext) throws PortalException {
        String categoryName = StringPool.BLANK;

        List<AssetCategory> journalCategoryList = AssetCategoryLocalServiceUtil
                .getCategories(JournalArticle.class.getName(), article.getResourcePrimKey());      
        for (AssetCategory category : journalCategoryList) {
            if (category.getVocabularyId() == vocabId) {            	
                categoryName = category.getTitle(searchContext.getLanguageId());
                break;
            }
        }
          
        return categoryName;
    }
    
    /**
	 * Get Global site group id
	 * @return
	 */
	private long getDefaultGroupId(){
		long groupId = 0l;
		try {
			groupId = GroupLocalServiceUtil.getCompanyGroup(PortalUtil.getDefaultCompanyId()).getGroupId();		
		} catch (PortalException e) {
			log.error(e.getMessage());
		}	
		return groupId;
	}
}