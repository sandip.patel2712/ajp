/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ajp.portal.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.ajp.portal.exception.NoSuchLegalCaseException;
import com.ajp.portal.model.LegalCase;
import com.ajp.portal.model.impl.LegalCaseImpl;
import com.ajp.portal.model.impl.LegalCaseModelImpl;
import com.ajp.portal.service.persistence.LegalCasePersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the legal case service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LegalCasePersistenceImpl
	extends BasePersistenceImpl<LegalCase> implements LegalCasePersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>LegalCaseUtil</code> to access the legal case persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		LegalCaseImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the legal cases where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching legal cases
	 */
	@Override
	public List<LegalCase> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the legal cases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid;
			finderArgs = new Object[] {uuid};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<LegalCase> list = null;

		if (retrieveFromCache) {
			list = (List<LegalCase>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (LegalCase legalCase : list) {
					if (!uuid.equals(legalCase.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LEGALCASE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(LegalCaseModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findByUuid_First(
			String uuid, OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchByUuid_First(uuid, orderByComparator);

		if (legalCase != null) {
			return legalCase;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchLegalCaseException(msg.toString());
	}

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchByUuid_First(
		String uuid, OrderByComparator<LegalCase> orderByComparator) {

		List<LegalCase> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findByUuid_Last(
			String uuid, OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchByUuid_Last(uuid, orderByComparator);

		if (legalCase != null) {
			return legalCase;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchLegalCaseException(msg.toString());
	}

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchByUuid_Last(
		String uuid, OrderByComparator<LegalCase> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<LegalCase> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where uuid = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase[] findByUuid_PrevAndNext(
			long legalCaseId, String uuid,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		uuid = Objects.toString(uuid, "");

		LegalCase legalCase = findByPrimaryKey(legalCaseId);

		Session session = null;

		try {
			session = openSession();

			LegalCase[] array = new LegalCaseImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, legalCase, uuid, orderByComparator, true);

			array[1] = legalCase;

			array[2] = getByUuid_PrevAndNext(
				session, legalCase, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LegalCase getByUuid_PrevAndNext(
		Session session, LegalCase legalCase, String uuid,
		OrderByComparator<LegalCase> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LEGALCASE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LegalCaseModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(legalCase)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<LegalCase> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the legal cases where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (LegalCase legalCase :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(legalCase);
		}
	}

	/**
	 * Returns the number of legal cases where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching legal cases
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LEGALCASE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"legalCase.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(legalCase.uuid IS NULL OR legalCase.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the legal case where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchLegalCaseException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findByUUID_G(String uuid, long groupId)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchByUUID_G(uuid, groupId);

		if (legalCase == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchLegalCaseException(msg.toString());
		}

		return legalCase;
	}

	/**
	 * Returns the legal case where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the legal case where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = new Object[] {uuid, groupId};

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof LegalCase) {
			LegalCase legalCase = (LegalCase)result;

			if (!Objects.equals(uuid, legalCase.getUuid()) ||
				(groupId != legalCase.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_LEGALCASE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<LegalCase> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByUUID_G, finderArgs, list);
				}
				else {
					LegalCase legalCase = list.get(0);

					result = legalCase;

					cacheResult(legalCase);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(_finderPathFetchByUUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (LegalCase)result;
		}
	}

	/**
	 * Removes the legal case where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the legal case that was removed
	 */
	@Override
	public LegalCase removeByUUID_G(String uuid, long groupId)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = findByUUID_G(uuid, groupId);

		return remove(legalCase);
	}

	/**
	 * Returns the number of legal cases where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching legal cases
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_LEGALCASE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"legalCase.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(legalCase.uuid IS NULL OR legalCase.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"legalCase.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching legal cases
	 */
	@Override
	public List<LegalCase> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid_C;
			finderArgs = new Object[] {uuid, companyId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<LegalCase> list = null;

		if (retrieveFromCache) {
			list = (List<LegalCase>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (LegalCase legalCase : list) {
					if (!uuid.equals(legalCase.getUuid()) ||
						(companyId != legalCase.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_LEGALCASE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(LegalCaseModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (legalCase != null) {
			return legalCase;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchLegalCaseException(msg.toString());
	}

	/**
	 * Returns the first legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<LegalCase> orderByComparator) {

		List<LegalCase> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (legalCase != null) {
			return legalCase;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchLegalCaseException(msg.toString());
	}

	/**
	 * Returns the last legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<LegalCase> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<LegalCase> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase[] findByUuid_C_PrevAndNext(
			long legalCaseId, String uuid, long companyId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		uuid = Objects.toString(uuid, "");

		LegalCase legalCase = findByPrimaryKey(legalCaseId);

		Session session = null;

		try {
			session = openSession();

			LegalCase[] array = new LegalCaseImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, legalCase, uuid, companyId, orderByComparator, true);

			array[1] = legalCase;

			array[2] = getByUuid_C_PrevAndNext(
				session, legalCase, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LegalCase getByUuid_C_PrevAndNext(
		Session session, LegalCase legalCase, String uuid, long companyId,
		OrderByComparator<LegalCase> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_LEGALCASE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LegalCaseModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(legalCase)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<LegalCase> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the legal cases where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (LegalCase legalCase :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(legalCase);
		}
	}

	/**
	 * Returns the number of legal cases where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching legal cases
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_LEGALCASE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"legalCase.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(legalCase.uuid IS NULL OR legalCase.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"legalCase.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByCountryCategoryId;
	private FinderPath _finderPathWithoutPaginationFindByCountryCategoryId;
	private FinderPath _finderPathCountByCountryCategoryId;

	/**
	 * Returns all the legal cases where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @return the matching legal cases
	 */
	@Override
	public List<LegalCase> findByCountryCategoryId(long countryCategoryId) {
		return findByCountryCategoryId(
			countryCategoryId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the legal cases where countryCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByCountryCategoryId(
		long countryCategoryId, int start, int end) {

		return findByCountryCategoryId(countryCategoryId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByCountryCategoryId(
		long countryCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return findByCountryCategoryId(
			countryCategoryId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByCountryCategoryId(
		long countryCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByCountryCategoryId;
			finderArgs = new Object[] {countryCategoryId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByCountryCategoryId;
			finderArgs = new Object[] {
				countryCategoryId, start, end, orderByComparator
			};
		}

		List<LegalCase> list = null;

		if (retrieveFromCache) {
			list = (List<LegalCase>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (LegalCase legalCase : list) {
					if ((countryCategoryId !=
							legalCase.getCountryCategoryId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LEGALCASE_WHERE);

			query.append(_FINDER_COLUMN_COUNTRYCATEGORYID_COUNTRYCATEGORYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(LegalCaseModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(countryCategoryId);

				if (!pagination) {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findByCountryCategoryId_First(
			long countryCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchByCountryCategoryId_First(
			countryCategoryId, orderByComparator);

		if (legalCase != null) {
			return legalCase;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("countryCategoryId=");
		msg.append(countryCategoryId);

		msg.append("}");

		throw new NoSuchLegalCaseException(msg.toString());
	}

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchByCountryCategoryId_First(
		long countryCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		List<LegalCase> list = findByCountryCategoryId(
			countryCategoryId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findByCountryCategoryId_Last(
			long countryCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchByCountryCategoryId_Last(
			countryCategoryId, orderByComparator);

		if (legalCase != null) {
			return legalCase;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("countryCategoryId=");
		msg.append(countryCategoryId);

		msg.append("}");

		throw new NoSuchLegalCaseException(msg.toString());
	}

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchByCountryCategoryId_Last(
		long countryCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		int count = countByCountryCategoryId(countryCategoryId);

		if (count == 0) {
			return null;
		}

		List<LegalCase> list = findByCountryCategoryId(
			countryCategoryId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where countryCategoryId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param countryCategoryId the country category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase[] findByCountryCategoryId_PrevAndNext(
			long legalCaseId, long countryCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = findByPrimaryKey(legalCaseId);

		Session session = null;

		try {
			session = openSession();

			LegalCase[] array = new LegalCaseImpl[3];

			array[0] = getByCountryCategoryId_PrevAndNext(
				session, legalCase, countryCategoryId, orderByComparator, true);

			array[1] = legalCase;

			array[2] = getByCountryCategoryId_PrevAndNext(
				session, legalCase, countryCategoryId, orderByComparator,
				false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LegalCase getByCountryCategoryId_PrevAndNext(
		Session session, LegalCase legalCase, long countryCategoryId,
		OrderByComparator<LegalCase> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LEGALCASE_WHERE);

		query.append(_FINDER_COLUMN_COUNTRYCATEGORYID_COUNTRYCATEGORYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LegalCaseModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(countryCategoryId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(legalCase)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<LegalCase> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the legal cases where countryCategoryId = &#63; from the database.
	 *
	 * @param countryCategoryId the country category ID
	 */
	@Override
	public void removeByCountryCategoryId(long countryCategoryId) {
		for (LegalCase legalCase :
				findByCountryCategoryId(
					countryCategoryId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(legalCase);
		}
	}

	/**
	 * Returns the number of legal cases where countryCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @return the number of matching legal cases
	 */
	@Override
	public int countByCountryCategoryId(long countryCategoryId) {
		FinderPath finderPath = _finderPathCountByCountryCategoryId;

		Object[] finderArgs = new Object[] {countryCategoryId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LEGALCASE_WHERE);

			query.append(_FINDER_COLUMN_COUNTRYCATEGORYID_COUNTRYCATEGORYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(countryCategoryId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_COUNTRYCATEGORYID_COUNTRYCATEGORYID_2 =
			"legalCase.countryCategoryId = ?";

	private FinderPath _finderPathWithPaginationFindBySubareaCategoryId;
	private FinderPath _finderPathWithoutPaginationFindBySubareaCategoryId;
	private FinderPath _finderPathCountBySubareaCategoryId;

	/**
	 * Returns all the legal cases where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @return the matching legal cases
	 */
	@Override
	public List<LegalCase> findBySubareaCategoryId(long subareaCategoryId) {
		return findBySubareaCategoryId(
			subareaCategoryId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the legal cases where subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	@Override
	public List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId, int start, int end) {

		return findBySubareaCategoryId(subareaCategoryId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the legal cases where subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	@Override
	public List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return findBySubareaCategoryId(
			subareaCategoryId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the legal cases where subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	@Override
	public List<LegalCase> findBySubareaCategoryId(
		long subareaCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindBySubareaCategoryId;
			finderArgs = new Object[] {subareaCategoryId};
		}
		else {
			finderPath = _finderPathWithPaginationFindBySubareaCategoryId;
			finderArgs = new Object[] {
				subareaCategoryId, start, end, orderByComparator
			};
		}

		List<LegalCase> list = null;

		if (retrieveFromCache) {
			list = (List<LegalCase>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (LegalCase legalCase : list) {
					if ((subareaCategoryId !=
							legalCase.getSubareaCategoryId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LEGALCASE_WHERE);

			query.append(_FINDER_COLUMN_SUBAREACATEGORYID_SUBAREACATEGORYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(LegalCaseModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subareaCategoryId);

				if (!pagination) {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findBySubareaCategoryId_First(
			long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchBySubareaCategoryId_First(
			subareaCategoryId, orderByComparator);

		if (legalCase != null) {
			return legalCase;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subareaCategoryId=");
		msg.append(subareaCategoryId);

		msg.append("}");

		throw new NoSuchLegalCaseException(msg.toString());
	}

	/**
	 * Returns the first legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchBySubareaCategoryId_First(
		long subareaCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		List<LegalCase> list = findBySubareaCategoryId(
			subareaCategoryId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findBySubareaCategoryId_Last(
			long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchBySubareaCategoryId_Last(
			subareaCategoryId, orderByComparator);

		if (legalCase != null) {
			return legalCase;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subareaCategoryId=");
		msg.append(subareaCategoryId);

		msg.append("}");

		throw new NoSuchLegalCaseException(msg.toString());
	}

	/**
	 * Returns the last legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchBySubareaCategoryId_Last(
		long subareaCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		int count = countBySubareaCategoryId(subareaCategoryId);

		if (count == 0) {
			return null;
		}

		List<LegalCase> list = findBySubareaCategoryId(
			subareaCategoryId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where subareaCategoryId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase[] findBySubareaCategoryId_PrevAndNext(
			long legalCaseId, long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = findByPrimaryKey(legalCaseId);

		Session session = null;

		try {
			session = openSession();

			LegalCase[] array = new LegalCaseImpl[3];

			array[0] = getBySubareaCategoryId_PrevAndNext(
				session, legalCase, subareaCategoryId, orderByComparator, true);

			array[1] = legalCase;

			array[2] = getBySubareaCategoryId_PrevAndNext(
				session, legalCase, subareaCategoryId, orderByComparator,
				false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LegalCase getBySubareaCategoryId_PrevAndNext(
		Session session, LegalCase legalCase, long subareaCategoryId,
		OrderByComparator<LegalCase> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LEGALCASE_WHERE);

		query.append(_FINDER_COLUMN_SUBAREACATEGORYID_SUBAREACATEGORYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LegalCaseModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(subareaCategoryId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(legalCase)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<LegalCase> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the legal cases where subareaCategoryId = &#63; from the database.
	 *
	 * @param subareaCategoryId the subarea category ID
	 */
	@Override
	public void removeBySubareaCategoryId(long subareaCategoryId) {
		for (LegalCase legalCase :
				findBySubareaCategoryId(
					subareaCategoryId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(legalCase);
		}
	}

	/**
	 * Returns the number of legal cases where subareaCategoryId = &#63;.
	 *
	 * @param subareaCategoryId the subarea category ID
	 * @return the number of matching legal cases
	 */
	@Override
	public int countBySubareaCategoryId(long subareaCategoryId) {
		FinderPath finderPath = _finderPathCountBySubareaCategoryId;

		Object[] finderArgs = new Object[] {subareaCategoryId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LEGALCASE_WHERE);

			query.append(_FINDER_COLUMN_SUBAREACATEGORYID_SUBAREACATEGORYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subareaCategoryId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_SUBAREACATEGORYID_SUBAREACATEGORYID_2 =
			"legalCase.subareaCategoryId = ?";

	private FinderPath
		_finderPathWithPaginationFindByCountryCategoryIdAndSubareaCategoryId;
	private FinderPath
		_finderPathWithoutPaginationFindByCountryCategoryIdAndSubareaCategoryId;
	private FinderPath _finderPathCountByCountryCategoryIdAndSubareaCategoryId;

	/**
	 * Returns all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @return the matching legal cases
	 */
	@Override
	public List<LegalCase> findByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId) {

		return findByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId, int start, int end) {

		return findByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator) {

		return findByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching legal cases
	 */
	@Override
	public List<LegalCase> findByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId, int start, int end,
		OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath =
				_finderPathWithoutPaginationFindByCountryCategoryIdAndSubareaCategoryId;
			finderArgs = new Object[] {countryCategoryId, subareaCategoryId};
		}
		else {
			finderPath =
				_finderPathWithPaginationFindByCountryCategoryIdAndSubareaCategoryId;
			finderArgs = new Object[] {
				countryCategoryId, subareaCategoryId, start, end,
				orderByComparator
			};
		}

		List<LegalCase> list = null;

		if (retrieveFromCache) {
			list = (List<LegalCase>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (LegalCase legalCase : list) {
					if ((countryCategoryId !=
							legalCase.getCountryCategoryId()) ||
						(subareaCategoryId !=
							legalCase.getSubareaCategoryId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_LEGALCASE_WHERE);

			query.append(
				_FINDER_COLUMN_COUNTRYCATEGORYIDANDSUBAREACATEGORYID_COUNTRYCATEGORYID_2);

			query.append(
				_FINDER_COLUMN_COUNTRYCATEGORYIDANDSUBAREACATEGORYID_SUBAREACATEGORYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(LegalCaseModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(countryCategoryId);

				qPos.add(subareaCategoryId);

				if (!pagination) {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findByCountryCategoryIdAndSubareaCategoryId_First(
			long countryCategoryId, long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase =
			fetchByCountryCategoryIdAndSubareaCategoryId_First(
				countryCategoryId, subareaCategoryId, orderByComparator);

		if (legalCase != null) {
			return legalCase;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("countryCategoryId=");
		msg.append(countryCategoryId);

		msg.append(", subareaCategoryId=");
		msg.append(subareaCategoryId);

		msg.append("}");

		throw new NoSuchLegalCaseException(msg.toString());
	}

	/**
	 * Returns the first legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchByCountryCategoryIdAndSubareaCategoryId_First(
		long countryCategoryId, long subareaCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		List<LegalCase> list = findByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case
	 * @throws NoSuchLegalCaseException if a matching legal case could not be found
	 */
	@Override
	public LegalCase findByCountryCategoryIdAndSubareaCategoryId_Last(
			long countryCategoryId, long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchByCountryCategoryIdAndSubareaCategoryId_Last(
			countryCategoryId, subareaCategoryId, orderByComparator);

		if (legalCase != null) {
			return legalCase;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("countryCategoryId=");
		msg.append(countryCategoryId);

		msg.append(", subareaCategoryId=");
		msg.append(subareaCategoryId);

		msg.append("}");

		throw new NoSuchLegalCaseException(msg.toString());
	}

	/**
	 * Returns the last legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching legal case, or <code>null</code> if a matching legal case could not be found
	 */
	@Override
	public LegalCase fetchByCountryCategoryIdAndSubareaCategoryId_Last(
		long countryCategoryId, long subareaCategoryId,
		OrderByComparator<LegalCase> orderByComparator) {

		int count = countByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId);

		if (count == 0) {
			return null;
		}

		List<LegalCase> list = findByCountryCategoryIdAndSubareaCategoryId(
			countryCategoryId, subareaCategoryId, count - 1, count,
			orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the legal cases before and after the current legal case in the ordered set where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param legalCaseId the primary key of the current legal case
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase[] findByCountryCategoryIdAndSubareaCategoryId_PrevAndNext(
			long legalCaseId, long countryCategoryId, long subareaCategoryId,
			OrderByComparator<LegalCase> orderByComparator)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = findByPrimaryKey(legalCaseId);

		Session session = null;

		try {
			session = openSession();

			LegalCase[] array = new LegalCaseImpl[3];

			array[0] = getByCountryCategoryIdAndSubareaCategoryId_PrevAndNext(
				session, legalCase, countryCategoryId, subareaCategoryId,
				orderByComparator, true);

			array[1] = legalCase;

			array[2] = getByCountryCategoryIdAndSubareaCategoryId_PrevAndNext(
				session, legalCase, countryCategoryId, subareaCategoryId,
				orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LegalCase getByCountryCategoryIdAndSubareaCategoryId_PrevAndNext(
		Session session, LegalCase legalCase, long countryCategoryId,
		long subareaCategoryId, OrderByComparator<LegalCase> orderByComparator,
		boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_LEGALCASE_WHERE);

		query.append(
			_FINDER_COLUMN_COUNTRYCATEGORYIDANDSUBAREACATEGORYID_COUNTRYCATEGORYID_2);

		query.append(
			_FINDER_COLUMN_COUNTRYCATEGORYIDANDSUBAREACATEGORYID_SUBAREACATEGORYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LegalCaseModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(countryCategoryId);

		qPos.add(subareaCategoryId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(legalCase)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<LegalCase> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63; from the database.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 */
	@Override
	public void removeByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId) {

		for (LegalCase legalCase :
				findByCountryCategoryIdAndSubareaCategoryId(
					countryCategoryId, subareaCategoryId, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(legalCase);
		}
	}

	/**
	 * Returns the number of legal cases where countryCategoryId = &#63; and subareaCategoryId = &#63;.
	 *
	 * @param countryCategoryId the country category ID
	 * @param subareaCategoryId the subarea category ID
	 * @return the number of matching legal cases
	 */
	@Override
	public int countByCountryCategoryIdAndSubareaCategoryId(
		long countryCategoryId, long subareaCategoryId) {

		FinderPath finderPath =
			_finderPathCountByCountryCategoryIdAndSubareaCategoryId;

		Object[] finderArgs = new Object[] {
			countryCategoryId, subareaCategoryId
		};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_LEGALCASE_WHERE);

			query.append(
				_FINDER_COLUMN_COUNTRYCATEGORYIDANDSUBAREACATEGORYID_COUNTRYCATEGORYID_2);

			query.append(
				_FINDER_COLUMN_COUNTRYCATEGORYIDANDSUBAREACATEGORYID_SUBAREACATEGORYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(countryCategoryId);

				qPos.add(subareaCategoryId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_COUNTRYCATEGORYIDANDSUBAREACATEGORYID_COUNTRYCATEGORYID_2 =
			"legalCase.countryCategoryId = ? AND ";

	private static final String
		_FINDER_COLUMN_COUNTRYCATEGORYIDANDSUBAREACATEGORYID_SUBAREACATEGORYID_2 =
			"legalCase.subareaCategoryId = ?";

	public LegalCasePersistenceImpl() {
		setModelClass(LegalCase.class);

		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
				"_dbColumnNames");

			field.setAccessible(true);

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the legal case in the entity cache if it is enabled.
	 *
	 * @param legalCase the legal case
	 */
	@Override
	public void cacheResult(LegalCase legalCase) {
		entityCache.putResult(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED, LegalCaseImpl.class,
			legalCase.getPrimaryKey(), legalCase);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {legalCase.getUuid(), legalCase.getGroupId()},
			legalCase);

		legalCase.resetOriginalValues();
	}

	/**
	 * Caches the legal cases in the entity cache if it is enabled.
	 *
	 * @param legalCases the legal cases
	 */
	@Override
	public void cacheResult(List<LegalCase> legalCases) {
		for (LegalCase legalCase : legalCases) {
			if (entityCache.getResult(
					LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
					LegalCaseImpl.class, legalCase.getPrimaryKey()) == null) {

				cacheResult(legalCase);
			}
			else {
				legalCase.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all legal cases.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(LegalCaseImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the legal case.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LegalCase legalCase) {
		entityCache.removeResult(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED, LegalCaseImpl.class,
			legalCase.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((LegalCaseModelImpl)legalCase, true);
	}

	@Override
	public void clearCache(List<LegalCase> legalCases) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LegalCase legalCase : legalCases) {
			entityCache.removeResult(
				LegalCaseModelImpl.ENTITY_CACHE_ENABLED, LegalCaseImpl.class,
				legalCase.getPrimaryKey());

			clearUniqueFindersCache((LegalCaseModelImpl)legalCase, true);
		}
	}

	protected void cacheUniqueFindersCache(
		LegalCaseModelImpl legalCaseModelImpl) {

		Object[] args = new Object[] {
			legalCaseModelImpl.getUuid(), legalCaseModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, legalCaseModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		LegalCaseModelImpl legalCaseModelImpl, boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {
				legalCaseModelImpl.getUuid(), legalCaseModelImpl.getGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if ((legalCaseModelImpl.getColumnBitmask() &
			 _finderPathFetchByUUID_G.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				legalCaseModelImpl.getOriginalUuid(),
				legalCaseModelImpl.getOriginalGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}
	}

	/**
	 * Creates a new legal case with the primary key. Does not add the legal case to the database.
	 *
	 * @param legalCaseId the primary key for the new legal case
	 * @return the new legal case
	 */
	@Override
	public LegalCase create(long legalCaseId) {
		LegalCase legalCase = new LegalCaseImpl();

		legalCase.setNew(true);
		legalCase.setPrimaryKey(legalCaseId);

		String uuid = PortalUUIDUtil.generate();

		legalCase.setUuid(uuid);

		legalCase.setCompanyId(companyProvider.getCompanyId());

		return legalCase;
	}

	/**
	 * Removes the legal case with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case that was removed
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase remove(long legalCaseId) throws NoSuchLegalCaseException {
		return remove((Serializable)legalCaseId);
	}

	/**
	 * Removes the legal case with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the legal case
	 * @return the legal case that was removed
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase remove(Serializable primaryKey)
		throws NoSuchLegalCaseException {

		Session session = null;

		try {
			session = openSession();

			LegalCase legalCase = (LegalCase)session.get(
				LegalCaseImpl.class, primaryKey);

			if (legalCase == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLegalCaseException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(legalCase);
		}
		catch (NoSuchLegalCaseException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LegalCase removeImpl(LegalCase legalCase) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(legalCase)) {
				legalCase = (LegalCase)session.get(
					LegalCaseImpl.class, legalCase.getPrimaryKeyObj());
			}

			if (legalCase != null) {
				session.delete(legalCase);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (legalCase != null) {
			clearCache(legalCase);
		}

		return legalCase;
	}

	@Override
	public LegalCase updateImpl(LegalCase legalCase) {
		boolean isNew = legalCase.isNew();

		if (!(legalCase instanceof LegalCaseModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(legalCase.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(legalCase);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in legalCase proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom LegalCase implementation " +
					legalCase.getClass());
		}

		LegalCaseModelImpl legalCaseModelImpl = (LegalCaseModelImpl)legalCase;

		if (Validator.isNull(legalCase.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			legalCase.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (legalCase.getCreateDate() == null)) {
			if (serviceContext == null) {
				legalCase.setCreateDate(now);
			}
			else {
				legalCase.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!legalCaseModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				legalCase.setModifiedDate(now);
			}
			else {
				legalCase.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (legalCase.isNew()) {
				session.save(legalCase);

				legalCase.setNew(false);
			}
			else {
				legalCase = (LegalCase)session.merge(legalCase);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!LegalCaseModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			Object[] args = new Object[] {legalCaseModelImpl.getUuid()};

			finderCache.removeResult(_finderPathCountByUuid, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid, args);

			args = new Object[] {
				legalCaseModelImpl.getUuid(), legalCaseModelImpl.getCompanyId()
			};

			finderCache.removeResult(_finderPathCountByUuid_C, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid_C, args);

			args = new Object[] {legalCaseModelImpl.getCountryCategoryId()};

			finderCache.removeResult(_finderPathCountByCountryCategoryId, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByCountryCategoryId, args);

			args = new Object[] {legalCaseModelImpl.getSubareaCategoryId()};

			finderCache.removeResult(_finderPathCountBySubareaCategoryId, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindBySubareaCategoryId, args);

			args = new Object[] {
				legalCaseModelImpl.getCountryCategoryId(),
				legalCaseModelImpl.getSubareaCategoryId()
			};

			finderCache.removeResult(
				_finderPathCountByCountryCategoryIdAndSubareaCategoryId, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByCountryCategoryIdAndSubareaCategoryId,
				args);

			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}
		else {
			if ((legalCaseModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					legalCaseModelImpl.getOriginalUuid()
				};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);

				args = new Object[] {legalCaseModelImpl.getUuid()};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);
			}

			if ((legalCaseModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid_C.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					legalCaseModelImpl.getOriginalUuid(),
					legalCaseModelImpl.getOriginalCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);

				args = new Object[] {
					legalCaseModelImpl.getUuid(),
					legalCaseModelImpl.getCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);
			}

			if ((legalCaseModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByCountryCategoryId.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					legalCaseModelImpl.getOriginalCountryCategoryId()
				};

				finderCache.removeResult(
					_finderPathCountByCountryCategoryId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByCountryCategoryId, args);

				args = new Object[] {legalCaseModelImpl.getCountryCategoryId()};

				finderCache.removeResult(
					_finderPathCountByCountryCategoryId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByCountryCategoryId, args);
			}

			if ((legalCaseModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindBySubareaCategoryId.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					legalCaseModelImpl.getOriginalSubareaCategoryId()
				};

				finderCache.removeResult(
					_finderPathCountBySubareaCategoryId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindBySubareaCategoryId, args);

				args = new Object[] {legalCaseModelImpl.getSubareaCategoryId()};

				finderCache.removeResult(
					_finderPathCountBySubareaCategoryId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindBySubareaCategoryId, args);
			}

			if ((legalCaseModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByCountryCategoryIdAndSubareaCategoryId.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					legalCaseModelImpl.getOriginalCountryCategoryId(),
					legalCaseModelImpl.getOriginalSubareaCategoryId()
				};

				finderCache.removeResult(
					_finderPathCountByCountryCategoryIdAndSubareaCategoryId,
					args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByCountryCategoryIdAndSubareaCategoryId,
					args);

				args = new Object[] {
					legalCaseModelImpl.getCountryCategoryId(),
					legalCaseModelImpl.getSubareaCategoryId()
				};

				finderCache.removeResult(
					_finderPathCountByCountryCategoryIdAndSubareaCategoryId,
					args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByCountryCategoryIdAndSubareaCategoryId,
					args);
			}
		}

		entityCache.putResult(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED, LegalCaseImpl.class,
			legalCase.getPrimaryKey(), legalCase, false);

		clearUniqueFindersCache(legalCaseModelImpl, false);
		cacheUniqueFindersCache(legalCaseModelImpl);

		legalCase.resetOriginalValues();

		return legalCase;
	}

	/**
	 * Returns the legal case with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the legal case
	 * @return the legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLegalCaseException {

		LegalCase legalCase = fetchByPrimaryKey(primaryKey);

		if (legalCase == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLegalCaseException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return legalCase;
	}

	/**
	 * Returns the legal case with the primary key or throws a <code>NoSuchLegalCaseException</code> if it could not be found.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case
	 * @throws NoSuchLegalCaseException if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase findByPrimaryKey(long legalCaseId)
		throws NoSuchLegalCaseException {

		return findByPrimaryKey((Serializable)legalCaseId);
	}

	/**
	 * Returns the legal case with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the legal case
	 * @return the legal case, or <code>null</code> if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED, LegalCaseImpl.class,
			primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		LegalCase legalCase = (LegalCase)serializable;

		if (legalCase == null) {
			Session session = null;

			try {
				session = openSession();

				legalCase = (LegalCase)session.get(
					LegalCaseImpl.class, primaryKey);

				if (legalCase != null) {
					cacheResult(legalCase);
				}
				else {
					entityCache.putResult(
						LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
						LegalCaseImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(
					LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
					LegalCaseImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return legalCase;
	}

	/**
	 * Returns the legal case with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param legalCaseId the primary key of the legal case
	 * @return the legal case, or <code>null</code> if a legal case with the primary key could not be found
	 */
	@Override
	public LegalCase fetchByPrimaryKey(long legalCaseId) {
		return fetchByPrimaryKey((Serializable)legalCaseId);
	}

	@Override
	public Map<Serializable, LegalCase> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, LegalCase> map =
			new HashMap<Serializable, LegalCase>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			LegalCase legalCase = fetchByPrimaryKey(primaryKey);

			if (legalCase != null) {
				map.put(primaryKey, legalCase);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(
				LegalCaseModelImpl.ENTITY_CACHE_ENABLED, LegalCaseImpl.class,
				primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (LegalCase)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler(
			uncachedPrimaryKeys.size() * 2 + 1);

		query.append(_SQL_SELECT_LEGALCASE_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (LegalCase legalCase : (List<LegalCase>)q.list()) {
				map.put(legalCase.getPrimaryKeyObj(), legalCase);

				cacheResult(legalCase);

				uncachedPrimaryKeys.remove(legalCase.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(
					LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
					LegalCaseImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the legal cases.
	 *
	 * @return the legal cases
	 */
	@Override
	public List<LegalCase> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @return the range of legal cases
	 */
	@Override
	public List<LegalCase> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of legal cases
	 */
	@Override
	public List<LegalCase> findAll(
		int start, int end, OrderByComparator<LegalCase> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the legal cases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>LegalCaseModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of legal cases
	 * @param end the upper bound of the range of legal cases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of legal cases
	 */
	@Override
	public List<LegalCase> findAll(
		int start, int end, OrderByComparator<LegalCase> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<LegalCase> list = null;

		if (retrieveFromCache) {
			list = (List<LegalCase>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_LEGALCASE);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LEGALCASE;

				if (pagination) {
					sql = sql.concat(LegalCaseModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LegalCase>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the legal cases from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (LegalCase legalCase : findAll()) {
			remove(legalCase);
		}
	}

	/**
	 * Returns the number of legal cases.
	 *
	 * @return the number of legal cases
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LEGALCASE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return LegalCaseModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the legal case persistence.
	 */
	public void afterPropertiesSet() {
		_finderPathWithPaginationFindAll = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()},
			LegalCaseModelImpl.UUID_COLUMN_BITMASK |
			LegalCaseModelImpl.DECISIONDATE_COLUMN_BITMASK);

		_finderPathCountByUuid = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()});

		_finderPathFetchByUUID_G = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			LegalCaseModelImpl.UUID_COLUMN_BITMASK |
			LegalCaseModelImpl.GROUPID_COLUMN_BITMASK);

		_finderPathCountByUUID_G = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			LegalCaseModelImpl.UUID_COLUMN_BITMASK |
			LegalCaseModelImpl.COMPANYID_COLUMN_BITMASK |
			LegalCaseModelImpl.DECISIONDATE_COLUMN_BITMASK);

		_finderPathCountByUuid_C = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByCountryCategoryId = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCountryCategoryId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByCountryCategoryId = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCountryCategoryId", new String[] {Long.class.getName()},
			LegalCaseModelImpl.COUNTRYCATEGORYID_COLUMN_BITMASK |
			LegalCaseModelImpl.DECISIONDATE_COLUMN_BITMASK);

		_finderPathCountByCountryCategoryId = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCountryCategoryId", new String[] {Long.class.getName()});

		_finderPathWithPaginationFindBySubareaCategoryId = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBySubareaCategoryId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindBySubareaCategoryId = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBySubareaCategoryId", new String[] {Long.class.getName()},
			LegalCaseModelImpl.SUBAREACATEGORYID_COLUMN_BITMASK |
			LegalCaseModelImpl.DECISIONDATE_COLUMN_BITMASK);

		_finderPathCountBySubareaCategoryId = new FinderPath(
			LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
			LegalCaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBySubareaCategoryId", new String[] {Long.class.getName()});

		_finderPathWithPaginationFindByCountryCategoryIdAndSubareaCategoryId =
			new FinderPath(
				LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
				LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
				FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
				"findByCountryCategoryIdAndSubareaCategoryId",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Integer.class.getName(), Integer.class.getName(),
					OrderByComparator.class.getName()
				});

		_finderPathWithoutPaginationFindByCountryCategoryIdAndSubareaCategoryId =
			new FinderPath(
				LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
				LegalCaseModelImpl.FINDER_CACHE_ENABLED, LegalCaseImpl.class,
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByCountryCategoryIdAndSubareaCategoryId",
				new String[] {Long.class.getName(), Long.class.getName()},
				LegalCaseModelImpl.COUNTRYCATEGORYID_COLUMN_BITMASK |
				LegalCaseModelImpl.SUBAREACATEGORYID_COLUMN_BITMASK |
				LegalCaseModelImpl.DECISIONDATE_COLUMN_BITMASK);

		_finderPathCountByCountryCategoryIdAndSubareaCategoryId =
			new FinderPath(
				LegalCaseModelImpl.ENTITY_CACHE_ENABLED,
				LegalCaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"countByCountryCategoryIdAndSubareaCategoryId",
				new String[] {Long.class.getName(), Long.class.getName()});
	}

	public void destroy() {
		entityCache.removeCache(LegalCaseImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;

	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_LEGALCASE =
		"SELECT legalCase FROM LegalCase legalCase";

	private static final String _SQL_SELECT_LEGALCASE_WHERE_PKS_IN =
		"SELECT legalCase FROM LegalCase legalCase WHERE legalCaseId IN (";

	private static final String _SQL_SELECT_LEGALCASE_WHERE =
		"SELECT legalCase FROM LegalCase legalCase WHERE ";

	private static final String _SQL_COUNT_LEGALCASE =
		"SELECT COUNT(legalCase) FROM LegalCase legalCase";

	private static final String _SQL_COUNT_LEGALCASE_WHERE =
		"SELECT COUNT(legalCase) FROM LegalCase legalCase WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "legalCase.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No LegalCase exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No LegalCase exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		LegalCasePersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

}