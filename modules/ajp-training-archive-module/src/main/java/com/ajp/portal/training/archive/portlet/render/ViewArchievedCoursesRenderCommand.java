package com.ajp.portal.training.archive.portlet.render;

import java.text.ParseException;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.ajp.portal.constant.AJPConstant;
import com.ajp.portal.training.archive.constants.AjpTrainingArchiveModulePortletKeys;
import com.ajp.portal.training.archive.display.context.ArchivedCourseDisplayContext;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

@Component(
        immediate = true,
        property = {
            "javax.portlet.name=" + AjpTrainingArchiveModulePortletKeys.AjpTrainingArchiveModule,
             "mvc.command.name=/"
        },
        service = MVCRenderCommand.class
)
public class ViewArchievedCoursesRenderCommand implements MVCRenderCommand{
    
    private static final Log _log = LogFactoryUtil.getLog(ViewArchievedCoursesRenderCommand.class);

    @Override
    public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
        
        final ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
        final String categoryList = ParamUtil.getString(renderRequest, "categoryList");
        final String courseCostTypeStr = ParamUtil.getString(renderRequest, "courseCoustType");       
        final boolean sortType = ParamUtil.getBoolean(renderRequest, "sortType");
        final String subjectAreaCatIds = ParamUtil.getString(renderRequest, "subjectAreaCatIds");
        final String countryCatIds = ParamUtil.getString(renderRequest, "countryCatIds");

        try {
            
            long countryVocabId = 0L;
            long subjectAreaVocabId = 0L;
            try {
                List<AssetVocabulary> vocabList = AssetVocabularyLocalServiceUtil.getGroupVocabularies(themeDisplay.getCompanyGroupId());
                for (AssetVocabulary vocab : vocabList) {
                    if (vocab.getTitle(themeDisplay.getLanguageId()).equals(AJPConstant.COUNTRY_VOCAB)) {
                        countryVocabId = vocab.getVocabularyId();
                    }
                    if (vocab.getTitle(themeDisplay.getLanguageId()).equals(AJPConstant.SUBJECT_AREA_VOCAB)) {
                        subjectAreaVocabId = vocab.getVocabularyId();
                    }
                }
            } catch (PortalException e) {
                _log.error(e.getMessage(), e);
            }
            
            List<AssetCategory> countryCatList = AssetCategoryLocalServiceUtil.getVocabularyCategories(countryVocabId, QueryUtil.ALL_POS,
                    QueryUtil.ALL_POS, null);
            
            List<AssetCategory> subjectAreaCatList = AssetCategoryLocalServiceUtil.getVocabularyCategories(subjectAreaVocabId, QueryUtil.ALL_POS,
                    QueryUtil.ALL_POS, null);
            
            renderRequest.setAttribute("countryCatList", countryCatList);
            renderRequest.setAttribute("subjectAreaCatList", subjectAreaCatList);
            
            renderRequest.setAttribute("archivedCoursesDisplayContext",
                    new ArchivedCourseDisplayContext(PortalUtil.getLiferayPortletRequest(renderRequest),
                            PortalUtil.getLiferayPortletResponse(renderResponse)));
            
            renderRequest.setAttribute("categoryList", categoryList);
            renderRequest.setAttribute("courseCoustType", courseCostTypeStr);
            renderRequest.setAttribute("sortType", sortType);
            renderRequest.setAttribute("subjectAreaCatIds", subjectAreaCatIds);
            renderRequest.setAttribute("countryCatIds", countryCatIds);
            
        } catch (NumberFormatException e) {
            throw e;
        } catch (ParseException | PortalException e) {
            throw new PortletException(e);
        } 
        
        return "/html/view.jsp";
    }

}
