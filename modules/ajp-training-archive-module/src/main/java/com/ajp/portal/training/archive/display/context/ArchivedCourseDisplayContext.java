package com.ajp.portal.training.archive.display.context;

import com.ajp.portal.bean.CourseBean;
import com.ajp.portal.constant.AJPConstant;
import com.ajp.portal.service.CourseLocalServiceUtil;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;

public class ArchivedCourseDisplayContext {

    public ArchivedCourseDisplayContext(final LiferayPortletRequest liferayPortletRequest,
            final LiferayPortletResponse liferayPortletResponse) throws NumberFormatException, PortalException, ParseException {
        _liferayPortletResponse = liferayPortletResponse;
        final ThemeDisplay themeDisplay = (ThemeDisplay) liferayPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
        final String categoryList = ParamUtil.getString(liferayPortletRequest, "categoryList");
        final String courseCostTypeStr = ParamUtil.getString(liferayPortletRequest, "courseCoustType");       
        final boolean sortType = ParamUtil.getBoolean(liferayPortletRequest, "sortType");
        final String subjectAreaCatIds = ParamUtil.getString(liferayPortletRequest, "subjectAreaCatIds");
        final String countryCatIds = ParamUtil.getString(liferayPortletRequest, "countryCatIds");

        
        final String[] seCatArray = Validator.isNotNull(subjectAreaCatIds) ? subjectAreaCatIds.split(StringPool.COMMA)
                : new String[] {};
        final String[] countryCatArray = Validator.isNotNull(countryCatIds) ? countryCatIds.split(StringPool.COMMA)
                : new String[] {};
        final String[] courseCostTypeArray = Validator.isNotNull(courseCostTypeStr)
                ? courseCostTypeStr.split(StringPool.COMMA) : new String[] {};

        HttpServletRequest request = PortalUtil.getHttpServletRequest(liferayPortletRequest);
        SearchContext searchContext = SearchContextFactory.getInstance(request);

        List<Long> seCatList = new ArrayList<>();
        if (!ArrayUtil.isEmpty(seCatArray)) {
            for (String category : seCatArray) {
            	seCatList.add(Long.parseLong(category));
            }
        }
        
        List<Long> countryCatList = new ArrayList<>();
        if (!ArrayUtil.isEmpty(countryCatArray)) {
            for (String category : countryCatArray) {
            	countryCatList.add(Long.parseLong(category));
            }
        }

        int courseCostType = AJPConstant.COURSE_COST_ALL_TYPE;
        if (courseCostTypeArray.length==1) {
            courseCostType = Integer.parseInt(courseCostTypeArray[0]);
        }

        final PortletURL iteratorUrl = liferayPortletResponse.createRenderURL();
        iteratorUrl.setParameter("categoryList", categoryList);
        iteratorUrl.setParameter("courseCoustType", courseCostTypeStr);
        iteratorUrl.setParameter("sortType", String.valueOf(sortType));
        iteratorUrl.setParameter("subjectAreaCatIds", subjectAreaCatIds);
        iteratorUrl.setParameter("countryCatIds", countryCatIds);
        
        searchContainer = new SearchContainer<>(liferayPortletRequest,
                iteratorUrl, null, "no-records-found");
        searchContainer.setClassName(CourseBean.class.getName());
        
        List<CourseBean> courseBeanList = CourseLocalServiceUtil.getArchivedCourses(themeDisplay.getCompanyId(),
                themeDisplay.getScopeGroupId(), seCatList,countryCatList, courseCostType,sortType, searchContainer.getStart(),
                searchContainer.getEnd(),searchContext);        
        int totalArchivedCourses =CourseLocalServiceUtil.getTotalArchivedCourses(themeDisplay.getCompanyId(),
                themeDisplay.getScopeGroupId(), seCatList,countryCatList, courseCostType, sortType, searchContext);
        
        searchContainer.setTotal(totalArchivedCourses);
        searchContainer.setResults(courseBeanList);
        
    }
    /**
     * The actual response.
     */
    private final LiferayPortletResponse _liferayPortletResponse;

    /**
     * The search container for the page which lists user's contacts.
     */
    private final SearchContainer<CourseBean> searchContainer;
    
    /**
     * @return the search container that lists user's contacts.
     */
    public SearchContainer<CourseBean> getSearchContainer() {
        return searchContainer;
    }
}
