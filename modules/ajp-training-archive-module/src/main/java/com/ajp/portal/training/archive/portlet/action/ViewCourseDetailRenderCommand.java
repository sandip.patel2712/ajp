package com.ajp.portal.training.archive.portlet.action;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.ajp.portal.training.archive.constants.AjpTrainingArchiveModulePortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

@Component(
        property = {
            "javax.portlet.name="+AjpTrainingArchiveModulePortletKeys.AjpTrainingArchiveModule,
            "mvc.command.name=/view_course"
        },
        service = MVCRenderCommand.class
)
public class ViewCourseDetailRenderCommand implements MVCRenderCommand{

    @Override
    public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
        
        ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
        
        long articleId = ParamUtil.getLong(renderRequest, "articleId");
        renderRequest.setAttribute("articleId", articleId);
        renderRequest.setAttribute("groupId", themeDisplay.getScopeGroupId());
        return "/html/course_detail.jsp";
    }

}
