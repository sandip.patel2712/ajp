package com.ajp.portal.training.archive.portlet;

import com.ajp.portal.constant.AJPConstant;
import com.ajp.portal.training.archive.constants.AjpTrainingArchiveModulePortletKeys;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author diptivaghasia
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=AJP",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=AJP Training Archive Module Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/html/view.jsp",
		"javax.portlet.name=" + AjpTrainingArchiveModulePortletKeys.AjpTrainingArchiveModule,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"com.liferay.portlet.header-portlet-css=/css/course-archieved.css"
	},
	service = Portlet.class
	
)
public class AjpTrainingArchiveModulePortlet extends MVCPortlet {
    
}