package com.ajp.portal.training.archive.constants;

/**
 * @author diptivaghasia
 */
public class AjpTrainingArchiveModulePortletKeys {

	public static final String AjpTrainingArchiveModule = "AjpTrainingArchiveModule";

}