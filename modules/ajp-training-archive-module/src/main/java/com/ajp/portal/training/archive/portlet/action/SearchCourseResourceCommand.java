package com.ajp.portal.training.archive.portlet.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;

import com.ajp.portal.bean.CourseBean;
import com.ajp.portal.constant.AJPConstant;
import com.ajp.portal.service.CourseLocalServiceUtil;
import com.ajp.portal.training.archive.constants.AjpTrainingArchiveModulePortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

@Component(
    property = {
        "javax.portlet.name=" + AjpTrainingArchiveModulePortletKeys.AjpTrainingArchiveModule,
        "mvc.command.name=/search-courses"
    },
    service = MVCResourceCommand.class
)
public class SearchCourseResourceCommand extends BaseMVCResourceCommand {

    @Override
    protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
            throws Exception {
        final ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

        
        final int start = ParamUtil.getInteger(resourceRequest, "start");
        final int end = ParamUtil.getInteger(resourceRequest, "end");
        final String returnToFullPageURL = ParamUtil.getString(resourceRequest, "returnToFullPageURL");
        final String categoryList = ParamUtil.getString(resourceRequest, "categoryList");
        final String courseCostTypeStr = ParamUtil.getString(resourceRequest, "courseCoustType");       
        final boolean sortType = ParamUtil.getBoolean(resourceRequest, "sortType");
           

        final String[] categoriesArray = Validator.isNotNull(categoryList) ? categoryList.split(StringPool.COMMA)
                : new String[] {};
        final String[] courseCostTypeArray = Validator.isNotNull(courseCostTypeStr)
                ? courseCostTypeStr.split(StringPool.COMMA) : new String[] {};

        
        HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
        SearchContext searchContext = SearchContextFactory.getInstance(request);

        List<Long> courseCatList = new ArrayList<>();
        if (!ArrayUtil.isEmpty(categoriesArray)) {
            for (String category : categoriesArray) {
                courseCatList.add(Long.parseLong(category));
            }
        }

        int courseCostType = AJPConstant.COURSE_COST_ALL_TYPE;
        if (courseCostTypeArray.length==1) {
            courseCostType = Integer.parseInt(courseCostTypeArray[0]);
        }

        List<CourseBean> courseBeanList =null;// CourseLocalServiceUtil.getArchivedCourses(themeDisplay.getCompanyId(),
               // themeDisplay.getScopeGroupId(), courseCatList, courseCostType,sortType, start, end,
                // searchContext);

        if (courseBeanList.size() > 0) {
            resourceRequest.setAttribute("courseBeanList", courseBeanList);
            resourceRequest.setAttribute("returnToFullPageURL", returnToFullPageURL);
            include(resourceRequest, resourceResponse, "/html/course-list.jsp");
        } else {
            resourceResponse.getWriter().write(StringPool.BLANK);
        }
    }

}
