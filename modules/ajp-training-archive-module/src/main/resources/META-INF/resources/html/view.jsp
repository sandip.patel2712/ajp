<%@page import="java.util.Calendar"%>
<%@page import="com.ajp.portal.constant.AJPConstant"%>
<%@page import="com.liferay.portal.kernel.theme.ThemeDisplay"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ajp.portal.training.archive.display.context.ArchivedCourseDisplayContext"%>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>

<%@ include file="/init.jsp" %>

<% 
SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
Calendar cal = Calendar.getInstance(themeDisplay.getLocale());
cal.setTime(new Date());
cal.add(Calendar.MONTH, 1);
Date courseToDate = cal.getTime();

final ArchivedCourseDisplayContext displayContext = (ArchivedCourseDisplayContext) request.getAttribute(
        "archivedCoursesDisplayContext");

final String categoryList = (String)renderRequest.getAttribute("categoryList");
final String courseCostType = (String)renderRequest.getAttribute("courseCoustType");
final boolean sortType = (Boolean)renderRequest.getAttribute("sortType");

final String subjectAreaCatIds = (String)renderRequest.getAttribute("subjectAreaCatIds");
final String countryCatIds = (String)renderRequest.getAttribute("countryCatIds");

%>

<portlet:renderURL var="searchCoursesUrl">
    <portlet:param name="returnToFullPageURL" value="<%=PortalUtil.getCurrentURL(request)%>"/>
    <liferay-portlet:param name="mvcRenderCommandName" value="/"/>
</portlet:renderURL>

 <div class="container">
 		<aui:form action="" method="post" name="searchArchivedCourseFm">
 		<div class="row">
            <div class="col-sm-3 text-center">
                <h3>Past Courses</h3>
            </div>
            <div class="col-sm-9"></div>

        </div>
        <div class="row content">
            <div class="col-sm-3">
                <div class="row text-left keyinfoheader">
                    <div class="col-sm-12 text-left">
                        <h4>Sort By</h4>
                    </div>

                </div>
                <div class="row keyinfodata text-center">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select class="form-control form-control-lg sortdropdown">
                                <option <%=(sortType==false) ? "selected" : "" %> value="0">Course Start Date (Ascending)</option>
                                <option <%=(sortType==true) ? "selected" : "" %> value="1">Course Start Date (Descending)</option>
                            </select>
                        </div>
                    </div>

                </div>
                <br>

                <div>
                    <div class="row text-left keyinfoheader">
                        <div class="col-sm-6">
                            <h4 class="filterheader text-left">Filter By</h4>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="filterheader text-right reset-filter">Clear All</h5>
                        </div>
                    </div>
                    <div class="row  text-center">
                        <div class="col-sm-12 keyinfodata">
                            <p class="keyinfolabel">Subject Area</p>
                        </div>
                        <div class="keyinfodata col-sm-12">
							<c:forEach items="${subjectAreaCatList}" var="subjectAreaCat">
								<% AssetCategory subjectAreaCategory  = (AssetCategory)pageContext.getAttribute("subjectAreaCat"); %>
								<div class="checkbox">
                                	<label><input type="checkbox" name="subjectcat" class="subject-area" 
                                	<%=(subjectAreaCatIds.indexOf(String.valueOf(subjectAreaCategory.getCategoryId()))>=0) ? "checked" : "" %> value="${subjectAreaCat.categoryId }">
                                	<%= subjectAreaCategory.getTitle(themeDisplay.getLanguageId())  %></label>
                            	</div>
							</c:forEach>
                        </div>
                    </div>

                    <div class="row text-center">
                        <div class="col-sm-12 keyinfodata">
                            <p class="keyinfolabel">Country of Training</p>
                        </div>
                        <div class="keyinfodata col-sm-12">
                            <c:forEach items="${countryCatList}" var="countryCat">
								<% AssetCategory countryCategory  = (AssetCategory)pageContext.getAttribute("countryCat"); %>
								<div class="checkbox">
                                	<label><input type="checkbox"  name="countrycat" class="country" 
                                	<%=(countryCatIds.indexOf(String.valueOf(countryCategory.getCategoryId()))>=0) ? "checked" : "" %> value="${countryCat.categoryId }">
                                	<%= countryCategory.getTitle(themeDisplay.getLanguageId())  %></label>
                            	</div>
							</c:forEach>
                        </div>
                    </div>
                    <div class="row  text-center">
                        <div class="col-sm-12 keyinfodata">
                            <p class="keyinfolabel">Training Cost</p>
                        </div>
                        <div class="keyinfodata col-sm-12">
                            <div class="checkbox">
                                <label><input type="checkbox"  name="countrycosttpye" class="course-cost-type" 
                                <%=(courseCostType.indexOf("0")>=0) ? "checked" : "" %> value="<%= AJPConstant.COURSE_FREE_TYPE %>" >Free</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="countrycosttpye" class="course-cost-type" 
                                <%=(courseCostType.indexOf("1")>=0) ? "checked" : "" %> value="<%= AJPConstant.COURSE_PAID_TYPE %>">Paid</label>
                            </div>
                        </div>
                    </div>                    
                </div>

            </div>

            <div class="col-sm-9">
                <div class="lfr-search-container-wrapper main-content-body">
				    <div class="main-content-body">
				        <liferay-ui:search-container id="contacts" searchContainer="<%= displayContext.getSearchContainer() %>">
				            <liferay-ui:search-container-row
				                    className="com.ajp.portal.bean.CourseBean"
				                    modelVar="courseBean">
				                <liferay-ui:search-container-column-text>
				                    <div class="no-records-found">
				                        
				                    </div>
				                </liferay-ui:search-container-column-text>
				                <liferay-ui:search-container-column-text colspan="<%= 2 %>">
				                    <div class="row text-left">
										<div>
										<portlet:renderURL var="viewCourseDetailURL">
											<portlet:param name="mvcRenderCommandName" value="/view_course" />
											<portlet:param name="articleId" value="<%=courseBean.getJournalArticle().getArticleId() %>" />
										</portlet:renderURL>
																			
										  <div>
				             			   <a href="${viewCourseDetailURL }" class="courselink"><%= HtmlUtil.escape(courseBean.getTitle()) %> </a>
				          				 </div> 
										</div>
										<div>
											<h4><%= HtmlUtil.escape(courseBean.getCountry()) %></h4>
										</div>
										<div>
											<h4><%= HtmlUtil.escape(courseBean.getSubjectArea()) %></h4>
										</div>
										<div>
											<h4><%= HtmlUtil.escape(courseBean.getDescription()) %></h4>
										</div>
										<div>
											<fmt:formatDate value="<%= courseBean.getCourseStartDate()%>" pattern="dd/MM/yyyy" var="courseStartDate" />
											<fmt:formatDate value="<%= courseBean.getCourseEndDate()%>" pattern="dd/MM/yyyy" var="courseEndDate" />
											<h4>Course Date ( ${courseStartDate} - ${courseEndDate} )</h4>
										</div>
									</div>
				                </liferay-ui:search-container-column-text>
				            </liferay-ui:search-container-row>
				            <liferay-ui:search-iterator markupView="lexicon" displayStyle="descriptive" paginate="<%= true %>"
				                                        searchContainer="<%= displayContext.getSearchContainer() %>"/>
				        </liferay-ui:search-container>
				    </div>
				</div>             
            </div>
        </div>
        <aui:input type="hidden" name="categoryList" />
        <aui:input type="hidden" name="courseCoustType" />
        <aui:input type="hidden" name="sortType" />
        <aui:input type="hidden" name="subjectAreaCatIds" />
        <aui:input type="hidden" name="countryCatIds" />
        </aui:form>
    </div>
  
<aui:script>
    
AUI().use('aui-io-request', 'aui-datepicker','aui-base', function(A) {
	
	var currentOffset = 0;
    var scrollAmount = 9;
        
    
    function loadCourses() {
    	
        A.one('#<portlet:namespace />categoryList').set('value',getCourseCategoryList());
        A.one('#<portlet:namespace />courseCoustType').set('value',getCourseCostType());
        A.one('#<portlet:namespace />subjectAreaCatIds').set('value',getSubjectAreaCatList());
        A.one('#<portlet:namespace />countryCatIds').set('value',getCountryCatList());
        A.one('#<portlet:namespace />sortType').set('value',A.one('.sortdropdown').val());
        

        submitForm(A.one('#<portlet:namespace />searchArchivedCourseFm'), '<%= searchCoursesUrl %>', true, false);
     
    }
    
    function getCourseCategoryList(){
    	var courseCategoryIds="";
    	$('input[name="subjectcat"]:checked').each(function() {
    		courseCategoryIds += this.value + ",";
    	});
    	$('input[name="countrycat"]:checked').each(function() {
    		courseCategoryIds += this.value + ",";
    	});
    	courseCategoryIds = courseCategoryIds.replace(/,\s*$/, "");
    	return courseCategoryIds;
    }
    
    function getSubjectAreaCatList(){
    	var subjectAreaCatIds="";
    	$('input[name="subjectcat"]:checked').each(function() {
    		subjectAreaCatIds += this.value + ",";
    	});
    	subjectAreaCatIds = subjectAreaCatIds.replace(/,\s*$/, "");
    	return subjectAreaCatIds;
    }
    
    function getCountryCatList(){
    	var countryCatIds="";
    	$('input[name="countrycat"]:checked').each(function() {
    		countryCatIds += this.value + ",";
    	});
    	countryCatIds = countryCatIds.replace(/,\s*$/, "");
    	return countryCatIds;
    }
    
    function getCourseCostType(){
    	var courseCostType="";
    	$('input[name="countrycosttpye"]:checked').each(function() {
    		courseCostType += this.value + ",";
    	});
    	courseCostType = courseCostType.replace(/,\s*$/, "");
    	return courseCostType;
    }
    
    A.all('.subject-area').each(
	  function (node) {
		  node.on('change', function(e) {
			  loadCourses();
		  });
	  }
	);
    
    A.all('.country').each(
		  function (node) {
			  node.on('change', function(e) {
				  loadCourses();
			  });
		  }
  	);
    
    A.all('.course-cost-type').each(
	  function (node) {
		  node.on('change', function(e) {
			  loadCourses();
		  });
	  }
	);
    
    A.one(".sortdropdown").on('change', function(e) {
		  loadCourses(null,null);
    });
    
    A.one(".reset-filter").on('click', function(e) {
    	A.all('.subject-area').each(function (node) { node._node.checked = false; });
    	A.all('.country').each(function (node) { node._node.checked = false; });
    	A.all('.course-cost-type').each(function (node) { node._node.checked = false; });    
    	loadCourses();
    });
   

});
</aui:script>