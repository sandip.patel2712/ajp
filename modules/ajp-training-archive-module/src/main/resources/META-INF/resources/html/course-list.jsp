
<%@ include file="/init.jsp"%>

<div class="coursecontainer">
	<div class="col-sm-9 coursedetails">
		<c:forEach items="${courseBeanList}" var="course" varStatus="loop">
			<portlet:renderURL var="viewCourseDetailURL">
				<portlet:param name="mvcRenderCommandName" value="/view_course" />
				<portlet:param name="articleId"
					value="${ course.journalArticle.articleId}" />
			</portlet:renderURL>

			<div class="row">

				<div class="col-sm-1"></div>
				<div class="col-sm-11">
					<div class="row text-left">
						<div>
							<%-- <h3>${course.title}</h3> --%>
							 <div>
	             			   <a href="${viewCourseDetailURL }" class="courselink">${course.title} </a>
	          				 </div>
						</div>
						<div>
							<h4>${course.country}</h4>
						</div>
						<div>
							<h4>${course.subjectArea}</h4>
						</div>
						<div>
							<p>${course.description}</p>
						</div>
						<div>
							<fmt:formatDate value="${course.courseStartDate}" pattern="dd/MM/yyyy" var="courseStartDate" />
							<fmt:formatDate value="${course.courseEndDate}" pattern="dd/MM/yyyy" var="courseEndDate" />
							<h4>Course Date ( ${courseStartDate} - ${courseEndDate} )</h4>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
	 <div class="col-sm-11">
                        <div class="row">
                            <footer class="container">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination pagination-lg">
                                        <li>
                                            <a href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#">6</a></li>
                                        <li><a href="#">7</a></li>
                                        <li><a href="#">8</a></li>
                                        <li><a href="#">9</a></li>
                                        <li><a href="#">10</a></li>
                                        <li>
                                            <a href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </footer>
                        </div>

                    </div>
	</div>
</div>
