package com.ajp.portal.announcements.portlet.display.context;

import com.ajp.portal.announcements.constants.AjpAnnouncementsConstants;
import com.ajp.portal.announcements.constants.AjpAnnouncementsModulePortletKeys;
import com.liferay.announcements.kernel.model.AnnouncementsEntry;
import com.liferay.announcements.kernel.service.AnnouncementsEntryLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletURL;

import javaslang.Lazy;

public class AnnouncementsDisplayContext {

	private static final Log _log = LogFactoryUtil.getLog(AnnouncementsDisplayContext.class);
	/**
	 * The search container for the page which lists AnnouncementsEntries.
	 */
	private Lazy<SearchContainer<AnnouncementsEntryBean>> _searchContainer;

	/**
	 * The keyword to filter Announcements by.
	 */
	private final String _keyword;

	public AnnouncementsDisplayContext(final LiferayPortletRequest liferayPortletRequest,
			final LiferayPortletResponse liferayPortletResponse) {

		final ThemeDisplay themeDisplay = (ThemeDisplay) liferayPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		_keyword = ParamUtil.getString(liferayPortletRequest, "keyword");
		String orderByCol = ParamUtil.getString(liferayPortletRequest, "orderByCol");
		String orderByType = ParamUtil.getString(liferayPortletRequest, "orderByType");
		_log.info("Search keyword is : " + _keyword);

		if (Validator.isNotNull(liferayPortletRequest) && Validator.isNotNull(liferayPortletResponse)) {

			this._searchContainer = Lazy.of(() -> {
				final PortletURL iteratorUrl = liferayPortletResponse.createRenderURL();
				if (Validator.isNotNull(_keyword)) {
					iteratorUrl.setParameter("keyword", _keyword);
				}

				final SearchContainer<AnnouncementsEntryBean> searchContainer = new SearchContainer<AnnouncementsEntryBean>(
						liferayPortletRequest, iteratorUrl, null, "no-announcements-entries-were-found");

				searchContainer.setClassName(AnnouncementsEntryBean.class.getName());

				DLFolder announcementsFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(), 0,
						AjpAnnouncementsConstants.ANNOUNCEMENT_FOLDER_NAME);
				List<AnnouncementsEntry> announcementsEntries = new ArrayList<AnnouncementsEntry>();

				announcementsEntries = getAnnouncementsEntries(liferayPortletRequest, _keyword,
						searchContainer.getStart(), searchContainer.getEnd(), orderByCol, orderByType);

				if (Validator.isNotNull(_keyword) || liferayPortletRequest.getPortletName()
						.contains(AjpAnnouncementsModulePortletKeys.AJPANNOUNCEMENTSWEBMODULE)) {
					searchContainer.setTotal(
							getAnnouncementsEntries(liferayPortletRequest, _keyword, -1, -1, orderByCol, orderByType)
									.size());
				} else {
					searchContainer.setTotal(AnnouncementsEntryLocalServiceUtil.getAnnouncementsEntriesCount());
				}

				final List<AnnouncementsEntryBean> results = new ArrayList<AnnouncementsEntryBean>(
						announcementsEntries.size());
				for (int j = 0; j < announcementsEntries.size(); j++) {
					final AnnouncementsEntry announcementsEntry = announcementsEntries.get(j);
					final AnnouncementsEntryBean announcementsEntryBean = new AnnouncementsEntryBean(
							announcementsEntry);
					String announcementsEntryImageURL = StringPool.BLANK;
					FileEntry announcementsEntryImage = getAnnouncementsEntryImage(announcementsEntry,
							announcementsFolder, themeDisplay);
					if (announcementsEntryImage != null) {
						announcementsEntryImageURL = getAnnouncementsEntryImageURL(announcementsEntryImage,
								liferayPortletRequest);
					}
					if (announcementsEntryImage != null) {
						announcementsEntryBean.setAnnouncementsEntryImage(announcementsEntryImage);
						announcementsEntryBean.setAnnouncementsEntryImageURL(announcementsEntryImageURL);
					}
					results.add(j, announcementsEntryBean);
				}
				searchContainer.setResults(results);

				return searchContainer;
			});
		}
	}

	private List<AnnouncementsEntry> getAnnouncementsEntries(LiferayPortletRequest liferayPortletRequest,
			String keyword, int start, int end, String orderByCol, String orderByType) {

		DynamicQuery dynamicQuery = AnnouncementsEntryLocalServiceUtil.dynamicQuery();

		if (liferayPortletRequest.getPortletName()
				.contains(AjpAnnouncementsModulePortletKeys.AJPANNOUNCEMENTSWEBMODULE)) {
			dynamicQuery.add(PropertyFactoryUtil.forName("displayDate").le(new Date()));
			dynamicQuery.add(PropertyFactoryUtil.forName("expirationDate").ge(new Date()));
		}
		if (Validator.isNotNull(keyword)) {
			Criterion keywordCriterion = RestrictionsFactoryUtil.or(
					RestrictionsFactoryUtil.ilike("title", StringPool.PERCENT + keyword + StringPool.PERCENT),
					RestrictionsFactoryUtil.ilike("content", StringPool.PERCENT + keyword + StringPool.PERCENT));
			dynamicQuery.add(keywordCriterion);
		}
		if (!orderByCol.isEmpty() && orderByCol != null) {
			if (orderByType.equalsIgnoreCase("asc")) {
				dynamicQuery.addOrder(OrderFactoryUtil.asc(orderByCol));
			} else {
				dynamicQuery.addOrder(OrderFactoryUtil.desc(orderByCol));
			}
		} else {
			dynamicQuery.addOrder(OrderFactoryUtil.desc("displayDate"));
		}

		dynamicQuery.setLimit(start, end);

		return AnnouncementsEntryLocalServiceUtil.dynamicQuery(dynamicQuery);
	}

	private String getAnnouncementsEntryImageURL(FileEntry announcementsEntryImage,
			LiferayPortletRequest liferayPortletRequest) {
		String URL = PortalUtil.getPortalURL(liferayPortletRequest) + "/documents/"
				+ announcementsEntryImage.getGroupId() + "/" + announcementsEntryImage.getFolderId() + "/"
				+ announcementsEntryImage.getTitle() + "/" + announcementsEntryImage.getUuid() + "?t="
				+ System.currentTimeMillis();
		return URL;
	}

	private FileEntry getAnnouncementsEntryImage(AnnouncementsEntry announcementsEntry, DLFolder announcementsFolder,
			ThemeDisplay themeDisplay) {
		DLFolder announcementsEntryFolder = null;
		if (announcementsFolder != null) {
			announcementsEntryFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(),
					announcementsFolder.getFolderId(), String.valueOf(announcementsEntry.getEntryId()));
		}
		if (announcementsEntryFolder != null) {
			List<FileEntry> announcementsFileEntris = null;
			try {
				announcementsFileEntris = DLAppServiceUtil.getFileEntries(themeDisplay.getCompanyGroupId(),
						announcementsEntryFolder.getFolderId());
			} catch (PortalException e) {
				_log.error(e.getMessage(), e);
			}
			if (announcementsFileEntris.size() > 0) {
				FileEntry announcementsImage = announcementsFileEntris.get(0);
				return announcementsImage;
			}
		}
		return null;
	}

	/**
	 * @return the search container that lists AnnouncementsEntries.
	 */
	public SearchContainer<AnnouncementsEntryBean> getSearchContainer() {
		return _searchContainer.get();
	}
}
