package com.ajp.portal.announcements.portlet.display.context;

import com.liferay.announcements.kernel.model.AnnouncementsEntry;
import com.liferay.portal.kernel.repository.model.FileEntry;

import java.util.Date;

public class AnnouncementsEntryBean {

	int priority;
	long entryId;
	String title;
	String content;
	String url;
	String type;
	Date createDate;
	Date displayDate;
	Date expirationDate;
	FileEntry announcementsEntryImage;
	String announcementsEntryImageURL;

	public AnnouncementsEntryBean() {
		super();
	}

	public AnnouncementsEntryBean(AnnouncementsEntry announcementsEntry) {
		this.priority = announcementsEntry.getPriority();
		this.entryId = announcementsEntry.getEntryId();
		this.title = announcementsEntry.getTitle();
		this.content = announcementsEntry.getContent();
		this.url = announcementsEntry.getUrl();
		this.type = announcementsEntry.getType();
		this.createDate = announcementsEntry.getCreateDate();
		this.displayDate = announcementsEntry.getDisplayDate();
		this.expirationDate = announcementsEntry.getExpirationDate();
	}

	public String getAnnouncementsEntryImageURL() {
		return announcementsEntryImageURL;
	}

	public void setAnnouncementsEntryImageURL(String announcementsEntryImageURL) {
		this.announcementsEntryImageURL = announcementsEntryImageURL;
	}

	public FileEntry getAnnouncementsEntryImage() {
		return announcementsEntryImage;
	}

	public void setAnnouncementsEntryImage(FileEntry announcementsEntryImage) {
		this.announcementsEntryImage = announcementsEntryImage;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public long getEntryId() {
		return entryId;
	}

	public void setEntryId(long entryId) {
		this.entryId = entryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getDisplayDate() {
		return displayDate;
	}

	public void setDisplayDate(Date displayDate) {
		this.displayDate = displayDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

}
