package com.ajp.portal.announcements.portlet.render;

import com.ajp.portal.announcements.constants.AjpAnnouncementsModulePortletKeys;
import com.liferay.announcements.kernel.model.AnnouncementsEntry;
import com.liferay.announcements.kernel.service.AnnouncementsEntryService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		immediate = true, 
		property = { 
			"javax.portlet.name=" + AjpAnnouncementsModulePortletKeys.AJPANNOUNCEMENTSWEBMODULE,
			"mvc.command.name=/jsp/view_entry_details" 
			},
		service = MVCRenderCommand.class
)
public class ViewAnnouncementsEntryMVCRenderCommand implements MVCRenderCommand{

	private static final Log _log = LogFactoryUtil.getLog(ViewAnnouncementsEntryMVCRenderCommand.class);

	private String JSP_PATH = "/jsp/view_entry_details.jsp";
	private AnnouncementsEntryService _announcementsEntryService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		long entryId = ParamUtil.getLong(renderRequest, "entryId");
		String backURL = ParamUtil.getString(renderRequest, "backURL");

		setAnnouncementsEntryDetails(renderRequest, entryId, backURL);
		
		return JSP_PATH;
	}

	private void setAnnouncementsEntryDetails(RenderRequest renderRequest, long entryId, String backURL) {
		try {
			AnnouncementsEntry announcementsEntry = _announcementsEntryService.getEntry(entryId);
			renderRequest.setAttribute("announcementsEntry", announcementsEntry);
			renderRequest.setAttribute("backURL", backURL);
		} catch (PortalException e) {
			_log.error(e.getMessage(), e);
		}
	}

	@Reference(unbind = "-")
	protected void setAnnouncementsEntryService(AnnouncementsEntryService announcementsEntryService) {

		_announcementsEntryService = announcementsEntryService;
	}
}
