package com.ajp.portal.announcements.constants;

public class AjpAnnouncementsConstants {

	public static final String ADD_ANNOUNCEMENT_ENTRY_ACTION = "addAnnouncementEntryAction";
	public static final String EDIT_ANNOUNCEMENT_ENTRY_ACTION = "editAnnouncementEntryAction";
	public static final String DELETE_ANNOUNCEMENT_ENTRY_ACTION = "deleteAnnouncementEntryAction";
	public static final String ANNOUNCEMENT_ENTRY_DISPLAY_CONTEXT = "ANNOUNCEMENT_ENTRY_DISPLAY_CONTEXT";
	
	public static final String TIME_AM = "AM";
	public static final String TIME_PM = "PM";
	
	public static final String ANNOUNCEMENT_FOLDER_NAME = "announcements";
}
