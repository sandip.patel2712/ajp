package com.ajp.portal.announcements.portlet.render;

import com.ajp.portal.announcements.constants.AjpAnnouncementsConstants;
import com.ajp.portal.announcements.constants.AjpAnnouncementsModulePortletKeys;
import com.ajp.portal.announcements.portlet.display.context.AnnouncementsDisplayContext;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.PortalUtil;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

@Component(
    property = {
            "javax.portlet.name=" + AjpAnnouncementsModulePortletKeys.AJPANNOUNCEMENTSADMINMODULE,
            "mvc.command.name=/announcements/search"
    },
    service = MVCRenderCommand.class
)
public class SearchAnnouncementsMVCRenderCommand implements MVCRenderCommand {

	private final String JSP_PATH = "/jsp/view_manage_entries.jsp";

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		renderRequest.setAttribute(AjpAnnouncementsConstants.ANNOUNCEMENT_ENTRY_DISPLAY_CONTEXT,
				new AnnouncementsDisplayContext(PortalUtil.getLiferayPortletRequest(renderRequest),
						PortalUtil.getLiferayPortletResponse(renderResponse)));

		return JSP_PATH;
	}
}
