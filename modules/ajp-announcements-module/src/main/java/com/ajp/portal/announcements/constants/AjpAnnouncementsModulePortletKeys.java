package com.ajp.portal.announcements.constants;

/**
 * @author Pc
 */
public class AjpAnnouncementsModulePortletKeys {

	public static final String AJPANNOUNCEMENTSADMINMODULE = "com_ajp_portal_announcements_AjpAnnouncementsAdminPortlet";
	public static final String AJPANNOUNCEMENTSWEBMODULE = "com_ajp_portal_announcements_AjpAnnouncementsWebPortlet";

}