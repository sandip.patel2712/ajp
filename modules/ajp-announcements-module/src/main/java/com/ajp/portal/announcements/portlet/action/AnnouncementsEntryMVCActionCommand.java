package com.ajp.portal.announcements.portlet.action;

import static com.ajp.portal.announcements.constants.AjpAnnouncementsConstants.TIME_PM;

import com.ajp.portal.announcements.constants.AjpAnnouncementsConstants;
import com.ajp.portal.announcements.constants.AjpAnnouncementsModulePortletKeys;
import com.liferay.announcements.kernel.exception.EntryExpirationDateException;
import com.liferay.announcements.kernel.model.AnnouncementsEntry;
import com.liferay.announcements.kernel.service.AnnouncementsEntryService;
import com.liferay.document.library.kernel.exception.NoSuchFolderException;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
	immediate = true,
	property = {
			"javax.portlet.name=" + AjpAnnouncementsModulePortletKeys.AJPANNOUNCEMENTSADMINMODULE,
			"javax.portlet.name=" + AjpAnnouncementsModulePortletKeys.AJPANNOUNCEMENTSWEBMODULE,
			"mvc.command.name=/jsp/editAnnouncementEntry"
			},
	service = MVCActionCommand.class
)
public class AnnouncementsEntryMVCActionCommand extends BaseMVCActionCommand {

	private static final Log _log = LogFactoryUtil.getLog(AnnouncementsEntryMVCActionCommand.class);
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private AnnouncementsEntryService _announcementsEntryService;
	
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		final ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		ServiceContext serviceContext = ServiceContextFactory.getInstance(AnnouncementsEntry.class.getName(), actionRequest);
		String actionType = ParamUtil.getString(actionRequest, "actionType");
		String title = ParamUtil.getString(actionRequest, "announcementTitle");
		String content = ParamUtil.getString(actionRequest, "announcementContent");
		String type = ParamUtil.getString(actionRequest, "announcementType");
		int priority = ParamUtil.getInteger(actionRequest, "announcementPriority");
		boolean alert = false;
		boolean displayImmediately = ParamUtil.getBoolean(actionRequest, "displayImmediately");
		String displayDateStr = ParamUtil.getString(actionRequest, "displayDate");
		Date displayDate = simpleDateFormat.parse(displayDateStr);
		String displayTime = ParamUtil.getString(actionRequest, "displayTime");
		String expirationDateStr = ParamUtil.getString(actionRequest, "expirationDate");
		Date expirationDate = simpleDateFormat.parse(expirationDateStr);
		String expirationTime = ParamUtil.getString(actionRequest, "expirationTime");
		
		AnnouncementsEntry announcementsEntry = null;
		if (AjpAnnouncementsConstants.EDIT_ANNOUNCEMENT_ENTRY_ACTION.equals(actionType)) {
			announcementsEntry = editAnnouncementEntry(actionRequest, actionRequest, themeDisplay, title, content, type, priority,
					alert, displayDate, expirationDate, displayTime, expirationTime, displayImmediately, serviceContext);
		} else if (AjpAnnouncementsConstants.ADD_ANNOUNCEMENT_ENTRY_ACTION.equals(actionType)) {
			announcementsEntry = addAnnouncementEntry(actionRequest, themeDisplay, title, content, type, priority, alert, displayDate,
					expirationDate, displayTime, expirationTime, displayImmediately);
		}
		addAnnouncementImage(actionRequest, themeDisplay, announcementsEntry, serviceContext);
	}

	private AnnouncementsEntry addAnnouncementEntry(ActionRequest actionRequest, ThemeDisplay themeDisplay, String title,
			String content, String type, int priority, boolean alert, Date displayDate, Date expirationDate,
			String displayTime, String expirationTime, boolean displayImmediately) {
		
		String displayTiming = getHoursAndMinutes(displayTime);
		int displayDateHour = Integer.parseInt((String) displayTiming.subSequence(0, displayTiming.indexOf(",")));
		int displayDateMinute = Integer.parseInt((String) displayTiming.subSequence(displayTiming.indexOf(",") + 1, displayTiming.length())); ;
		String expirationTiming = getHoursAndMinutes(expirationTime);
		int expirationDateHour =  Integer.parseInt((String) expirationTiming.subSequence(0, expirationTiming.indexOf(",")));
		int expirationDateMinute = Integer.parseInt((String) expirationTiming.subSequence(expirationTiming.indexOf(",") + 1, expirationTiming.length())); ;
		int displayYear = displayDate.getYear() + 1900;
		int expirationYear = expirationDate.getYear() + 1900;
		
		try {
			AnnouncementsEntry announcementsEntry = _announcementsEntryService.addEntry(themeDisplay.getPlid(), 0L, 0L,
					title, content, StringPool.BLANK, type, displayDate.getMonth(), displayDate.getDate(), displayYear,
					displayDateHour, displayDateMinute, displayImmediately, expirationDate.getMonth(),
					expirationDate.getDate(), expirationYear, expirationDateHour, expirationDateMinute, priority,
					alert);
			SessionMessages.add(actionRequest, "announcements-entry-added");

			Role guest = RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(), RoleConstants.GUEST);
			ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),
					AnnouncementsEntry.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
					String.valueOf(announcementsEntry.getEntryId()), guest.getRoleId(),
					new String[] { ActionKeys.VIEW });

			return announcementsEntry;

		} catch (NumberFormatException | PortalException e) {
			if (e instanceof EntryExpirationDateException) {
				SessionErrors.add(actionRequest, "expiration-date-error");
			} else {
				SessionErrors.add(actionRequest, "error-edit-announcements-entry");
			}
			_log.error(e.getMessage(), e);
		}

		return null;
	}

	private void addAnnouncementImage(ActionRequest actionRequest, ThemeDisplay themeDisplay,
			AnnouncementsEntry announcementsEntry, ServiceContext serviceContext) {
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		File imageFile = uploadRequest.getFile("announcementImage");
		String imageFileName = uploadRequest.getFileName("announcementImage");
		long repositoryId = themeDisplay.getCompanyGroupId();

		if (imageFile != null && imageFileName != StringPool.BLANK) {
			try {
				Folder announcementFolder = getAnnouncementFolder(themeDisplay, serviceContext);
				Role guest = RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(), RoleConstants.GUEST);
				InputStream imageFileInputstream = new FileInputStream(imageFile);
				String caseMimeType = MimeTypesUtil.getContentType(imageFile.getName());

				DLFolder announcementEntryDlFolder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), repositoryId, repositoryId, false, announcementFolder.getFolderId(),
						String.valueOf(announcementsEntry.getEntryId()), StringPool.BLANK, false, serviceContext);

				FileEntry fileEntry = DLAppServiceUtil.addFileEntry(repositoryId, announcementEntryDlFolder.getFolderId(),
						imageFileName, caseMimeType, imageFileName, StringPool.BLANK, StringPool.BLANK,
						imageFileInputstream, imageFile.length(), serviceContext);

				ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),
						DLFileEntry.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
						String.valueOf(fileEntry.getFileEntryId()), guest.getRoleId(),
						new String[] { ActionKeys.VIEW });
				ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),DLFolder.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
						String.valueOf(announcementEntryDlFolder.getFolderId()),guest.getRoleId(), new String[] {ActionKeys.VIEW});

			} catch (PortalException | FileNotFoundException e) {
				_log.error(e.getMessage(), e);
			}
		}
	}

	private Folder getAnnouncementFolder(ThemeDisplay themeDisplay, ServiceContext serviceContext) {
		long repositoryId = themeDisplay.getCompanyGroupId();
		Folder announcementDlFolder = null;
		try {
			announcementDlFolder = DLAppServiceUtil.getFolder(repositoryId, 0,
					AjpAnnouncementsConstants.ANNOUNCEMENT_FOLDER_NAME);
		} catch (PortalException e) {
			// Ignore Exception 
		}
		try {
			Role guest = RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(), RoleConstants.GUEST);
			if (Validator.isNull(announcementDlFolder)) {
				announcementDlFolder = DLAppServiceUtil.addFolder(repositoryId, 0,
						AjpAnnouncementsConstants.ANNOUNCEMENT_FOLDER_NAME, StringPool.BLANK, serviceContext);
				ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),
						DLFolder.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
						String.valueOf(announcementDlFolder.getFolderId()), guest.getRoleId(),
						new String[] { ActionKeys.VIEW });
				return announcementDlFolder;
			} else {
				return announcementDlFolder;
			}
		} catch (PortalException e) {
			_log.error(e.getMessage(), e);
		}
		return null;
	}

	private String getHoursAndMinutes(String time) {
		int hours = 0;
		int minutes = 0;
		String[] times = time.split(" ");
		if (times.length > 0) {
			String timeFormate = times[1];
			String[] timeArray = times[0].split(":");
			if (timeArray.length > 0) {
				if (TIME_PM.equalsIgnoreCase(timeFormate)) {
					if (Integer.parseInt(timeArray[0]) != 12) {
						hours = Integer.parseInt(timeArray[0]) + 12;
					} else {
						hours = Integer.parseInt(timeArray[0]);
					}
				} else {
					hours = Integer.parseInt(timeArray[0]);
				}
				minutes = Integer.parseInt(timeArray[1]);
			}
		}
		String result = hours + "," + minutes;
		return result;
	}

	private AnnouncementsEntry editAnnouncementEntry(ActionRequest actionRequest, ActionRequest actionRequest2,
			ThemeDisplay themeDisplay, String title, String content, String type, int priority,
			boolean alert, Date displayDate, Date expirationDate, String displayTime, String expirationTime, boolean displayImmediately, ServiceContext serviceContext2) {
		String displayTiming = getHoursAndMinutes(displayTime);
		int displayDateHour = Integer.parseInt((String) displayTiming.subSequence(0, displayTiming.indexOf(",")));
		int displayDateMinute = Integer.parseInt((String) displayTiming.subSequence(displayTiming.indexOf(",") + 1, displayTiming.length())); ;
		String expirationTiming = getHoursAndMinutes(expirationTime);
		int expirationDateHour =  Integer.parseInt((String) expirationTiming.subSequence(0, expirationTiming.indexOf(",")));
		int expirationDateMinute = Integer.parseInt((String) expirationTiming.subSequence(expirationTiming.indexOf(",") + 1, expirationTiming.length())); ;
		int displayYear = displayDate.getYear() + 1900;
		int expirationYear = expirationDate.getYear() + 1900;
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		File imageFile = uploadRequest.getFile("announcementImage");
		String imageFileName = uploadRequest.getFileName("announcementImage");

		try {
			long entryId = ParamUtil.getLong(actionRequest, "entryId");
			ServiceContext serviceContext = ServiceContextFactory.getInstance(AnnouncementsEntry.class.getName(),
                    actionRequest);
			AnnouncementsEntry announcementsEntry = _announcementsEntryService.updateEntry(entryId, title, content, StringPool.BLANK, type, displayDate.getMonth(), displayDate.getDate(), displayYear,
					displayDateHour, displayDateMinute, displayImmediately, expirationDate.getMonth(), expirationDate.getDate(),
					expirationYear, expirationDateHour, expirationDateMinute, priority);
			SessionMessages.add(actionRequest, "announcements-entry-updated");
			if(imageFile != null && imageFileName != StringPool.BLANK ) {
				updateAnnouncementImage(themeDisplay, imageFile, imageFileName, entryId, serviceContext );
			}
			return announcementsEntry;
		} catch (PortalException e) {
			if (e instanceof EntryExpirationDateException) {
				SessionErrors.add(actionRequest, "expiration-date-error");
			} else {
				SessionErrors.add(actionRequest, "error-edit-announcements-entry");
			}
			_log.error(e.getMessage(), e);
		}
		return null;
	}

	private void updateAnnouncementImage(ThemeDisplay themeDisplay, File imageFile, String imageFileName, long entryId, ServiceContext serviceContext) {
		long repositoryId = themeDisplay.getCompanyGroupId();
		Folder announcementFolder = getAnnouncementFolder(themeDisplay, serviceContext);
		DLFileEntry announcementEntryImage = null;
		InputStream announcementEntryImageInputstream = null;
		String caseMimeType = MimeTypesUtil.getContentType(imageFile.getName());
		try {
			Role guest = RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(), RoleConstants.GUEST);
			Folder announcementEntryDlFolder = DLAppServiceUtil.getFolder(repositoryId, announcementFolder.getFolderId(), String.valueOf(entryId));
			if (Validator.isNull(announcementEntryDlFolder)) {
				announcementEntryDlFolder = DLAppServiceUtil.addFolder(repositoryId, announcementFolder.getFolderId(), String.valueOf(entryId), StringPool.BLANK, serviceContext);
				ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),DLFolder.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
						String.valueOf(announcementEntryDlFolder.getFolderId()),guest.getRoleId(), new String[] {ActionKeys.VIEW});
			} else {
				announcementEntryImageInputstream = new FileInputStream(imageFile);
				List<DLFileEntry> announcementEntryFolderEntris = DLFileEntryLocalServiceUtil.getFileEntries(repositoryId,
						announcementEntryDlFolder.getFolderId());
				if(announcementEntryFolderEntris.size() > 0) {
					announcementEntryImage = announcementEntryFolderEntris.get(0);
					_log.info("announcementEntryImage : " + announcementEntryImage.getFileEntryId());
					DLAppServiceUtil.updateFileEntry(announcementEntryImage.getFileEntryId(),
							String.valueOf(entryId), caseMimeType, imageFileName, StringPool.BLANK, "",
							true, imageFile, serviceContext);
				} else {
					FileEntry fileEntry= DLAppServiceUtil.addFileEntry(repositoryId, announcementEntryDlFolder.getFolderId(), imageFileName, caseMimeType,
							imageFileName, StringPool.BLANK, StringPool.BLANK, announcementEntryImageInputstream,  imageFile.length(), serviceContext);
	
					ResourcePermissionLocalServiceUtil.setResourcePermissions(themeDisplay.getCompanyId(),DLFileEntry.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
							String.valueOf(fileEntry.getFileEntryId()),guest.getRoleId(), new String[] {ActionKeys.VIEW});
	
				}
			}
		} catch (NoSuchFolderException noSuchFolderException) {
			//Ignore Exception
		} catch (PortalException | FileNotFoundException e) {
			_log.error(e.getMessage(), e);
		}
	}

	@Reference(unbind = "-")
	protected void setAnnouncementsEntryService(AnnouncementsEntryService announcementsEntryService) {

		_announcementsEntryService = announcementsEntryService;
	}
}
