package com.ajp.portal.announcements.portlet;

import com.ajp.portal.announcements.constants.AjpAnnouncementsConstants;
import com.ajp.portal.announcements.constants.AjpAnnouncementsModulePortletKeys;
import com.ajp.portal.announcements.portlet.display.context.AnnouncementsDisplayContext;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PortalUtil;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=AJP",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.header-portlet-css=/css/announcement-details-page.css",
		"com.liferay.portlet.header-portlet-css=/css/announcement-main-page.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Ajp Announcements Web Module",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/jsp/view_entries.jsp",
		"javax.portlet.name=" + AjpAnnouncementsModulePortletKeys.AJPANNOUNCEMENTSWEBMODULE,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class AjpAnnouncementsWebPortlet extends MVCPortlet {

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		renderRequest.setAttribute(AjpAnnouncementsConstants.ANNOUNCEMENT_ENTRY_DISPLAY_CONTEXT,
				new AnnouncementsDisplayContext(PortalUtil.getLiferayPortletRequest(renderRequest),
						PortalUtil.getLiferayPortletResponse(renderResponse)));
		super.doView(renderRequest, renderResponse);
	}
}