package com.ajp.portal.announcements.portlet.render;

import static com.ajp.portal.announcements.constants.AjpAnnouncementsConstants.ADD_ANNOUNCEMENT_ENTRY_ACTION;
import static com.ajp.portal.announcements.constants.AjpAnnouncementsConstants.DELETE_ANNOUNCEMENT_ENTRY_ACTION;
import static com.ajp.portal.announcements.constants.AjpAnnouncementsConstants.EDIT_ANNOUNCEMENT_ENTRY_ACTION;

import com.ajp.portal.announcements.constants.AjpAnnouncementsConstants;
import com.ajp.portal.announcements.constants.AjpAnnouncementsModulePortletKeys;
import com.ajp.portal.announcements.portlet.display.context.AnnouncementsDisplayContext;
import com.liferay.announcements.kernel.model.AnnouncementsEntry;
import com.liferay.announcements.kernel.model.AnnouncementsEntryConstants;
import com.liferay.announcements.kernel.service.AnnouncementsEntryService;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		immediate = true, 
		property = { 
			"javax.portlet.name=" + AjpAnnouncementsModulePortletKeys.AJPANNOUNCEMENTSADMINMODULE,
			"javax.portlet.name=" + AjpAnnouncementsModulePortletKeys.AJPANNOUNCEMENTSWEBMODULE,
			"mvc.command.name=/jsp/editAnnouncementEntry", 
			"mvc.command.name=/jsp/view_manage_entries"
			},
		service = MVCRenderCommand.class
)
public class AnnouncementsEntryMVCRenderCommand implements MVCRenderCommand{

	private static final Log _log = LogFactoryUtil.getLog(AnnouncementsEntryMVCRenderCommand.class);

	private String JSP_PATH = "/jsp/view_manage_entries.jsp";
	private AnnouncementsEntryService _announcementsEntryService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		final ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String actionType = ParamUtil.getString(renderRequest, "actionType");
		_log.info("actionType:: " + actionType);

		setAnnouncementsEntryTypesLists(renderRequest);
		setDisplayContext(renderRequest, renderResponse);

		if (ADD_ANNOUNCEMENT_ENTRY_ACTION.equals(actionType)) {
			JSP_PATH = "/jsp/edit_announcement_entry.jsp";
		} else if (EDIT_ANNOUNCEMENT_ENTRY_ACTION.equals(actionType)) {
			setEditAnnouncementsEntryDetails(renderRequest);
		} else if (DELETE_ANNOUNCEMENT_ENTRY_ACTION.equals(actionType)) {
			deleteAnnouncementEntry(themeDisplay, renderRequest);
		}

		return JSP_PATH;
	}

	private void setDisplayContext(RenderRequest renderRequest, RenderResponse renderResponse) {
		renderRequest.setAttribute(AjpAnnouncementsConstants.ANNOUNCEMENT_ENTRY_DISPLAY_CONTEXT,
                new AnnouncementsDisplayContext(PortalUtil.getLiferayPortletRequest(renderRequest),
                        PortalUtil.getLiferayPortletResponse(renderResponse)));
	}

	private void setAnnouncementsEntryTypesLists(RenderRequest renderRequest) {
		List<String> types = new ArrayList<String>();
		for (String curType : AnnouncementsEntryConstants.TYPES) {
			types.add(curType);
		}
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyy");  
		String defaultDate = formatter.format(new Date());
		String defaultTime = "09:10 AM";
		renderRequest.setAttribute("announcementsEntryTypes", types);
		renderRequest.setAttribute("defaultDate", defaultDate);
		renderRequest.setAttribute("defaultTime", defaultTime);
	}

	private void setEditAnnouncementsEntryDetails(RenderRequest renderRequest) {
		long entryId = ParamUtil.getLong(renderRequest, "entryId");
		String redirect = (String) renderRequest.getAttribute("currentURL");
		renderRequest.setAttribute("redirect", redirect);
		try {
			if (entryId != 0) {
				AnnouncementsEntry announcementsEntry = _announcementsEntryService.getEntry(entryId);
				if (Validator.isNotNull(announcementsEntry)) {
					renderRequest.setAttribute("announcementEntry", announcementsEntry);
					JSP_PATH = "/jsp/edit_announcement_entry.jsp";
				}
			}	
		} catch (PortalException e) {
			SessionErrors.add(renderRequest, "edit-legalCase-error");
			_log.error(e.getMessage(), e);
		}
	}

	private void deleteAnnouncementEntry(ThemeDisplay themeDisplay, RenderRequest renderRequest) {
		long entryId = 0L;
		try {
			entryId = ParamUtil.getLong(renderRequest, "entryId");
			if (entryId != 0) {
				_announcementsEntryService.deleteEntry(entryId);
				deleteAnnouncementImage(themeDisplay, entryId);
				JSP_PATH = "/jsp/view_manage_entries.jsp";
			}
			SessionMessages.add(renderRequest, "announcements-entry-deleted");
		} catch (PortalException e) {
			SessionErrors.add(renderRequest, "error-edit-announcements-entry");
			_log.error(e.getMessage(), e);
		}

		_log.debug("AnnouncementEntry (" + entryId + ") is deleted successfully.");
	}

	private void deleteAnnouncementImage(ThemeDisplay themeDisplay, long entryId) {
		try {
			DLFolder announcementDlFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(), 0, AjpAnnouncementsConstants.ANNOUNCEMENT_FOLDER_NAME);
			DLFolder announcementEntryDlFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(), announcementDlFolder.getFolderId(), String.valueOf(entryId));
			if(announcementEntryDlFolder != null) {
				DLFolderLocalServiceUtil.deleteFolder(announcementEntryDlFolder, true);
			}
		} catch (PortalException e) {
			_log.error(e.getMessage(), e);
		}
	}

	@Reference(unbind = "-")
	protected void setAnnouncementsEntryService(AnnouncementsEntryService announcementsEntryService) {

		_announcementsEntryService = announcementsEntryService;
	}
}
