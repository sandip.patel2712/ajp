<%@ include file="/init.jsp"%>

<liferay-ui:error key="expiration-date-error"
	message="expiration-date-error" />
<liferay-ui:error key="error-edit-announcements-entry"
	message="error-edit-announcements-entry" />
<liferay-ui:success key="announcements-entry-added"
	message="announcements-entry-was-created-successfully" />
<liferay-ui:success key="announcements-entry-updated"
	message="announcements-entry-was-updated-successfully" />
<liferay-ui:success key="announcements-entry-deleted"
	message="announcements-entry-was-deleted-successfully" />

<%
	String orderByCol = ParamUtil.getString(request, "orderByCol");
	String orderByType = ParamUtil.getString(request, "orderByType");
	String sortingOrder = orderByType;
	if (orderByType.equals("desc")) {
		orderByType = "desc";
	} else {
		orderByType = "asc";
	}

	if (Validator.isNull(orderByType)) {
		orderByType = "desc";
	}
	
%>

<portlet:renderURL var="addAnnouncementEntry">
	<portlet:param name="mvcRenderCommandName"
		value="/jsp/editAnnouncementEntry" />
	<portlet:param name="actionType" value="addAnnouncementEntryAction" />
</portlet:renderURL>

<portlet:renderURL var="searchURL">
	<portlet:param name="mvcRenderCommandName" value="/announcements/search" />
</portlet:renderURL>

<liferay-frontend:management-bar>
	<liferay-frontend:management-bar-buttons>
		<liferay-frontend:management-bar-filters>
			<liferay-frontend:management-bar-sort orderByCol="<%= orderByCol %>"
				orderByType="<%= orderByType %>"
				orderColumns='<%= new String[] {"title","displayDate","expirationDate"} %>'
				portletURL="<%= displayContext.getSearchContainer().getIteratorURL() %>" />
		</liferay-frontend:management-bar-filters>
	</liferay-frontend:management-bar-buttons>
</liferay-frontend:management-bar>

<div class="ajp-announcements-container">
	<div class="row">
		<div class="col-md-6">
			<aui:button onClick="${addAnnouncementEntry}"
				value="label-add-announcement-entry"></aui:button>
		</div>
		<div class="col-md-6">
			<aui:form action="<%=searchURL%>" name="searchFm" cssClass="search-announcements-form" >
				<liferay-ui:input-search markupView="lexicon" id="keyword"
					name="keyword" />
			</aui:form>
		</div>
	</div>
</div>

<div class="ajp-announcements-entries">
	<liferay-ui:search-container id="announcementsEntries"
		searchContainer="<%=displayContext.getSearchContainer()%>"
		iteratorURL="<%=displayContext.getSearchContainer().getIteratorURL()%>"
		orderByCol="<%= orderByCol %>" orderByType="<%= orderByType %>">

		<liferay-ui:search-container-row
			className="com.ajp.portal.announcements.portlet.display.context.AnnouncementsEntryBean"
			modelVar="announcementsEntryBean">

			<portlet:renderURL var="editAnnouncementEntry">
				<portlet:param name="mvcRenderCommandName"
					value="/jsp/editAnnouncementEntry" />
				<portlet:param name="actionType" value="editAnnouncementEntryAction" />
				<portlet:param name="entryId"
					value="${announcementsEntryBean.entryId}" />
				<portlet:param name="currentURL"
					value="<%=themeDisplay.getURLCurrent()%>" />
			</portlet:renderURL>

			<portlet:renderURL var="deleteAnnouncementEntry">
				<portlet:param name="mvcRenderCommandName" value="/jsp/view_manage_entries" />
				<portlet:param name="actionType" value="deleteAnnouncementEntryAction" />
				<portlet:param name="entryId"
					value="${announcementsEntryBean.entryId}" />
			</portlet:renderURL>

			<fmt:formatDate pattern="MM/dd/yyyy"
				value="${announcementsEntryBean.createDate}" var="createDate" />

			<liferay-ui:search-container-column-text name="title"
				value="${announcementsEntryBean.title}" />

			<liferay-ui:search-container-column-text name="type"
				value="${announcementsEntryBean.type}" />

			<liferay-ui:search-container-column-text name="create-date"
				value="${createDate}" />

			<fmt:formatDate pattern="MM/dd/yyyy"
				value="${announcementsEntryBean.displayDate}" var="displayDate" />

			<liferay-ui:search-container-column-text name="display-date"
				value="${displayDate}" />

			<fmt:formatDate pattern="MM/dd/yyyy"
				value="${announcementsEntryBean.expirationDate}"
				var="expirationDate" />

			<liferay-ui:search-container-column-text name="expiration-date"
				value="${expirationDate}" />

			<liferay-ui:search-container-column-text name="<%=StringPool.BLANK%>">
				<liferay-ui:icon-menu direction="left-side"
					icon="<%=StringPool.BLANK%>" markupView="lexicon"
					message="<%=StringPool.BLANK%>" showWhenSingleIcon="<%=true%>">
					<liferay-ui:icon message="edit" url="${editAnnouncementEntry}" />
					<liferay-ui:icon-delete message="delete"
						confirmation="are-you-sure-you-want-to-delete-the-announcement-entry"
						url="${deleteAnnouncementEntry}" />
				</liferay-ui:icon-menu>
			</liferay-ui:search-container-column-text>
		</liferay-ui:search-container-row>
		<liferay-ui:search-iterator markupView="lexicon" paginate="<%=true%>" />
	</liferay-ui:search-container>
</div>