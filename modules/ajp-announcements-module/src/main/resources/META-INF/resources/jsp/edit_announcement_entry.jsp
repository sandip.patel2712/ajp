<%@ include file="../init.jsp"%>
<fmt:setTimeZone var="timeZone" value = "<%=themeDisplay.getUser().getTimeZone().getID() %>" />

<portlet:renderURL var="viewURL">
</portlet:renderURL>

<portlet:actionURL name="/jsp/editAnnouncementEntry"
	var="addAnnouncementEntry">
	<portlet:param name="actionType"
		value="${not empty announcementEntry ? 'editAnnouncementEntryAction' : 'addAnnouncementEntryAction'}" />
</portlet:actionURL>

<aui:form enctype="multipart/form-data" method="post" name="fm"
	cssClass="announcementEntryForm form-group"
	onSubmit='<%="event.preventDefault(); " + renderResponse.getNamespace() + "saveEntry();"%>'>

	<aui:input name="entryId" type="hidden"
		value="${announcementEntry.entryId }" />
	<aui:fieldset>
		<aui:input id="announcementTitle" name="announcementTitle"
			value="${announcementEntry.title }" label="announcement-title">
			<aui:validator name="required" errorMessage="error-required" />
			<aui:validator name="maxLength" errorMessage="error-maxLength">255</aui:validator>
		</aui:input>

		<aui:field-wrapper label="announcement-content" required="<%=true%>">
			<liferay-ui:input-editor contents="${announcementEntry.content}"
				editorName='label-error' />
			<aui:input name="announcementContent" type="hidden" />
		</aui:field-wrapper>

		<aui:select name="announcementType" label="type">
			<c:forEach items="${announcementsEntryTypes}"
				var="announcementsEntryType">
				<aui:option value="${announcementsEntryType}"
					label="${announcementsEntryType}"
					selected="${announcementsEntryType == announcementEntry.type }"></aui:option>
			</c:forEach>
		</aui:select>

		<aui:select name="announcementPriority" label="priority">
			<aui:option label="normal"
				selected="${0 == announcementEntry.priority }" value="0" />
			<aui:option label="important"
				selected="${1 == announcementEntry.priority }" value="1" />
		</aui:select>

		<label><liferay-ui:message key="label-display-date" /></label>
		<div class="row display-date">
			<div class="col-md-2">
				<fmt:formatDate pattern="MM/dd/yyyy"
				value="${announcementEntry.displayDate}" var="displayDate" />
				<aui:input name="displayDate" label="<%=StringPool.BLANK %>"
					value="${not empty announcementEntry ? displayDate : defaultDate}"
					dateTogglerCheckboxLabel="display-immediately"
					cssClass="announcement-date-picker" />
			</div>
			<div class="col-md-2">
				<fmt:formatDate pattern="HH:mm" timeZone="${timeZone}" type="both"
					value="${announcementEntry.displayDate}" var="displayDateObj" />
				<c:set var = "displayTimeParts" value = "${fn:split(displayDateObj, ':')}" />	
				<c:set var="displayDateHours" value="${displayTimeParts[0] >= 13 ? displayTimeParts[0] -12 : displayTimeParts[0]}" />
				<c:set var="displayDateFormate" value="${displayTimeParts[0] > 12 ? 'PM' : 'AM'}" />
				<c:set var="displayDateMinutes" value="${displayTimeParts[1]}" />
				<c:set var="displayTime" value="${displayDateHours }:${displayDateMinutes } ${ displayDateFormate}" />
				<aui:input name="displayTime" cssClass="announcement-time-picker"
					value="${not empty announcementEntry ? displayTime : defaultTime}"
					label="<%=StringPool.BLANK %>" />
			</div>
		</div>
		<aui:input name="displayImmediately" label="display-immediately"
			type="checkbox"
			checked="${not empty announcementEntry ? (announcementEntry.displayDate == announcementEntry.createDate || announcementEntry.displayDate == announcementEntry.modifiedDate) : true}"
			onChange="checkDisplayImmediately()" value="1"></aui:input>

		<label><liferay-ui:message key="label-expiration-date" /></label>
		<div class="row">
			<div class="col-md-2">
				<fmt:formatDate pattern="MM/dd/yyyy" timeZone="${timeZone}"
					type="both" value="${announcementEntry.expirationDate}"
					var="expirationDate" />
				<aui:input name="expirationDate" label="<%=StringPool.BLANK %>"
					value="${not empty announcementEntry ? expirationDate : defaultDate}"
					cssClass="announcement-date-picker" />
			</div>
			<div class="col-md-2">
				<fmt:formatDate pattern="HH:mm"
					timeZone="${timeZone}" type="both"
					value="${announcementEntry.expirationDate}"
					var="expirationDateObj" />
				<c:set var = "expirationTimeParts" value = "${fn:split(expirationDateObj, ':')}" />
				<c:set var="expirationDateHours" value="${expirationTimeParts[0] >= 13 ? expirationTimeParts[0] -12 : expirationTimeParts[0]}" />
				<c:set var="expirationDateFormate" value="${expirationTimeParts[0] > 12 ? 'PM' : 'AM'}" />
				<c:set var="expirationDateMinutes" value="${expirationTimeParts[1]}" />
				<c:set var="expirationTime" value="${expirationDateHours }:${expirationDateMinutes } ${ expirationDateFormate}" />
				<aui:input name="expirationTime" cssClass="announcement-time-picker"
					value="${not empty announcementEntry ? expirationTime : defaultTime}"
					label="<%=StringPool.BLANK %>" />
			</div>
		</div>

		<aui:input type="file" name="announcementImage"
			label="announcement-image" id="announcementImage">
			<aui:validator name="acceptFiles" errorMessage="accept-files-error">'jpeg,jpg,png'</aui:validator>
		</aui:input>
	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit"
			value="${not empty legalCase ? 'label-update' : 'label-save'}" />
		<aui:button type="cancel" onClick="${viewURL}%>" />
	</aui:button-row>
</aui:form>

<aui:script>
jQuery.noConflict();
(function($) {
    $(function() {
	    YUI().use('aui-base','aui-datepicker', function(Y) {
	        new Y.DatePicker({
	            trigger: '.announcement-date-picker',
	            popover: {
	                zIndex: 100,
	            }
	        });
	    });
	    
	    YUI().use('aui-timepicker', function(Y) {
	    	new Y.TimePicker({
				trigger: '.announcement-time-picker',
		       	popover: {
		        	zIndex: 1
		       	}
		     });
		});
		checkDisplayImmediately();
	});
})(jQuery);

	function <portlet:namespace />getContent() {
		return window.<portlet:namespace />editor.getHTML();
	}

	function <portlet:namespace />saveEntry() {
		document.<portlet:namespace />fm.action = '${addAnnouncementEntry}';
		document.<portlet:namespace />fm.target = '';
		document.<portlet:namespace />fm.<portlet:namespace />announcementContent.value = <portlet:namespace />getContent();
		document.getElementById("<portlet:namespace />displayDate").disabled = false;
		document.getElementById("<portlet:namespace />displayTime").disabled = false;
		submitForm(document.<portlet:namespace />fm);
	}

	function checkDisplayImmediately(){
		var checkBoxValue = document.getElementById("<portlet:namespace />displayImmediately").checked;
		if(checkBoxValue == true){
			document.getElementById("<portlet:namespace />displayDate").disabled = true;
			document.getElementById("<portlet:namespace />displayTime").disabled = true;
		} else {
			document.getElementById("<portlet:namespace />displayDate").disabled = false;
			document.getElementById("<portlet:namespace />displayTime").disabled = false;
		}
	}
</aui:script>