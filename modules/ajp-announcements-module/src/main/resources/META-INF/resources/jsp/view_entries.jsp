<%@ include file="/init.jsp"%>

<div class="container-fluid">
	<div class="row announcementpanel">
		<h2 class="announcementpanelheader">
			<liferay-ui:message key="label-announcements" />
		</h2>
	</div>
</div>

<c:forEach items="<%=displayContext.getSearchContainer().getResults()%>"
	var="announcementsEntryBean" varStatus="count">
	<c:choose>
		<c:when test="${count.index > 4}">
			<div class="container-fluid announcementlistpanel more-entires">
		</c:when>
		<c:otherwise>
			<div class="container-fluid announcementlistpanel">
		</c:otherwise>
	</c:choose>

	<div class="row content announcementinfo">
		<div class="col-sm-3">
			<div class="announcementimage">
				<img
					src="${announcementsEntryBean.announcementsEntryImageURL }"
					alt="announcement image" onerror=this.src="<%=request.getContextPath()%>/image/announcement-default-img.png">
			</div>
		</div>
		<div class="col-sm-9 text-left">
			<portlet:renderURL var="viewAnnouncementEntryDetails">
				<portlet:param name="mvcRenderCommandName"
					value="/jsp/view_entry_details" />
				<portlet:param name="entryId"
					value="${announcementsEntryBean.entryId}" />
				<portlet:param name="backURL"
					value="<%=themeDisplay.getURLCurrent()%>"></portlet:param>
			</portlet:renderURL>
			<h2 class="announcementtitle">
				<a href="${viewAnnouncementEntryDetails}"  >
					${announcementsEntryBean.title}</a>
			</h2>
			<div class="announcementdata">
				<p>${announcementsEntryBean.content}</p>
			</div>
			   <fmt:formatDate pattern="dd MMM yyyy"
				value="${announcementsEntryBean.createDate}"
				var="announcementCreateDate" />
			<p class="announcementdate">Posted on : ${announcementCreateDate}</p>
		</div>
	</div>

	</div>
</c:forEach>

<c:if test="<%=displayContext.getSearchContainer().getTotal() > 5%>">
	<div class="container-fluid">
		<h3>
			<a href="javascript: showOtherEntries()" class="view-label"><liferay-ui:message
					key="label-view-all" /></a>
		</h3>
	</div>
</c:if>

<script>
	$(document).ready(function() {
		manageAnnouncementContent();
	});

	function showOtherEntries() {
		if (!$('.more-entires').is(':visible')) {
			$(".more-entires").css('display', 'block');
			$(".view-label").css('display', 'none');
		} else {
			$(".more-entires").css('display', 'none');
			$(".view-label").css('display', 'block');
		}
	}

	function manageAnnouncementContent() {
		var showChar = 500;
		var ellipsestext = "...";
		$('.announcementdata').each(
				function() {
					var content = $(this).html();

					if (content.length > showChar) {

						var c = content.substr(0, showChar);
						var h = content.substr(showChar, content.length
								- (showChar - 1));

						var html = c + '<span class="moreellipses">'
								+ ellipsestext + '&nbsp;</span>';

						$(this).html(html);
					}
				});
	}
</script>