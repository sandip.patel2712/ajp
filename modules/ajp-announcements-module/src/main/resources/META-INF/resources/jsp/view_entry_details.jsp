<%@ include file="/init.jsp"%>

<liferay-ui:header title="announcement-entry" backURL="${backURL}" />

<div class="container-fluid announcementlistpanel">
	<div class="row content announcementinfo">
		<h2 class="announcementheader">${announcementsEntry.title }</h2>
		<fmt:formatDate pattern="dd MMM yyyy"
			value="${announcementsEntry.createDate}" var="createDate" />

		<p class="announcementdate">Posted on : ${createDate }</p>
		<br> <br>
		<div class="announcementdata">
			<p>${announcementsEntry.content }</p>
		</div>

	</div>
</div>