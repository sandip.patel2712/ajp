
<%@ include file="/init.jsp"%>


<c:forEach items="${courseBeanList}" var="course" varStatus="loop">
	<c:if test="${loop.index mod 3 == 0}">
		<div class="coursecontainer">
			<div class="row">
				<c:set var="rowIndex" scope="session" value="${loop.index}" />
	</c:if>
	<portlet:renderURL var="viewCourseDetailURL">
		<portlet:param name="mvcRenderCommandName" value="/view_course" />
		<portlet:param name="articleId"
			value="${ course.journalArticle.articleId}" />
	</portlet:renderURL>
	<div class="col-sm-4 text-left coursetitle">
		<div class="coursedetailboximage">
			<img src="${course.courseThumbnailImageUrl}"
				class="img-responsive courseimage" alt="course-image">
		</div>
		<div class="coursedetailbox">
			<div>
				<a href="${viewCourseDetailURL }" class="courselink">${course.title}
				</a>
			</div>
			<div  class="coursedetailboxdate">
				<fmt:formatDate value="${course.courseStartDate}"
					pattern="dd/MM/yyyy" var="courseStartDate" />
				<fmt:formatDate value="${course.courseEndDate}" pattern="dd/MM/yyyy"
					var="courseEndDate" />
				<p>Course Date </p><p>${courseStartDate} - ${courseEndDate} </p>
			</div>
		</div>
		<div class="coursedetailbox">
			<div>
				<p>${course.country}</p>
			</div>
			<div>
				<p class="subjectarea">${course.subjectArea}</p>
			</div>
			<div>
				<c:choose>
					<c:when test="${not empty course.courseAmount}">
						<p>${course.courseAmount}</p>
					</c:when>
					<c:otherwise>
						<p>Free</p>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<c:if test="${(loop.index mod 3) == 2}">
		</div>
		</div>
	</c:if>
</c:forEach>
