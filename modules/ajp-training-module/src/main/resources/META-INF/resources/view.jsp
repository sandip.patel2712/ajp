<%@page import="java.util.Calendar"%>
<%@page import="com.ajp.portal.constant.AJPConstant"%>
<%@page import="com.liferay.portal.kernel.theme.ThemeDisplay"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@ include file="/init.jsp" %>

<% 
SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
Calendar cal = Calendar.getInstance(themeDisplay.getLocale());
cal.setTime(new Date());
cal.add(Calendar.MONTH, 1);
Date courseToDate = cal.getTime();
%>

<portlet:resourceURL id="/search-courses" var="searchCoursesUrl">
    <portlet:param name="returnToFullPageURL" value="<%=PortalUtil.getCurrentURL(request)%>"/>
</portlet:resourceURL>

 <div class="container">
 		<aui:form action="" method="post" name="fm">
        <div class="row content">
            <div class="col-sm-3">
                <div class="row text-left keyinfoheader">
                    <div class="col-sm-12 text-left">
                        <h4>Sort By</h4>
                    </div>

                </div>
                <div class="row keyinfodata text-center">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select class="form-control form-control-lg sortdropdown">
                                <option selected value="0">Course Start Date (Ascending)</option>
                                <option value="1">Course Start Date (Descending)</option>
                            </select>
                        </div>
                    </div>

                </div>
                <br>

                <div>
                    <div class="row text-left keyinfoheader">
                        <div class="col-sm-6">
                            <h4 class="filterheader text-left">Filter By</h4>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="filterheader text-right reset-filter">Clear All</h5>
                        </div>
                    </div>
                    <div class="row  text-center">
                        <div class="col-sm-12 keyinfodata">
                            <p class="keyinfolabel">Subject Area</p>
                        </div>
                        <div class="keyinfodata col-sm-12">
							<c:forEach items="${subjectAreaCatList}" var="subjectAreaCat">
								<% AssetCategory subjectAreaCategory  = (AssetCategory)pageContext.getAttribute("subjectAreaCat"); %>
								<div class="checkbox">
                                	<label><input type="checkbox" name="subjectcat" class="subject-area" value="${subjectAreaCat.categoryId }">
                                	<%= subjectAreaCategory.getTitle(themeDisplay.getLanguageId())  %></label>
                            	</div>
							</c:forEach>
                        </div>
                    </div>

                    <div class="row text-center">
                        <div class="col-sm-12 keyinfodata">
                            <p class="keyinfolabel">Country of Training</p>
                        </div>
                        <div class="keyinfodata col-sm-12">
                            <c:forEach items="${countryCatList}" var="countryCat">
								<% AssetCategory countryCategory  = (AssetCategory)pageContext.getAttribute("countryCat"); %>
								<div class="checkbox">
                                	<label><input type="checkbox"  name="countrycat" class="country" value="${countryCat.categoryId }">
                                	<%= countryCategory.getTitle(themeDisplay.getLanguageId())  %></label>
                            	</div>
							</c:forEach>
                        </div>
                    </div>
                    <div class="row  text-center">
                        <div class="col-sm-12 keyinfodata">
                            <p class="keyinfolabel">Training Cost</p>
                        </div>
                        <div class="keyinfodata col-sm-12">
                            <div class="checkbox">
                                <label><input type="checkbox"  name="countrycosttpye" class="course-cost-type" value="<%= AJPConstant.COURSE_FREE_TYPE %>" >Free</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="countrycosttpye" class="course-cost-type" value="<%= AJPConstant.COURSE_PAID_TYPE %>">Paid</label>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-sm-12 keyinfodata">
                            <p class="keyinfolabel">Course Start Date</p>
                        </div>
                        <div class="keyinfodata text-center">
                            <div class="filter-date">
                                <input name="coursefromdate" class="form-control date" type="text"  id="coursefromdate" placeholder="mm/dd/yyyy" value=""
                                style="width: 75%; margin-left: 13%">
    							<input name="coursetodate" class="form-control date" type="text"  id="coursetodate" placeholder="mm/dd/yyyy" value=""
    							style="width: 75%; margin-left: 13%">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-sm-9">
                <div class="lfr-search-container-wrapper main-content-body">
				    <div id="<portlet:namespace/>coursesList" class="list-group">
				    </div>
				</div>
				<div id="<portlet:namespace/>loaderTrigger">
				    <div id="<portlet:namespace/>loadingSpinner" class="loading-animation"></div>
				</div>                
            </div>
        </div>
        </aui:form>
    </div>
  
<aui:script>
    
AUI().use('aui-io-request', 'aui-datepicker','aui-base', function(A) {
	
	var currentOffset = 0;
    var scrollAmount = 9;
    var isScrollAlive = true;
    var isLoading = false;
    
    var loadingSpinner = A.one("#" + '<portlet:namespace/>' + 'loadingSpinner');

    var dataContainer = A.one("#" + '<portlet:namespace/>' + 'coursesList');

    var controller = new ScrollMagic.Controller();
    
   // dataContainer.plug(A.Plugin.ParseContent);
    
    var scene = new ScrollMagic.Scene({
        triggerElement: "#" + '<portlet:namespace/>' + 'loaderTrigger',
        triggerHook: "onEnter"
    }).addTo(controller).on("enter", function () {
    	if (isScrollAlive && !isLoading) {
            loadCourses(null,null);
        }
    });
    
    
    function loadCourses(coursefromdate, courstodate) {
    	isLoading = true;
        loadingSpinner.show();
                
        var data = Liferay.Util.ns('<portlet:namespace/>', {
            'start': currentOffset,
            'end': currentOffset + scrollAmount,
            'subjectAreaCatIds' : getSubjectAreaCatList(),
            'countryCatIds' : getCountryCatList(),
            'courseCoustType' : getCourseCostType(),
            'sortType' : A.one('.sortdropdown').val(),
            'startdate' : coursefromdate ? coursefromdate : A.one('#coursefromdate').val(),
            'enddate' : courstodate ? courstodate : A.one('#coursetodate').val()
        });

        var searchCoursesUrl = '${searchCoursesUrl}';
        
        A.io.request(searchCoursesUrl.toString(), {
            method: 'POST',
            dataType: 'text/html',
            data: data,
            on: {
                success: function () {
                    var courses = this.get('responseData');
                    if (courses != null) {
                        var coursesNode = A.Node.create(courses.trim());
                        if (currentOffset === 0) {
                            dataContainer.html("");
                        }

                        coursesNode.appendTo(dataContainer);

                        scene.update();
                        currentOffset += scrollAmount;
                    } else {
                    	// isScrollAlive = false;
                    }
                },
                complete: function() {
                	console.log("in complete");
                    loadingSpinner.hide();
                    isLoading = false;
                }
            }
        });
    }
    
    /* function getCourseCategoryList(){
    	var courseCategoryIds="";
    	$('input[name="subjectcat"]:checked').each(function() {
    		courseCategoryIds += this.value + ",";
    	});
    	$('input[name="countrycat"]:checked').each(function() {
    		courseCategoryIds += this.value + ",";
    	});
    	courseCategoryIds = courseCategoryIds.replace(/,\s*$/, "");
    	return courseCategoryIds;
    } */
    
    function getSubjectAreaCatList(){
    	var subjectAreaCatIds="";
    	$('input[name="subjectcat"]:checked').each(function() {
    		subjectAreaCatIds += this.value + ",";
    	});
    	subjectAreaCatIds = subjectAreaCatIds.replace(/,\s*$/, "");
    	return subjectAreaCatIds;
    }
    
    function getCountryCatList(){
    	var countryCatIds="";
    	$('input[name="countrycat"]:checked').each(function() {
    		countryCatIds += this.value + ",";
    	});
    	countryCatIds = countryCatIds.replace(/,\s*$/, "");
    	return countryCatIds;
    }
    
    function getCourseCostType(){
    	var courseCostType="";
    	$('input[name="countrycosttpye"]:checked').each(function() {
    		courseCostType += this.value + ",";
    	});
    	courseCostType = courseCostType.replace(/,\s*$/, "");
    	return courseCostType;
    }
    
    A.all('.subject-area').each(
	  function (node) {
		  node.on('change', function(e) {
			  currentOffset = 0;
			  dataContainer.html("");
			  loadCourses(null,null);
		  });
	  }
	);
    
    A.all('.country').each(
		  function (node) {
			  node.on('change', function(e) {
				  currentOffset = 0;
				dataContainer.html("");
				  loadCourses(null,null);
			  });
		  }
  	);
    
    A.all('.course-cost-type').each(
	  function (node) {
		  node.on('change', function(e) {
			  currentOffset = 0;
			  dataContainer.html("");
			  loadCourses(null,null);
		  });
	  }
	);
    
    A.one(".sortdropdown").on('change', function(e) {
		  currentOffset = 0;
		  dataContainer.html("");
		  loadCourses(null,null);
    });
    
    A.one(".reset-filter").on('click', function(e) {
    	A.all('.subject-area').each(function (node) { node._node.checked = false; });
    	A.all('.country').each(function (node) { node._node.checked = false; });
    	A.all('.course-cost-type').each(function (node) { node._node.checked = false; });
    	$("#coursetodate").val("");
    	$("#coursefromdate").val("");
    	currentOffset = 0;
		dataContainer.html("");		
		loadCourses(null,null);
    });
    
    
    var fromDatePicker = new A.DatePicker({
        trigger: '#coursefromdate',
        mask: '%m/%d/%Y',
        popover: {
            zIndex: 1000
        },
        on: {
            selectionChange: function(event) {
            	if (event.newSelection[0]) {
            		var updatedDate = event.newSelection[0];
            		updatedDate = (updatedDate.getMonth()+1) +"/"+updatedDate.getDate()+"/"+updatedDate.getFullYear();
                    currentOffset = 0;
    				dataContainer.html("");
    				loadCourses(updatedDate,null);
            	}
            }
        },
        calendar: {
            minimumDate: new Date()
        }
    });
   
    
    var toDatePicker = new A.DatePicker({
        trigger: '#coursetodate',
        mask: '%m/%d/%Y',
        popover: {
            zIndex: 1000
        },
        on: {
            selectionChange: function(event) {
            	if (event.newSelection[0]) {
            		var updatedDate = event.newSelection[0];
            		updatedDate = (updatedDate.getMonth()+1)+"/"+updatedDate.getDate()+"/"+updatedDate.getFullYear();
                    currentOffset = 0;
    				dataContainer.html("");
    				loadCourses(null, updatedDate);
                }
            	
            }
        },
        calendar: {
            minimumDate: new Date()
        }
    });
  

    loadCourses(null,null);
    
    function onDestroyPortlet(event) {
        if (event.portletId === '<%=portletDisplay.getRootPortletId()%>') {
        	if (scene) {
                scene.destroy();
                scene = null;
            }
            if (controller) {
                controller.destroy(true);
                controller = null;
            }
        }
    }
    Liferay.on('destroyPortlet', onDestroyPortlet);

});
</aui:script>