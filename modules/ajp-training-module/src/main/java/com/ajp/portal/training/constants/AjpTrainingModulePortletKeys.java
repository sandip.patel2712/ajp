package com.ajp.portal.training.constants;

/**
 * @author diptivaghasia
 */
public class AjpTrainingModulePortletKeys {

	public static final String AJP_TRAINING_PORTLET_ID = "com_ajp_portal_training_portlet_AjpTrainingPortlet";

}