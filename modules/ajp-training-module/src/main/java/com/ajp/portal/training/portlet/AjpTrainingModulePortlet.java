package com.ajp.portal.training.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.ajp.portal.constant.AJPConstant;
import com.ajp.portal.training.constants.*;
import com.ajp.portal.training.constants.AjpTrainingModulePortletKeys;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

/**
 * @author diptivaghasia
 */
@Component(
    immediate = true,
    property = {
        "com.liferay.portlet.display-category=AJP",
        "com.liferay.portlet.instanceable=false",
        "javax.portlet.display-name=AJP Training Module Portlet",
        "javax.portlet.init-param.template-path=/",
        "javax.portlet.init-param.view-template=/view.jsp",
       
        "com.liferay.portlet.header-portlet-css=/css/course-details.css",
        "com.liferay.portlet.header-portlet-css=/css/course-training.css",
        "javax.portlet.name=" + AjpTrainingModulePortletKeys.AJP_TRAINING_PORTLET_ID,
        "javax.portlet.resource-bundle=content.Language",
        "javax.portlet.security-role-ref=power-user,user"
    },
    service = Portlet.class
)
public class AjpTrainingModulePortlet extends MVCPortlet {
    
    private static final Log _log = LogFactoryUtil.getLog(AjpTrainingModulePortlet.class);
    
    @Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
            throws IOException, PortletException {

        final ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

        long countryVocabId = 0L;
        long subjectAreaVocabId = 0L;
        try {
            List<AssetVocabulary> vocabList = AssetVocabularyLocalServiceUtil.getGroupVocabularies(themeDisplay.getCompanyGroupId());
            for (AssetVocabulary vocab : vocabList) {
                if (vocab.getTitle(themeDisplay.getLanguageId()).equals(AJPConstant.COUNTRY_VOCAB)) {
                    countryVocabId = vocab.getVocabularyId();
                }
                if (vocab.getTitle(themeDisplay.getLanguageId()).equals(AJPConstant.SUBJECT_AREA_VOCAB)) {
                    subjectAreaVocabId = vocab.getVocabularyId();
                }
            }
        } catch (PortalException e) {
            _log.error(e.getMessage(), e);
        }
        
        List<AssetCategory> countryCatList = AssetCategoryLocalServiceUtil.getVocabularyCategories(countryVocabId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null);
        
        List<AssetCategory> subjectAreaCatList = AssetCategoryLocalServiceUtil.getVocabularyCategories(subjectAreaVocabId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null);
        
        renderRequest.setAttribute("countryCatList", countryCatList);
        renderRequest.setAttribute("subjectAreaCatList", subjectAreaCatList);
        
        include(viewTemplate, renderRequest, renderResponse);
    }
}