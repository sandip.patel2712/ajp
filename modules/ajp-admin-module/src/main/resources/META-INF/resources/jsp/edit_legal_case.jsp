<%@ include file="../init.jsp"%>

<portlet:renderURL var="viewURL">
</portlet:renderURL>

<portlet:actionURL name="/jsp/editLegalCase" var="addLegalCase">
	<portlet:param name="actionType"
		value="${not empty legalCase ? 'editLegalCaseAction' : 'addLegalCaseAction'}" />
</portlet:actionURL>

<aui:form enctype="multipart/form-data" method="post" name="fm"
	cssClass="legalCaseForm form-group"
	onSubmit='<%="event.preventDefault(); " + renderResponse.getNamespace() + "saveEntry();"%>'>

	<aui:fieldset>
		<aui:input type="hidden" name="legalCaseId"
			value="${legalCase.legalCaseId}"></aui:input>

		<aui:input type="text" label="label-legalCase-title" name="title"
			value="${legalCase.title}">
			<aui:validator name="required" errorMessage="error-required" />
			<aui:validator name="maxLength" errorMessage="error-maxLength">255</aui:validator>
		</aui:input>
		<aui:input id="url" name="url" value="${not empty legalCase.url ? legalCase.url : '' }" label="case-url" >
			<aui:validator name="url"/>
			<aui:validator name="maxLength" errorMessage="error-maxLength">1000</aui:validator>
		</aui:input>
		<aui:field-wrapper label="label-legalCase-desc"  >
			<liferay-ui:input-editor contents="${legalCase.description}"
				editorName='legal-case-description' >
				<aui:validator name="maxLength" errorMessage="error-maxLength">30000</aui:validator>
				</liferay-ui:input-editor>
			<aui:input name="legalCaseDescription" type="hidden" />
		</aui:field-wrapper>

		<fmt:formatDate pattern="MM/dd/yyyy" value="${legalCase.decisionDate}"
			var="decisionDate" />

		<aui:input type="text" label="label-decision-date" id="decisionDate"
			name="decisionDate" class="date-selector" value="${decisionDate}"
			onfocus="openDatePicker();">
			<aui:validator name="required" />
		</aui:input>

		<aui:select name="countryCategoryId" label="label-country">
			<c:forEach items="${countryCategoryList}" var="countryCategory">
				<aui:option value="${countryCategory.categoryId}"
					selected="${countryCategory.categoryId == legalCase.countryCategoryId ? 'true' : 'false'}">${countryCategory.name}</aui:option>
			</c:forEach>
			<aui:validator name="required" />
		</aui:select>

		<aui:select name="subjectAreaCategoryId" label="label-subject-area">
			<c:forEach items="${subjectAreaCategoryList}"
				var="subjectAreaCategory">
				<aui:option value="${subjectAreaCategory.categoryId}"
					selected="${subjectAreaCategory.categoryId == legalCase.subareaCategoryId ? 'true' : 'false'}">${subjectAreaCategory.name}</aui:option>
			</c:forEach>
			<aui:validator name="required" />
		</aui:select>

		<aui:input type="file" name="legalCaseSummary"
			label="case-summary-upload" id="legalCaseSummary">
			<aui:validator name="acceptFiles" errorMessage="accept-files-error">'pdf,doc,docx,rtf'</aui:validator>
		</aui:input>

		<c:if
			test="${not empty legalCaseSummaryName && legalCaseSummaryName != '-'}">
			<span class="document-note">Selected file is :
				${legalCaseSummaryName}</span>
		</c:if>

		<aui:input type="file" name="legalCaseJudgement"
			label="case-judgement-upload" id="legalCaseJudgement">
			<aui:validator name="acceptFiles" errorMessage="accept-files-error">'pdf,doc,docx,rtf'</aui:validator>
		</aui:input>

		<c:if
			test="${not empty legalCaseJudgementName && legalCaseJudgementName != '-'}">
			<span class="document-note">Selected file is :
				${legalCaseJudgementName}</span>
		</c:if>
	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit"
			value="${not empty legalCase ? 'label-update' : 'label-save'}" />
		<aui:button type="cancel" onClick="${viewURL}%>" />
	</aui:button-row>
</aui:form>

<script>
    var datePicker;
    YUI().use('aui-base','aui-datepicker', function(Y) {
        datePicker = new Y.DatePicker({
            trigger: '#<portlet:namespace />decisionDate',
            calendar: {
            	maximumDate : new Date(), 
             },
            popover: {
                zIndex: 100,
            }
        });
    });

    function openDatePicker() {
        datePicker.getPopover().show();
    }

    function <portlet:namespace />getContent() {
		return window.<portlet:namespace />editor.getHTML();
	}

    function <portlet:namespace />saveEntry() {
		document.<portlet:namespace />fm.action = '${addLegalCase}';
		document.<portlet:namespace />fm.target = '';
		document.<portlet:namespace />fm.<portlet:namespace />legalCaseDescription.value = <portlet:namespace />getContent();
		submitForm(document.<portlet:namespace />fm);
	}
</script>