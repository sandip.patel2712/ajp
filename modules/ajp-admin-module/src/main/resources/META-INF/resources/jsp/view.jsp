<%@include file="../init.jsp"%>

<liferay-ui:error key="error-edit-legalCase"
	message="error-edit-legalCase" />

<liferay-ui:success key="legalCaseAdded"
	message="the-legalCase-was-created-successfully" />
<liferay-ui:success key="legalCaseUpdated"
	message="the-legalCase-was-updated-successfully" />
<liferay-ui:success key="legalCaseDeleted"
	message="the-legalCase-was-deleted-successfully" />

<%
	final LegalCasesDisplayContext displayContext = (LegalCasesDisplayContext) request
			.getAttribute(LegalCaseConstants.LEGAL_CASES_DISPLAY_CONTEXT);

	String orderByCol = ParamUtil.getString(request, "orderByCol");
	String orderByType = ParamUtil.getString(request, "orderByType");
	String sortingOrder = orderByType;
	if (orderByType.equals("desc")) {
		orderByType = "desc";
	} else {
		orderByType = "asc";
	}

	if (Validator.isNull(orderByType)) {
		orderByType = "desc";
	}
	
%>

<portlet:renderURL var="addLegalCase">
	<portlet:param name="mvcRenderCommandName" value="/jsp/editLegalCase" />
	<portlet:param name="actionType" value="addLegalCaseAction" />
</portlet:renderURL>

<portlet:renderURL var="searchURL">
	<portlet:param name="mvcRenderCommandName" value="/legalcase/search" />
</portlet:renderURL>

<liferay-frontend:management-bar>
	<liferay-frontend:management-bar-buttons>
		<liferay-frontend:management-bar-filters>
			<liferay-frontend:management-bar-sort orderByCol="<%= orderByCol %>"
				orderByType="<%= orderByType %>"
				orderColumns='<%= new String[] {"title","decisionDate", "countryCategoryId", "subareaCategoryId"} %>'
				portletURL="<%= displayContext.getSearchContainer().getIteratorURL() %>" />
		</liferay-frontend:management-bar-filters>
	</liferay-frontend:management-bar-buttons>
</liferay-frontend:management-bar>

<div class="legal-case-container">
	<div class="row">
		<div class="col-md-6">
			<aui:button onClick="${addLegalCase}" value="label-add-legalCase"></aui:button>
		</div>
		<div class="col-md-6">
			<aui:form action="<%=searchURL%>" name="searchFm">
				<liferay-ui:input-search markupView="lexicon" id="keyword"
					name="keyword" />
			</aui:form>
		</div>
	</div>
	<br>
	<br>

	<liferay-ui:search-container id="legalcases" searchContainer="<%= displayContext.getSearchContainer()%>"
		iteratorURL="<%= displayContext.getSearchContainer().getIteratorURL() %>"
		orderByCol="<%= orderByCol %>" orderByType="<%= orderByType %>"
		delta="10">

		<liferay-ui:search-container-row
			className="com.ajp.portal.model.LegalCaseBean"
			modelVar="legalCaseBean">

			<portlet:renderURL var="editLegalCase">
				<portlet:param name="mvcRenderCommandName"
					value="/jsp/editLegalCase" />
				<portlet:param name="actionType" value="editLegalCaseAction" />
				<portlet:param name="legalCaseId"
					value="${legalCaseBean.legalCaseId}" />
				<portlet:param name="legalCaseSummaryName"
					value="${legalCaseBean.legalCaseSummaryName}" />
					<portlet:param name="legalCaseJudgementName"
					value="${legalCaseBean.legalCaseJudgementName}" />
				<portlet:param name="currentURL"
					value="<%=themeDisplay.getURLCurrent()%>" />
			</portlet:renderURL>

			<portlet:renderURL var="deleteLegalCase">
				<portlet:param name="mvcRenderCommandName" value="/jsp/view" />
				<portlet:param name="actionType" value="deleteLegalCaseAction" />
				<portlet:param name="legalCaseId"
					value="${legalCaseBean.legalCaseId}" />
			</portlet:renderURL>

			<fmt:formatDate pattern="MM/dd/yyyy"
				value="${legalCaseBean.decisionDate}" var="decisionDate" />

			<liferay-ui:search-container-column-text name="label-legalCase-title"
				value="${legalCaseBean.title}" orderable="<%= true %>"
				orderableProperty="title" />
			
			<liferay-ui:search-container-column-text name="case-url"
						value="${not empty legalCaseBean.url ? legalCaseBean.url : '-'}" />

			<liferay-ui:search-container-column-text name="label-country"
						value="${legalCaseBean.country}" />
			<liferay-ui:search-container-column-text name="label-subject-area"
						value="${legalCaseBean.subjectArea}" />

			<liferay-ui:search-container-column-text name="label-decision-date"
				value="${decisionDate}" />
			<liferay-ui:search-container-column-text name="label-case-summary">
				<a href="${legalCaseBean.legalCaseSummaryURL}" target="_blank">${legalCaseBean.legalCaseSummaryName}
				</a>
			</liferay-ui:search-container-column-text>
			<liferay-ui:search-container-column-text name="label-case-judgement">
				<a href="${legalCaseBean.legalCaseJudgementURL}" target="_blank">${legalCaseBean.legalCaseJudgementName}
				</a>
			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="label-action">
				<liferay-ui:icon-menu direction="left-side"
					icon="<%=StringPool.BLANK%>" markupView="lexicon"
					message="<%=StringPool.BLANK%>" showWhenSingleIcon="<%=true%>">
					<liferay-ui:icon message="edit" url="${editLegalCase}" />
					<liferay-ui:icon-delete message="delete"
						confirmation="are-you-sure-you-want-to-delete-the-legal-case"
						url="${deleteLegalCase}" />
				</liferay-ui:icon-menu>
			</liferay-ui:search-container-column-text>
		</liferay-ui:search-container-row>
		<liferay-ui:search-iterator markupView="lexicon" paginate="<%=true%>"  />
	</liferay-ui:search-container>
</div>