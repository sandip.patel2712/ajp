package com.ajp.portal.admin.display.context;

import com.ajp.portal.admin.constants.LegalCaseConstants;
import com.ajp.portal.admin.portlet.helper.LegalCaseHelper;
import com.ajp.portal.constant.LegalCaseConstant;
import com.ajp.portal.model.LegalCase;
import com.ajp.portal.model.LegalCaseBean;
import com.ajp.portal.service.LegalCaseLocalServiceUtil;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.document.library.kernel.util.DLUtil;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;

import javaslang.Lazy;

/**
 * The display context for a list of legal cases.
 *
 */
public class LegalCasesDisplayContext {
	
    private static final Log _log = LogFactoryUtil.getLog(LegalCasesDisplayContext.class);
    /**
     * The search container for the page which lists legal cases.
     */
    private Lazy<SearchContainer<LegalCaseBean>> _searchContainer;
    
    /**
     * The keyword to filter legal cases by.
     */
    private final String _keyword;

	public LegalCasesDisplayContext(final LiferayPortletRequest liferayPortletRequest,
			final LiferayPortletResponse liferayPortletResponse) {

		final ThemeDisplay themeDisplay = (ThemeDisplay) liferayPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		LegalCaseHelper legalCaseHelper = new LegalCaseHelper();
		List<AssetCategory> countryCategories = legalCaseHelper.getCategoriesList((RenderRequest) liferayPortletRequest, LegalCaseConstants.CASE_COUNTRY_CATEGORY_NAME);
		List<AssetCategory> subjectAreaCategories = legalCaseHelper.getCategoriesList((RenderRequest) liferayPortletRequest, LegalCaseConstants.CASE_SUBJECT_AREA_CATEGORY_NAME);

		_keyword = ParamUtil.getString(liferayPortletRequest, "keyword");
		String orderByCol = ParamUtil.getString(liferayPortletRequest, "orderByCol");
		String orderByType = ParamUtil.getString(liferayPortletRequest, "orderByType");
		_log.debug("Search keyword is : "+_keyword);

		if (Validator.isNotNull(liferayPortletRequest) && Validator.isNotNull(liferayPortletResponse)) {
			
			 this._searchContainer = Lazy.of(() -> {
                final PortletURL iteratorUrl = liferayPortletResponse.createRenderURL();
                if (Validator.isNotNull(_keyword)) {
                    iteratorUrl.setParameter("keyword", _keyword);
                }
                final SearchContainer<LegalCaseBean> searchContainer = new SearchContainer<LegalCaseBean>(liferayPortletRequest,
                		iteratorUrl, null, "no-legal-case-were-found");
              
                searchContainer.setClassName(LegalCaseBean.class.getName());

                DLFolder caseDlFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(), 0, LegalCaseConstant.LEGAL_CASE_FOLDER_NAME);
                List<LegalCase> legalCases = new ArrayList<LegalCase>();
                if (Validator.isNotNull(_keyword)) {
                	legalCases = LegalCaseLocalServiceUtil.getLegalCases(_keyword, searchContainer.getStart(), searchContainer.getEnd(), orderByCol, orderByType);
                	List<LegalCase> allLegalCases = LegalCaseLocalServiceUtil.getLegalCases(_keyword, -1, -1, StringPool.BLANK, StringPool.BLANK);
                	searchContainer.setTotal(allLegalCases.size());
                } else {
                	legalCases = LegalCaseLocalServiceUtil.getLegalCases(_keyword, searchContainer.getStart(), searchContainer.getEnd(), orderByCol, orderByType);
                	searchContainer.setTotal(LegalCaseLocalServiceUtil.getLegalCasesCount());
                }
                final List<LegalCaseBean> results = new ArrayList<LegalCaseBean>(legalCases.size());
                for (int j = 0; j < legalCases.size(); j++) {
                	final LegalCase legalCase = legalCases.get(j);
                    final LegalCaseBean legalCaseBean = new LegalCaseBean(legalCase);
                    String legalCaseSummaryData = StringPool.BLANK;
                    String legalCaseJudgementData = StringPool.BLANK;
                    String country = StringPool.BLANK;
                    String subjectArea = StringPool.BLANK;
					try {
						legalCaseSummaryData = getLegalCaseDocumentData(legalCase, caseDlFolder, themeDisplay, legalCase.getSummaryFileEntryId());
						legalCaseJudgementData = getLegalCaseDocumentData(legalCase, caseDlFolder, themeDisplay, legalCase.getJudgementFileEntryId());
						country = getCountryName(legalCase, countryCategories);
						subjectArea = getSubjectAreaName(legalCase, subjectAreaCategories);
					} catch (PortalException e) {
						_log.error(e.getMessage(),e);
					}

					if (legalCaseSummaryData != StringPool.BLANK && legalCaseSummaryData != null) {
						String[] legalCaseSummaryDetails = legalCaseSummaryData.split("&&");
						legalCaseBean.setLegalCaseSummaryURL(legalCaseSummaryDetails[0]);
						legalCaseBean.setLegalCaseSummaryName(legalCaseSummaryDetails[1]);
					} else {
    					legalCaseBean.setLegalCaseSummaryURL("-");
        				legalCaseBean.setLegalCaseSummaryName("-");
    				}
					if (legalCaseJudgementData != StringPool.BLANK && legalCaseJudgementData != null) {
						String[] legalCaseJudgementDetails = legalCaseJudgementData.split("&&");
						legalCaseBean.setLegalCaseJudgementURL(legalCaseJudgementDetails[0]);
						legalCaseBean.setLegalCaseJudgementName(legalCaseJudgementDetails[1]);
    				} else {
    					legalCaseBean.setLegalCaseJudgementURL("-");
						legalCaseBean.setLegalCaseJudgementName("-");
    				}

    				legalCaseBean.setCountry(country);
    				legalCaseBean.setSubjectArea(subjectArea);
                    results.add(j, legalCaseBean);
                }
                searchContainer.setResults(results);

                return searchContainer;
		    });
		}
	}

	private String getSubjectAreaName(LegalCase legalCase, List<AssetCategory> subjectAreaCategories) {
		for (AssetCategory subjectArea : subjectAreaCategories) {
			if(legalCase.getSubareaCategoryId() == subjectArea.getCategoryId()) {
				return subjectArea.getName();
			}
		}
		return null;
	}

	private String getCountryName(LegalCase legalCase, List<AssetCategory> countryCategories) {
		for (AssetCategory country : countryCategories) {
			if(legalCase.getCountryCategoryId() == country.getCategoryId()) {
				return country.getName();
			}
		}
		return null;
	}

	private String getLegalCaseDocumentData(LegalCase legalCase, DLFolder caseDlFolder, ThemeDisplay themeDisplay,
			long caseFileEntryId) throws PortalException {
		DLFolder caseSubDlFolder = null;
		if (caseDlFolder != null) {
			caseSubDlFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getCompanyGroupId(),
					caseDlFolder.getFolderId(), String.valueOf(legalCase.getLegalCaseId()));
		}

		if (caseSubDlFolder != null && caseFileEntryId > 0) {
			FileEntry caseFileEntry = DLAppServiceUtil.getFileEntry(caseFileEntryId);
			String legalCaseDocumentName = caseFileEntry.getTitle();
			String legalCaseDocumentURL = DLUtil.getPreviewURL(caseFileEntry, caseFileEntry.getFileVersion(),
					themeDisplay, StringPool.BLANK);
			return legalCaseDocumentURL + "&&" + legalCaseDocumentName;
		}

		return null;
	}

	 /**
     * @return the search container that lists community groups.
     */
    public SearchContainer<LegalCaseBean> getSearchContainer() {
        return _searchContainer.get();
    }
}