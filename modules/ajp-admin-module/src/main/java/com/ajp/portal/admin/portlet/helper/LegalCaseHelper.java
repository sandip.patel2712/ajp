package com.ajp.portal.admin.portlet.helper;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.RenderRequest;

public class LegalCaseHelper {

	private static final Log _log = LogFactoryUtil.getLog(LegalCaseHelper.class);

	public List<AssetCategory> getCategoriesList(RenderRequest renderRequest, String vocabularyName) {
		final ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
		AssetVocabulary vocabulary = null;
		try {
			vocabulary = AssetVocabularyLocalServiceUtil.getGroupVocabulary(themeDisplay.getCompanyGroupId(),
					vocabularyName);
			categoryList = AssetCategoryLocalServiceUtil.getVocabularyCategories(vocabulary.getVocabularyId(), -1, -1,
					null);
			if (categoryList.size() > 0) {
				return categoryList;
			}
		} catch (PortalException e) {
			_log.error(e.getMessage(), e);
		}

		return null;
	}
}
