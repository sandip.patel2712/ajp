package com.ajp.portal.admin.constants;

public class LegalCaseConstants {

	public static final String ADD_LEGALCASE_ACTION = "addLegalCaseAction";
	public static final String EDIT_LEGALCASE_ACTION = "editLegalCaseAction";
	public static final String DELETE_LEGALCASE_ACTION = "deleteLegalCaseAction";
	public static final String LEGAL_CASES_DISPLAY_CONTEXT = "LEGAL_CASES_DISPLAY_CONTEXT";

	public static final String CASE_COUNTRY_CATEGORY_NAME = "Case Repository Country";
	public static final String CASE_SUBJECT_AREA_CATEGORY_NAME = "Case Repository Subject Area";
}
