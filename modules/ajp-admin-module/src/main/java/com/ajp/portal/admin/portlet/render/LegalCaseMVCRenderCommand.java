package com.ajp.portal.admin.portlet.render;

import static com.ajp.portal.admin.constants.LegalCaseConstants.ADD_LEGALCASE_ACTION;
import static com.ajp.portal.admin.constants.LegalCaseConstants.DELETE_LEGALCASE_ACTION;
import static com.ajp.portal.admin.constants.LegalCaseConstants.EDIT_LEGALCASE_ACTION;

import com.ajp.portal.admin.constants.LegalCaseConstants;
import com.ajp.portal.admin.constants.LegalCasePortletKeys;
import com.ajp.portal.admin.display.context.LegalCasesDisplayContext;
import com.ajp.portal.admin.portlet.helper.LegalCaseHelper;
import com.ajp.portal.model.LegalCase;
import com.ajp.portal.service.LegalCaseLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		immediate = true, 
		property = { 
			"javax.portlet.name=" + LegalCasePortletKeys.LEGALCASE,
			"mvc.command.name=/jsp/editLegalCase", 
			"mvc.command.name=/jsp/view"
			},
		service = MVCRenderCommand.class
)
public class LegalCaseMVCRenderCommand implements MVCRenderCommand{

	private static final Log _log = LogFactoryUtil.getLog(LegalCaseMVCRenderCommand.class);
	private LegalCaseLocalService _legalCaseLocalService;

	private String JSP_PATH = "/jsp/view.jsp";

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		String actionType = ParamUtil.getString(renderRequest, "actionType");
		_log.info("actionType:: " + actionType);

		setCategoryLists(renderRequest);
		setDisplayContext(renderRequest, renderResponse);

		if (ADD_LEGALCASE_ACTION.equals(actionType)) {
			JSP_PATH = "/jsp/edit_legal_case.jsp";
		} else if (EDIT_LEGALCASE_ACTION.equals(actionType)) {
			setEditLegalCaseDetails(renderRequest);
		} else if (DELETE_LEGALCASE_ACTION.equals(actionType)) {
			deleteLegalCase(renderRequest);
		}

		return JSP_PATH;
	}

	private void setDisplayContext(RenderRequest renderRequest, RenderResponse renderResponse) {
		renderRequest.setAttribute(LegalCaseConstants.LEGAL_CASES_DISPLAY_CONTEXT,
                new LegalCasesDisplayContext(PortalUtil.getLiferayPortletRequest(renderRequest),
                        PortalUtil.getLiferayPortletResponse(renderResponse)));
	}

	private void setCategoryLists(RenderRequest renderRequest) {
		LegalCaseHelper legalCaseHelper = new LegalCaseHelper();
		List<AssetCategory> countryCategories = legalCaseHelper.getCategoriesList(renderRequest, LegalCaseConstants.CASE_COUNTRY_CATEGORY_NAME);
		List<AssetCategory> subjectAreaCategories = legalCaseHelper.getCategoriesList(renderRequest, LegalCaseConstants.CASE_SUBJECT_AREA_CATEGORY_NAME);
		if(countryCategories.size() > 0) {
			renderRequest.setAttribute("countryCategoryList", countryCategories);
		}
		if(subjectAreaCategories.size() > 0) {
			renderRequest.setAttribute("subjectAreaCategoryList", subjectAreaCategories);
		}
	}

	private void setEditLegalCaseDetails(RenderRequest renderRequest) {
		long legalCaseId = ParamUtil.getLong(renderRequest, "legalCaseId");
		String redirect = (String) renderRequest.getAttribute("currentURL");
		String legalCaseSummaryName = ParamUtil.getString(renderRequest, "legalCaseSummaryName");
		String legalCaseJudgementName = ParamUtil.getString(renderRequest, "legalCaseJudgementName");
		renderRequest.setAttribute("redirect", redirect);
		try {
			if (legalCaseId != 0) {
				LegalCase legalCase = _legalCaseLocalService.getLegalCase(legalCaseId);
				if (Validator.isNotNull(legalCase)) {
					renderRequest.setAttribute("legalCase", legalCase);
					renderRequest.setAttribute("legalCaseSummaryName", legalCaseSummaryName);
					renderRequest.setAttribute("legalCaseJudgementName", legalCaseJudgementName);
					JSP_PATH = "/jsp/edit_legal_case.jsp";
				}
			}	
		} catch (PortalException e) {
			SessionErrors.add(renderRequest, "edit-legalCase-error");
			_log.error(e.getMessage(), e);
		}
	}

	private void deleteLegalCase(RenderRequest renderRequest) {
		long legalCaseId = 0L;
		final ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			legalCaseId = ParamUtil.getLong(renderRequest, "legalCaseId");
			if (legalCaseId != 0) {
				_legalCaseLocalService.deleteLegalCase(themeDisplay, legalCaseId);
				JSP_PATH = "/jsp/view.jsp";
			}
			SessionMessages.add(renderRequest, "legalCaseDeleted");
		} catch (PortalException e) {
			SessionErrors.add(renderRequest, "edit-legalCase-error");
			_log.error(e.getMessage(), e);
		}

		_log.debug("LegalCase (" + legalCaseId + ") is deleted successfully.");
	}

	@Reference(unbind = "-")
    protected void setLegalCaseService(LegalCaseLocalService legalCaseLocalService) {
		_legalCaseLocalService = legalCaseLocalService;
    }
}
