package com.ajp.portal.admin.portlet.action;

import static com.ajp.portal.admin.constants.LegalCaseConstants.ADD_LEGALCASE_ACTION;
import static com.ajp.portal.admin.constants.LegalCaseConstants.EDIT_LEGALCASE_ACTION;

import com.ajp.portal.admin.constants.LegalCasePortletKeys;
import com.ajp.portal.model.LegalCase;
import com.ajp.portal.service.LegalCaseLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		immediate = true,
		property = {
				"javax.portlet.name=" +  LegalCasePortletKeys.LEGALCASE,
				"mvc.command.name=/jsp/editLegalCase"
				},
		service = MVCActionCommand.class
)
public class LegalCaseMVCActionCommand extends BaseMVCActionCommand {

	private static final Log _log = LogFactoryUtil.getLog(LegalCaseMVCActionCommand.class);
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private LegalCaseLocalService _legalCaseLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		final ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String actionType = ParamUtil.getString(actionRequest, "actionType");
		String title = ParamUtil.getString(actionRequest, "title");
		String url = ParamUtil.getString(actionRequest, "url");
		String description = ParamUtil.getString(actionRequest, "legalCaseDescription");
		String decisionDateStr = ParamUtil.getString(actionRequest, "decisionDate");
		long countryCategoryId = ParamUtil.getLong(actionRequest, "countryCategoryId");
		long subareaCategoryId = ParamUtil.getLong(actionRequest, "subjectAreaCategoryId");
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		File caseSummaryFile = uploadRequest.getFile("legalCaseSummary");
		String caseSummaryFileName = uploadRequest.getFileName("legalCaseSummary");
		File caseJudgementFile = uploadRequest.getFile("legalCaseJudgement");
		String caseJudgementFileName = uploadRequest.getFileName("legalCaseJudgement");

		if (EDIT_LEGALCASE_ACTION.equals(actionType)) {
			editLegalCase(actionRequest, themeDisplay, title, url, description, decisionDateStr,
					countryCategoryId, subareaCategoryId, caseSummaryFile, caseSummaryFileName, caseJudgementFile, caseJudgementFileName);
		} else if (ADD_LEGALCASE_ACTION.equals(actionType)) {
			addLegalCase(actionRequest, themeDisplay, title, url, description, decisionDateStr, countryCategoryId,
					subareaCategoryId, caseSummaryFile, caseSummaryFileName, caseJudgementFile, caseJudgementFileName);
		}
	}

	private void addLegalCase(ActionRequest actionRequest, ThemeDisplay themeDisplay, String title, String url, String description,
			String decisionDateStr, long countryCategoryId, long subareaCategoryId, File caseSummaryFile,
			String caseSummaryFileName, File caseJudgementFile, String caseJudgementFileName) {
		Date decisionDate = null;
		try {
			decisionDate = simpleDateFormat.parse(decisionDateStr);
			ServiceContext serviceContext = ServiceContextFactory.getInstance(LegalCase.class.getName(), actionRequest);
			_legalCaseLocalService.addLegalCase(themeDisplay, countryCategoryId, subareaCategoryId, title, url, description,
					decisionDate, caseSummaryFile, caseSummaryFileName, caseJudgementFile, caseJudgementFileName, serviceContext);
			SessionMessages.add(actionRequest, "legalCaseAdded");
		} catch (ParseException | PortalException | IOException e) {
			SessionErrors.add(actionRequest, "error-edit-legalCase");
			_log.error(e.getMessage(), e);
		}
	}

	private void editLegalCase(ActionRequest actionRequest, ThemeDisplay themeDisplay,
			String title, String url, String description, String decisionDateStr, long countryCategoryId, long subareaCategoryId,
			File caseSummaryFile, String caseSummaryFileName, File caseJudgementFile, String caseJudgementFileName) {
		Date decisionDate = null;
		try {
			long legalCaseId = ParamUtil.getLong(actionRequest, "legalCaseId");
			decisionDate = simpleDateFormat.parse(decisionDateStr);
			ServiceContext serviceContext = ServiceContextFactory.getInstance(LegalCase.class.getName(), actionRequest);
			_legalCaseLocalService.updateLegalCase(themeDisplay, legalCaseId, countryCategoryId, subareaCategoryId,
					title, url, description, decisionDate, caseSummaryFile, caseSummaryFileName, caseJudgementFile, caseJudgementFileName, serviceContext);
			SessionMessages.add(actionRequest, "legalCaseUpdated");
		} catch (ParseException | PortalException | IOException e) {
			SessionErrors.add(actionRequest, "error-edit-legalCase");
			_log.error(e.getMessage(), e);
		}
	}

	@Reference(unbind = "-")
	protected void setLegalCaseService(LegalCaseLocalService legalCaseLocalService) {
		_legalCaseLocalService = legalCaseLocalService;
	}
}
