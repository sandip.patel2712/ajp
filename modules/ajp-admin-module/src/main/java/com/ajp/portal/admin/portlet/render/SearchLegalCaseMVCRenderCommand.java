package com.ajp.portal.admin.portlet.render;

import com.ajp.portal.admin.constants.LegalCaseConstants;
import com.ajp.portal.admin.constants.LegalCasePortletKeys;
import com.ajp.portal.admin.display.context.LegalCasesDisplayContext;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.PortalUtil;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

@Component(
    property = {
            "javax.portlet.name=" + LegalCasePortletKeys.LEGALCASE,
            "mvc.command.name=/legalcase/search"
    },
    service = MVCRenderCommand.class
)
public class SearchLegalCaseMVCRenderCommand implements MVCRenderCommand {

	private final String JSP_PATH = "/jsp/view.jsp";

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		renderRequest.setAttribute(LegalCaseConstants.LEGAL_CASES_DISPLAY_CONTEXT,
                new LegalCasesDisplayContext(PortalUtil.getLiferayPortletRequest(renderRequest),
                        PortalUtil.getLiferayPortletResponse(renderResponse)));

		return JSP_PATH;
	}
}
