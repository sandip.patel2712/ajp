package com.ajp.portal.admin.portlet;

import com.ajp.portal.admin.constants.LegalCaseConstants;
import com.ajp.portal.admin.constants.LegalCasePortletKeys;
import com.ajp.portal.admin.display.context.LegalCasesDisplayContext;
import com.ajp.portal.admin.portlet.helper.LegalCaseHelper;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PortalUtil;

import java.io.IOException;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author Pc
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.css-class-wrapper=portlet-controlpanel",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.preferences-owned-by-group=true",
		"com.liferay.portlet.render-weight=100",
		"com.liferay.portlet.header-portlet-css=/css/main.min.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Case Management",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/jsp/view.jsp",
		"javax.portlet.name=" + LegalCasePortletKeys.LEGALCASE,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=administrator"
	},
	service = Portlet.class
)
public class LegalCasePortlet extends MVCPortlet {

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		renderRequest.setAttribute(LegalCaseConstants.LEGAL_CASES_DISPLAY_CONTEXT,
				new LegalCasesDisplayContext(PortalUtil.getLiferayPortletRequest(renderRequest),
						PortalUtil.getLiferayPortletResponse(renderResponse)));
		setCategoryLists(renderRequest);
		include(viewTemplate, renderRequest, renderResponse);
	}

	private void setCategoryLists(RenderRequest renderRequest) {
		LegalCaseHelper legalCaseHelper = new LegalCaseHelper();
		List<AssetCategory> countryCategories = legalCaseHelper.getCategoriesList(renderRequest,
				LegalCaseConstants.CASE_COUNTRY_CATEGORY_NAME);
		List<AssetCategory> subjectAreaCategories = legalCaseHelper.getCategoriesList(renderRequest,
				LegalCaseConstants.CASE_SUBJECT_AREA_CATEGORY_NAME);
		if (countryCategories.size() > 0) {
			renderRequest.setAttribute("countryCategoryList", countryCategories);
		}
		if (subjectAreaCategories.size() > 0) {
			renderRequest.setAttribute("subjectAreaCategoryList", subjectAreaCategories);
		}
	}
}