package com.ajp.portal.admin.portlet;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
		immediate = true,
		property = { 
				"panel.category.key=" + PanelCategoryKeys.CONTROL_PANEL_APPS,
				"service.ranking:Integer=1000" },
		service = PanelApp.class)
public class ControlPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return "com_ajp_portal_admin_LegalCasePortlet";
	}

	@Override
	@Reference(target = "(javax.portlet.name=com_ajp_portal_admin_LegalCasePortlet)", unbind = "-")
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}